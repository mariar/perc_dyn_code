
%% training set

addpath('/Users/maria/MATLAB-Drive/MATLAB/perc_dyn_code/scripts_stimgen copy/scripts_stimgen copy');
addpath( '/Users/maria/MATLAB-Drive/MATLAB/perc_dyn_code/meg_study_stimuli');

stem = '481p';

ntrials     = 150;
nblocks     = 1;
npings      = 0;
AB          = [0.1 0.3 0.5 0.7 0.9];
block_prbs  = [0.2 0.5 0.8];
switchprob  = 0.04;
outdir      =  '/Users/maria/MATLAB-Drive/MATLAB/perc_dyn_code/meg_study_stimuli';
framerate   = 100; % behavioural lab screen
displayparms.monitor_width_in_mm    = 545;
displayparms.sub_mon_dist_in_mm     = 960;
displayparms.monitor_horiz_res      = 1920;

% make the task slightly harder every time to force use of prior...
REL     = linspace(0.1,0.9,5);
outfile = [stem '1a.mat'];
generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)
outfile = [stem '1b.mat'];
generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)
outfile = [stem '1c.mat'];
generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)
% outfile = [stem '1d.mat'];
% generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)

REL     = linspace(0.16,0.84,5);
outfile = [stem '2a.mat'];
generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)
outfile = [stem '2b.mat'];
generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)
% outfile = [stem '2c.mat'];
% generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)

REL     = linspace(0.22,0.78,5);
outfile = [stem '3a.mat'];
generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)
% outfile = [stem '3b.mat'];
% generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)

% REL     = linspace(0.28,0.72,5);
% outfile = [stem '4a.mat'];
% generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)

%% test set
addpath('/Users/maria/MATLAB-Drive/MATLAB/perc_dyn_code/scripts_stimgen copy/scripts_stimgen copy');
addpath( '/Users/maria/MATLAB-Drive/MATLAB/perc_dyn_code/meg_study_stimuli');

stem = '481m';

ntrials     = 150;
nblocks     = 1;
npings      = 0;
AB          = [0.1 0.3 0.5 0.7 0.9];
REL         = [0.1 0.3 0.5 0.7 0.9];
block_prbs  = [0.2 0.5 0.8];
switchprob  = 0.04;
outdir      =  '/Users/maria/MATLAB-Drive/MATLAB/perc_dyn_code/meg_study_stimuli';
framerate   = 120; % effective framerate of propix for stimulus presentation
displayparms.monitor_width_in_mm    = 545;
displayparms.sub_mon_dist_in_mm     = 1200;
displayparms.monitor_horiz_res      = 1920;

outfile = [stem '1.mat'];
generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)
outfile = [stem '2.mat'];
generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)
outfile = [stem '3.mat'];
generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)
outfile = [stem '4.mat'];
generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)
outfile = [stem '5.mat']; % make a 5th one just in case... (eg if a block is aborted half way through)
generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)

%% visualise to check

cd(outdir)
load newruns_prac1.mat
sp{1} = seq_props;
load newruns_prac2.mat
sp{2} = seq_props;
load newruns_prac3.mat
sp{3} = seq_props;
load newruns_prac4.mat
sp{4} = seq_props;

clearvars -except sp

ab = [sp{1}.ab sp{2}.ab sp{3}.ab sp{4}.ab];
rel = [sp{1}.rel sp{2}.rel sp{3}.rel sp{4}.rel];
