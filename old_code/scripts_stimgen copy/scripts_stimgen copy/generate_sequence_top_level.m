addpath('/Users/marshall/Documents/MATLAB/meg_study/scripts_stimgen');

ntrials=500;
nblocks=10;
npings=100;
min_trls_btw_pings=1;
AB=[0.1 0.3 0.5 0.7 0.9];
REL=[0.1 0.3 0.5 0.7 0.9];
block_prbs=[0.2 0.5 0.8];
switchprob=0.04;
outdir='/Users/marshall/Documents/MATLAB/meg_study_stimuli';
outfile='feedback_testing.mat';

generate_seq_of_rdks(ntrials,nblocks,npings,min_trls_btw_pings,AB,REL,block_prbs,switchprob,outdir,outfile)

%% short version

addpath('/Users/marshall/Documents/MATLAB/meg_study/scripts_stimgen');

ntrials=108;
nblocks=3;
npings=12;
min_trls_btw_pings=1;
AB=[0.3 0.5 0.7];
REL=[0.3 0.5 0.7];
block_prbs=[0.2 0.5 0.8];
switchprob=0.04;
outdir='/Users/marshall/Documents/MATLAB/meg_study_stimuli';
outfile='propix.mat';

generate_seq_of_rdks(ntrials,nblocks,npings,min_trls_btw_pings,AB,REL,block_prbs,switchprob,outdir,outfile)

%% first meg pilot

addpath('/Users/marshall/Documents/MATLAB/meg_study/scripts_stimgen');

ntrials=500;
nblocks=10;
npings=0;
min_trls_btw_pings=1;
AB=[0.1 0.3 0.5 0.7 0.9];
REL=[0.1 0.3 0.5 0.7 0.9];
block_prbs=[0.2 0.5 0.8];
switchprob=0.04;
outdir='/Users/marshall/Documents/MATLAB/meg_study_stimuli';
outfile='percdyn_meg_pilot6.mat';

generate_seq_of_rdks(ntrials,nblocks,npings,min_trls_btw_pings,AB,REL,block_prbs,switchprob,outdir,outfile)

%%

%% first meg subj

addpath('/Users/marshall/Documents/MATLAB/meg_study/scripts_stimgen');

ntrials=300;
nblocks=6;
npings=0;
min_trls_btw_pings=1;
AB=[0.1 0.3 0.5 0.7 0.9];
REL=[0.1 0.3 0.5 0.7 0.9];
block_prbs=[0.2 0.5 0.8];
switchprob=0.04;
outdir='/Users/marshall/Documents/MATLAB/meg_study_stimuli';
outfile='perc_dyn_s427_practice.mat';

generate_seq_of_rdks(ntrials,nblocks,npings,min_trls_btw_pings,AB,REL,block_prbs,switchprob,outdir,outfile)