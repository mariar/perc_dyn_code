function [RDK_incoh,RDK_coh,dots_incoh,dots_coh] = tom_gen_one_RDK_meg( dots )
% Usage : RDKframes = hb_getRDK(dots)
%
%  EXAMPLE USE
% % dots = [];
% % dots.params.fieldSize = [500 500];
% % dots.params.fieldShape = 1; % 0~1 : Spare factor, 1~ : Square-like field
% % dots.params.nDots = 100; % number of dots
% % dots.params.coherence.abs = .5; % 0~1 : Proportion of coherent dot
% % dots.params.speed = 100; % Pixel per second
% % dots.params.duration = 2; %
% % dots.params.ori = 0; % 0~360 : Orientation
% % dots.params.frameRate = 60;
% % dots.params.dotLifeOption = 'N'; % 'N' for normal dist, 'U' for uniform dist
% % dots.params.dotLife = dots.params.duration * dots.params.frameRate; % frames, Mean of dots' life
% % nFrames = dots.params.frameRate * dots.params.duration;
% % [RDKcoordinates,dots] = hb_getRDK(dots);
% % for frameIdx = 1:nFrames
% %     plot( RDKcoordinates(:, 1, frameIdx),RDKcoordinates(:, 2, frameIdx), 'kp' );
% %     axis([-1 1 -1 1]*.75*dots.circField.size(1));
% %     axis xy;
% %     drawnow;
% % end
% Written by Hio-Been Han, hiobeen@yonsei.ac.kr, 2016-08-01
% Simple alternative for VCRDM Toolbox from Shadlen Lab (www.shadlenlab.columbia.edu)
% �?들렌 코드가 구려서 �?접 만들었�?�

warning off;
if nargin < 1
    disp('No input argument detected. Make with default setting')
    dots = [];
    dots.params.fieldSize       = [500 500]; % Unit : Pixel   
    dots.params.fieldShape      = 1; % 0~1 : Spare factor, 1~ : Square-like field
    dots.params.nDots           = 100; % number of dots
    dots.params.coherence.abs   = .5; % 0~1 : Proportion of coherent dot
    
    dots.params.speed           = 100; % Unit : Pixel per second
    dots.params.duration        = 2; %
    dots.params.ori1            = 0; % 0~360 : Orientation
    dots.params.ori2            = 0;
    dots.params.frameRate       = 60;
    dots.params.dotLifeOption   = 'N'; % 'N' for normal dist, 'U' for uniform dist
    dots.params.dotLife         = [50, 5]; % frames, [ Mean, SD ] of dots' life, SD is optional for 'N'
    nFrames                     = dots.params.frameRate * dots.params.duration;
end

%% (1) Create Field (same for incoh and coh rdks)

dots=rdk_create_circfield(dots);

%% (2) Set movement parameters (different for incoh and coh)

dots.params.angles_incoh=ones(1,dots.params.nDots) .* 360 .* rand([1,dots.params.nDots]);

if ~dots.params.isping
    dots.params.angles_coh=dots.params.angles_incoh;
    tmp=randperm(dots.params.nDots);
    dots_to_zero=tmp(1:floor(dots.params.nDots*dots.params.coherence.abs));
    dots.params.angles_coh(dots_to_zero)=0; % now a zero indicates the dot will be coherent
end

% fill in the two different motion vectors (this was the bit I hacked)
if ~dots.params.isping
    try
        dots.params.angles_coh((dots.params.angles_coh==0)) = [...
            ones(1,round(sum(dots.params.angles_coh==0)*dots.params.coherence.rel))*dots.params.ori1...
            ones(1,round(sum(dots.params.angles_coh==0)*(1-dots.params.coherence.rel)))*dots.params.ori2...
            ];
    catch
        % this is stupid, but sometimes the above fails because of the use of
        % 'round'. Normally rounding 100*n and 100*(1-n) (where 0<n<1) will
        % mean that one gets rounded up and the other down. But in the annoying
        % case where n==0.5, both will get rounded up, and the resultant vector
        % of dots will be one too long. This messy hack fixes that
        tmp=[...
            ones(1,round(sum(dots.params.angles_coh==0)*dots.params.coherence.rel))*dots.params.ori1...
            ones(1,round(sum(dots.params.angles_coh==0)*(1-dots.params.coherence.rel)))*dots.params.ori2...
            ];
        x=1+(round(rand)*(length(tmp)-1)); %x is one half the time and length(tmp) the other half
        tmp(x)=[]; %remove either the first (ori1) or last (ori2) dot
        dots.params.angles_coh((dots.params.angles_coh==0)) = tmp;
    end
end
nFrames_incoh = floor(dots.params.frameRate * dots.params.duration_incoh);
nFrames_coh = floor(dots.params.frameRate * dots.params.duration_coh);

%% (3) INITIAL RANDOM MOTION - Initial prototype of dot configuration

dots_incoh=dots;
dots_incoh.params.angles=dots_incoh.params.angles_incoh;
dots_incoh=rdk_initpos(dots_incoh);

%% (4) loop through and fill up this rdk

dots_incoh.params.duration=dots_incoh.params.duration_incoh;
dots_incoh.params.nFrames=dots_incoh.params.nFrames_incoh;
dots_incoh=rdk_gen_motion(dots_incoh);

%% (5) coherent motion, initial position is final from old position

% only on nonping trials
if ~dots.params.isping
    dots_coh=dots;
    dots_coh.dotInfo=dots_incoh.dotFrames(end).dotInfo;
    dots_coh.params.duration=dots_coh.params.duration_coh;
    dots_coh.params.nFrames=dots_coh.params.nFrames_coh;
    dots_coh.params.angles=dots_coh.params.angles_coh;
    
    for dotIdx = 1:dots.params.nDots
        dots_coh.dotInfo(dotIdx).angle = dots.params.angles_coh(dotIdx);
        dots_coh.dotInfo(dotIdx).dx = dots_coh.params.speed*sin(dots_coh.params.angles(dotIdx)*pi/180)/(dots_coh.params.frameRate-1);
        dots_coh.dotInfo(dotIdx).dy = dots_coh.params.speed*cos(dots_coh.params.angles(dotIdx)*pi/180)/(dots_coh.params.frameRate-1);
        %         dots_coh.dotInfo(dotIdx).XY = [dots_coh.circField.coords(dots_coh.circField.randperm(dotIdx),1)-(dots_coh.circField.size(1)*.5),...
        %         dots_coh.circField.coords(dots_coh.circField.randperm(dotIdx),2)-(dots_coh.circField.size(2)*.5)];
        dots_coh.dotInfo(dotIdx).remained_life=dots_incoh.dotInfo(dotIdx).remained_life;
    end
    % loop through and fill up the coherent rdk
    
    dots_coh=rdk_gen_motion(dots_coh);
    
else
    dots_coh=[]; % no coherent motion on ping trials!
end

%% this method makes the first frame of dots_coh identical to the last frame of dots_incoh
% so rdk_gen_motion makes one frame too many, which we trim off here

% dots_incoh.dotFrames=dots_incoh.dotFrames(1:end-1);
% dots_incoh.params.nFrames=dots_incoh.params.nFrames-1;

% and because we're stupid and use the same function to create the coherent
% motion, we have to trim the last frame of that too

% if ~dots.params.isping
%     dots_coh.dotFrames=dots_coh.dotFrames(1:end-1);
%     dots_coh.params.nFrames=dots_coh.params.nFrames-1;
% end
%% (5) Convert into 3-D matrices

disp('converting incoh');
[dots_incoh,RDK_incoh]=rdk_conv_to_3d(dots_incoh);

if ~dots.params.isping
    disp('converting coh');
    [dots_coh,RDK_coh]=rdk_conv_to_3d(dots_coh);
else
    disp('no coh to convert');
    RDK_coh=[];
end

end

%% subfunctions

function dots=rdk_create_circfield(dots)
dots.circField = [];
dots.circField.size= dots.params.fieldSize;
dots.circField.oval = hb_cropOval( ones(dots.circField.size), dots.params.fieldShape, 0);
dots.circField.coords = [];
[dots.circField.coords(:,1),dots.circField.coords(:,2)]= ind2sub( size(dots.circField.oval), find(dots.circField.oval==1));
dots.circField.randperm = hb_randperm( size(dots.circField.coords,1) );
end

function dots=rdk_initpos(dots)
dots.dotInfo = [];
for dotIdx = 1:dots.params.nDots
    dots.dotInfo(dotIdx).dotIdx = dotIdx;
    dots.dotInfo(dotIdx).angle = dots.params.angles(dotIdx);
    dots.dotInfo(dotIdx).dx = dots.params.speed*sin(dots.params.angles(dotIdx)*pi/180)/(dots.params.frameRate-1);
    dots.dotInfo(dotIdx).dy = dots.params.speed*cos(dots.params.angles(dotIdx)*pi/180)/(dots.params.frameRate-1);
    dots.dotInfo(dotIdx).XY = [dots.circField.coords(dots.circField.randperm(dotIdx),1)-(dots.circField.size(1)*.5),...
        dots.circField.coords(dots.circField.randperm(dotIdx),2)-(dots.circField.size(2)*.5)];
    switch(dots.params.dotLifeOption)
        case('U') % Uniform
            dots.dotInfo(dotIdx).remained_life = floor(rand()*dots.params.dotLife(1)*2)+1;
        case('N') % Normal
            dots.dotInfo(dotIdx).remained_life = floor(normrnd(dots.params.dotLife(1),dots.params.dotLife(2)))+1;
    end
end
end

function dots=rdk_gen_motion(dots)
dots.dotFrames = [];
dots.dotFrames(1).dotInfo = dots.dotInfo;

dots.circField.randperm_counter = dots.params.nDots;
for frameIdx = 2:dots.params.nFrames + 1 % this is +1 for a silly reason
    dots.dotFrames(frameIdx).dotInfo = dots.dotInfo;
    for dotIdx = 1:dots.params.nDots
        refresh = 0;
        % Incremental change (life)
        life = dots.dotFrames(frameIdx-1).dotInfo(dotIdx).remained_life -1;
        if life > 0
            dots.dotFrames(frameIdx).dotInfo(dotIdx).remained_life = life;
        else
            % Get new dot XY
            newdotIdx = dots.circField.randperm(dots.circField.randperm_counter);
            dots.circField.randperm_counter=dots.circField.randperm_counter+1;
            XY =...
                [dots.circField.coords(dots.circField.randperm(newdotIdx),1)-(dots.circField.size(1)*.5),...
                dots.circField.coords(dots.circField.randperm(newdotIdx),2)-(dots.circField.size(2)*.5)];
            refresh = 1;
        end
        
        if ~refresh
            % Incremental change (pos)
            XY = dots.dotFrames(frameIdx-1).dotInfo(dotIdx).XY+[...
                dots.dotFrames(frameIdx).dotInfo(dotIdx).dx, dots.dotFrames(frameIdx).dotInfo(dotIdx).dy];
            try % 트�?��?��?치 나중�? 바꾸기
                inCircle = dots.circField.oval(round(XY(1)+(dots.circField.size(1)*.5)),round(XY(2)+(dots.circField.size(2)*.5)));
            catch
                inCircle = false;
            end
            
            if ~inCircle
                % Get new dot XY
                newdotIdx = dots.circField.randperm(dots.circField.randperm_counter);
                dots.circField.randperm_counter=dots.circField.randperm_counter+1;
                XY =...
                    [dots.circField.coords(dots.circField.randperm(newdotIdx),1)-(dots.circField.size(1)*.5),...
                    dots.circField.coords(dots.circField.randperm(newdotIdx),2)-(dots.circField.size(2)*.5)];
            end
        end
        dots.dotFrames(frameIdx).dotInfo(dotIdx).XY = XY;
        
    end
end
end

function [dots,RDKcoordinates]=rdk_conv_to_3d(dots)
dots.RDKcoordinates = [];
for frameIdx = 1:dots.params.nFrames
    XYs = [];
    for dotIdx = 1:dots.params.nDots
        XY = dots.dotFrames(frameIdx).dotInfo(dotIdx).XY;
        XYs = [XYs;XY];
    end
    dots.RDKcoordinates = cat(3, dots.RDKcoordinates, XYs);
end

RDKcoordinates = dots.RDKcoordinates;

end

function resultImg = hb_cropOval(inputImg, spareFactor, bgrcolor)
if nargin < 3
    bgrcolor = 0;
end
hwidth = size(inputImg, 2) / 2.0;
hheight = size(inputImg, 1) / 2.0;

spareWidth = hwidth * spareFactor;
spareHeight = hheight * spareFactor;

[ww, hh] = meshgrid(1:hwidth, 1:hheight);

% simple ellipse equation gets us part three of your mask
mask_rightBottom = (((ww.^2)/spareWidth^2+(hh.^2)/spareHeight^2)<=1); 
mask_rightTop = flipud(mask_rightBottom);
mask_leftBottom = fliplr(mask_rightBottom);
mask_leftTop = flipud(mask_leftBottom);

mask_integrated = [mask_leftTop, mask_rightTop; ...
    mask_leftBottom, mask_rightBottom];

resultImg = inputImg;
[~,~,nDim] = size(resultImg);
if nDim == 1
    resultImg(mask_integrated(:,:)==0) = bgrcolor;
else
    multichannel_mask = repmat(mask_integrated,[1 1 nDim]);
    resultImg(multichannel_mask==0) = bgrcolor;
end

end

function shuffled_v = hb_Shuffle(v)
shuffled_v = v([hb_randperm(length(v))]);
end

function perm = hb_randperm(N)
%  USAGE -> perm= hb_randperm(N)
%  perm = hb_randperm(N) returns a vector containing a random permutation of the
%    integers 1:N.  For example, randperm(6) might be [2 4 5 6 1 3].
% 
[~, perm]=sort(rand([N,1]));
end
