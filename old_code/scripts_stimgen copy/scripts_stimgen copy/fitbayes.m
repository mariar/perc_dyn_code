%% get the 'true' q (block properties) from the corresponding stimulus file

function [Eq, EH] = fitbayes(y)

% stimdir=[dirstem 'seq_version_testing/pilot stim'];
% stimfile=['seq_pilot' num2str(subno) '.mat'];

% load(fullfile(stimdir,stimfile));

% q=dotProps.blx(1:size(d,1));

%% compute prior

% H=1/25; %probability of side reversal

% y=d(:,7);

%% now set up the state space
% possible values for p and j
q_candidates=(0.01:0.01:0.99)';
H_candidates=exp(log(0.01):(log(0.2)-log(0.01))/20:log(0.2))';

% grids
[qq,HH]=ndgrid(q_candidates,H_candidates);

% transition function
transfunc=  (reshape(repmat(eye(length(q_candidates)),1,length(H_candidates)),length(q_candidates),length(q_candidates),length(H_candidates))... % p(pL(t)| no jump occurred) * p(no jump occurred)
    .* permute(reshape(repmat(1-H_candidates,length(q_candidates),length(q_candidates)),length(H_candidates),length(q_candidates),length(q_candidates)),[2 3 1]))...
    + ((ones(length(q_candidates),length(q_candidates),length(H_candidates))./length(q_candidates))... % + p(pL(t)| jump occurred) * p(jump_occurred)
    .* permute(reshape(repmat(H_candidates,length(q_candidates),length(q_candidates)),length(H_candidates),length(q_candidates),length(q_candidates)),[2 3 1]));

prior_p_qH=NaN(size(qq,1),size(qq,2),100); %initiate prior and post matrices
post_p_qH=NaN(size(qq,1),size(qq,2),100);
% uniform prior to start with
prior_p_qH(:,:,1)=ones(size(qq))./length(qq(:));

%% fit the model
for i=1:length(y)
    
    % 'leak' or apply transition function
    if i>1
        unpacked_post=permute(reshape(repmat(post_p_qH(:,:,i-1),1,length(q_candidates)),length(q_candidates),length(H_candidates),length(q_candidates)),[1 3 2]);
        unpacked_prior=unpacked_post.*transfunc;
        prior_p_qH(:,:,i)=squeeze(sum(unpacked_prior,1));
        prior_p_qH(:,:,i)=prior_p_qH(:,:,i)./sum(sum(prior_p_qH(:,:,i)));
    end
    
    % p(y|q,H)
    if y(i)==1
        % if y(i) is a hit, p(y(i)|q) is simply q
        py_given_qH=qq;
    else
        % if y(i) is a miss, p(y(i)|q) is 1-q
        py_given_qH=1-qq;
    end
    
    % Bayes' theorem: p(q,H|y_1:i)=p(y|q_i,H)p(q_1:i-1,H);
    post_p_qH(:,:,i)=py_given_qH.*prior_p_qH(:,:,i);
    
    %normalise posterior
    post_p_qH(:,:,i)=post_p_qH(:,:,i)./sum(sum(post_p_qH(:,:,i)));
    
    % get joint maximum of prior for q and H
    [mx,ix]=max(myvect(prior_p_qH(:,:,i)));
    [ixq,ixH]=ind2sub(size(qq),ix);
    joint_mode(i,:)=[q_candidates(ixq), H_candidates(ixH)];
    
    % get marginal expected values for q and H
    Eq(i)=sum(myvect(prior_p_qH(:,:,i).*qq));
    EH(i)=sum(myvect(prior_p_qH(:,:,i).*HH));
  
end

end

function out = myvect(in)

out=in(:);

end