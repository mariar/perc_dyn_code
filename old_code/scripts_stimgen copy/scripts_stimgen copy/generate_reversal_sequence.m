%% generate a sequence of trials with reversals and switches in it

% sample syntax
%   [trls,trueprobs,ab,rel] = generate_reversal_sequence(...
%   1000, [0.15 0.35 0.65 0.85], 0.04, 0.05:0.1:0.95, 0.05:0.1:0.95 );

function [trls,trueprobs,ab,rel]=generate_reversal_sequence(ntrls,prbs,sw_prob,ab_levels,rel_levels,makeplot)

% ntrls=400;
% sw_prob=0.04;
% prbs=[0.15 0.35 0.65 0.85];

stop=0;
count=1;
pol = [1 1];
sw = 0;

while stop ~=0.5 || n_blocks < 3 || abs(pol(1)) > 0.2   % ensure balance of left and right trials
    
    sw=find(rand(ntrls,1)<sw_prob);
    
    n_blocks=length(sw);
    x = randi(length(prbs)-1,[1,n_blocks+1]);
    y = mod(cumsum(x),length(prbs))+1;
    block_seq = prbs(y);
    
    % get actual block values
    trueprobs=zeros(ntrls,1);
    
    sw=[1; sw; ntrls];
    for i=1:length(sw)-1
        trueprobs(sw(i):sw(i+1))=block_seq(i);
    end
    
    % get actual trial values
    trls = rand(ntrls,1)<trueprobs;
    stop = mean(trls);
    
    % check blocks do not have strong linear trend
    pol = polyfit(linspace(0,1,ntrls)',trls,1);
    
        count=count+1; % I just had this for debugging, not required now

       
end
% disp(count);


% plot
[Eq, EH] = fitbayes(trls);


%%
% turn sequence into 'full' sequence

% all possible values of 'ab' and 'rel'

% ab_levels=[0.05:0.1:0.95];
% rel_levels=[0.05:0.1:0.95];

% all possible combinations of those values
all_conds=CombVec(ab_levels,rel_levels)';

% how many conditions in total
n_conds=length(all_conds);

% how many repetitions per condition
n_reps_per_cond=ntrls./n_conds;

% create a sequence ntrls long
all_trials_unrandomised=repmat(all_conds,n_reps_per_cond,1);

% shuffle it
all_trials_randomised=all_trials_unrandomised(randperm(size(all_trials_unrandomised,1)),:);

% extract ab and rel
abvals=all_trials_randomised(:,1);
relvals=all_trials_randomised(:,2);

% fix floating point things
abvals=round(abvals,2);
relvals=round(relvals,2);
%% now assign the randomised trials to the conditions

% this involves indexing fun

% we have to make sure that the trials where rel (2nd col) < 0.5
% (i.e., coherence is mostly left) go to a trial coded '1'
% and rel > 0.5 go to trial coded '0'.

inds.trls_coded_0=trls==0;
inds.trls_coded_1=trls==1;
inds.rel_less_than_half=relvals<0.5;
inds.rel_more_than_half=relvals>0.5;

if ismember(0.5,rel_levels)
inds.rel_is_half=~inds.rel_less_than_half & ~inds.rel_more_than_half;

tmp=find(inds.rel_is_half);
tmp=tmp(randperm(length(tmp)));

inds.rel_less_than_half(tmp(1:length(tmp)/2))=1;
inds.rel_more_than_half(tmp(1+length(tmp)/2:end))=1;
end

ab=NaN(size(abvals));
rel=NaN(size(relvals));

rel(inds.trls_coded_0)=relvals(inds.rel_more_than_half);
rel(inds.trls_coded_1)=relvals(inds.rel_less_than_half);
% ab has to get swapped around too!
ab(inds.trls_coded_0)=abvals(inds.rel_more_than_half);
ab(inds.trls_coded_1)=abvals(inds.rel_less_than_half);


if makeplot
    figure;
    subplot(2,3,[1 2 4 5])
    plot(trueprobs, 'col', [0.3 0.3 0.8], 'LineWidth', 2)
    hold on
    hs = scatter(1:ntrls, trls);
    hs.SizeData = 24;
    hs.MarkerEdgeColor = [0.2 0.2 0.2];
    hs.MarkerFaceColor = [0.1 0.1 0.1];
    h = lsline;
    set(h, 'LineWidth', 2);
%     plot(smooth(trls,25));
    plot(Eq, 'col', [0.3 0.8 0.3], 'LineWidth', 2);
    plot(EH, 'col', [0.8 0.3 0.3], 'LineWidth', 2);
    set(gca,'YLim',[-0.1 1.1])
   
    subplot(2,3,[3 6])
    imagesc([zscore(ab) zscore(rel) zscore(Eq')]);colormap gray

end
