clear all;close all;clc

%%
a=repmat([0.3 0.7],4,1)
b=repmat([0.2;0.4;0.6;0.8],1,2)
%%
indir='/Users/marshall/Documents/MATLAB/meg_study_stimuli';
infile='test4.mat';

load(fullfile(indir,infile));

%%

sq=round(seq_props.ab' .* seq_props.rel' .* 100);

%%
ix=28;

vc=find(sq==ix);

vc=vc(1:3);

dots=RDK_coordinates_coh(vc);

newdots{1}(:,:,0+1:3:180)=dots{1}(:,:,1:60);
newdots{1}(:,:,1+1:3:180)=dots{2}(:,:,1:60);
newdots{1}(:,:,2+1:3:180)=dots{3}(:,:,1:60);

newdots{2}(:,:,0+1:3:180)=dots{1}(:,:,61:120);
newdots{2}(:,:,1+1:3:180)=dots{2}(:,:,61:120);
newdots{2}(:,:,2+1:3:180)=dots{3}(:,:,61:120);

newdots{3}(:,:,0+1:3:180)=dots{1}(:,:,120:179);
newdots{3}(:,:,1+1:3:180)=dots{2}(:,:,120:179);
newdots{3}(:,:,2+1:3:180)=dots{3}(:,:,120:179);

ix=14;

vc=find(sq==ix);

vc=vc(1:3);

dots=RDK_coordinates_coh(vc);

%%

RDK_coordinates_coh=newdots
RDK_coordinates_incoh=newdots

outfile='leaved.mat';

save(fullfile(indir,outfile),'RDK_coordinates_coh','RDK_coordinates_incoh','seq_props')
newdots{4}(:,:,0+1:3:180)=dots{1}(:,:,1:60);
newdots{4}(:,:,1+1:3:180)=dots{2}(:,:,1:60);
newdots{4}(:,:,2+1:3:180)=dots{3}(:,:,1:60);

newdots{5}(:,:,0+1:3:180)=dots{1}(:,:,61:120);
newdots{5}(:,:,1+1:3:180)=dots{2}(:,:,61:120);
newdots{5}(:,:,2+1:3:180)=dots{3}(:,:,61:120);

newdots{6}(:,:,0+1:3:180)=dots{1}(:,:,120:179);
newdots{6}(:,:,1+1:3:180)=dots{2}(:,:,120:179);
newdots{6}(:,:,2+1:3:180)=dots{3}(:,:,120:179);
