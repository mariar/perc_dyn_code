% just a sketch to illustrate how to produce a better pseudorandom sequence
% of pings and normal trials

ntrials=500;

npings=100;

rat=ntrials/npings;

disp(['for every ' num2str(rat) ' A-type trials there should be 1 B-type trial.'])

flo=2;
cei=2*rat-flo;

disp(['floor = ' num2str(flo) ', ceiling = ' num2str(cei) '.']);

%
goodseq=0;

tic
while goodseq==0
    
    pingpoints=randi(cei-flo+1,1,npings)+flo-1;
    
    trls=[];
    
    for i=1:length(pingpoints)
        trls=[trls zeros(1,pingpoints(i)) 1];
    end
    
    if sum(trls==0)==ntrials
        goodseq=1;
        figure;imagesc(trls);
    end
end
toc