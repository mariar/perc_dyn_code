function generate_seq_of_rdks(ntrials,nblocks,npings,AB,REL,block_prbs,switchprob,framerate,displayparms,outdir,outfile)


%% input parms for testing only
%ntrials=240;
%AB=[0.1 0.3 0.7 0.9];
%REL=[0.1 0.3 0.7 0.9];
%block_prbs=[0.15 0.35 0.65 0.85];
%switchprob=0.04;

%outdir='~/Dropbox/rdk from pc/seq';
%outfile='test1.mat';

% ntrials=80;
% nblocks=4;
% AB=[0.4 0.6];
% REL=[0.1 0.3 0.7 0.9];
% block_prbs=[0.15 0.85];
% switchprob=0.04;

ntpb = ntrials/nblocks;

if floor(ntpb)~=ntpb
    disp('ntrials / nblocks is not an integer. Fix')
    return
end

%% call subfunction to create the sequence itself

[trls,trueprobs_nonpings,ab_nonpings,rel_nonpings] = generate_reversal_sequence(...
                        ntrials,block_prbs,switchprob,AB,REL,1);

%% intersperse some pings into the sequence
% (we're not doing pings any more... this can be ignored)

% gen seqs at random but ensuring pings don't occur too close together
if npings>0
    ping_idx = intersperse_pings(ntrials,npings,2);   
else
    ping_idx = zeros(ntrials,1);    
end
ping_idx = logical(ping_idx);

%%

ab=zeros(1,npings+ntrials);

ab(~ping_idx)=ab_nonpings;
ab(ping_idx)=NaN;


rel=zeros(1,npings+ntrials);
rel(~ping_idx)=rel_nonpings;
rel(ping_idx)=NaN;

trueprobs=zeros(1,npings+ntrials);
trueprobs(~ping_idx)=trueprobs_nonpings;
trueprobs(ping_idx)=NaN;

nppb=npings/nblocks;

%% parameters for totally random motion before coherent motion begins

% possible_incoh_durations_for_nonpings=[1:0.1:2];
possible_incoh_durations_for_nonpings = [1];

possible_incoh_durations_for_pings = [1.5:0.1:2]; %ping should always be a bit later

for i = 1:ntrials
    tmp = randperm(length(possible_incoh_durations_for_nonpings));
    incoh_durations_nonpings(i) = possible_incoh_durations_for_nonpings(tmp(1));
end

incoh_durations_pings=[]; %initialise as empty so something can be passed in the case that it has zero length
for i = 1:npings
    tmp = randperm(length(possible_incoh_durations_for_pings));
    incoh_durations_pings(i) = possible_incoh_durations_for_pings(tmp(1));
end


incoh_durations             = zeros(1,ntrials+npings);
incoh_durations(ping_idx)   = incoh_durations_pings;
incoh_durations(~ping_idx)  = incoh_durations_nonpings;

%% generate a bunch of RDKs from those parms
%fixed dot parameters
dots = [];
dots.params.fieldShape      = 1; % 0~1 : Spare factor, 1~ : Square-like field
dots.params.nDots           = 100; % number of dots
dots.params.dotLifeOption   = 'N'; % 'N' for normal dist, 'U' for uniform dist
dots.params.dotLife         = [12,4];
%% THESE PARAMETERS NEED TO CHANGE ACCORDING TO DISPLAY PARAMETERS!

% framerate is different in different labs
% MEG is 120Hz, Booth 100Hz
dots.params.frameRate = framerate; 

% old way specifying things in pixels
% dots.params.fieldSize = [300 300];
% dots.params.speed = 60; % Pixel per second

% new way with dva
pix_per_degree = metpixperdeg(  displayparms.monitor_width_in_mm,...
                                displayparms.monitor_horiz_res, ...
                                displayparms.sub_mon_dist_in_mm);

dots.params.fieldSize_in_dva    = [4.2 4.2];
dots.params.speed_in_dva        = 0.8;

dots.params.fieldSize           = ceil(dots.params.fieldSize_in_dva * pix_per_degree);
dots.params.speed               = ceil(dots.params.speed_in_dva * pix_per_degree);

%%
% make all the RDKs and store
tic
for i=1:ntrials+npings
    disp(i);
    dots.params.coherence.rel       = rel(i); % 0~1 : Proportion of dots coherens in ori1 vs ori2 (1 = all ori1, 0.5 = even, 0 = all ori2)
    dots.params.coherence.abs       = ab(i); % 0~1 : Proportion of total coherent dots (in both directions)
    
    dots.params.duration_incoh      = incoh_durations(i); % variable duration of incoherent rdk before trial begins / ping happens
    dots.params.nFrames_incoh       = dots.params.frameRate * dots.params.duration_incoh;
    
    dots.params.isping = ping_idx(i);
    
    % switch parameters of coherent rdk depending on if ping or not
    % (after all, the pings don't have any coherent motion!)
    if dots.params.isping
        dots.params.duration_coh    = 0;
        dots.params.nFrames_coh     = 0;
        dots.params.ori1            = NaN; % 0~360 : Orientation
        dots.params.ori2            = NaN; % 0~360 : Orientation
    else
        dots.params.duration_coh    = 2.5;
        dots.params.nFrames_coh     = dots.params.frameRate * dots.params.duration_coh;
        dots.params.ori1            = 90;  % 0~360 : Orientation
        dots.params.ori2            = 270; % 0~360 : Orientation
    end
    
    [RDK_coordinates_incoh{i},RDK_coordinates_coh{i},dots_incoh,dots_coh] = tom_gen_one_RDK_meg(dots);
end
toc

%% put together other output

seq_props                       = [];
seq_props.ab                    = ab;
seq_props.rel                   = rel;
seq_props.ntrials               = ntrials;
seq_props.ntrials_inc_pings     = ntrials+npings;
seq_props.nblocks               = nblocks;
seq_props.ntpb                  = ntpb;
seq_props.ntpb_inc_pings        = ntpb+nppb;
seq_props.ab_levels             = AB;
seq_props.rel_levels            = REL;
seq_props.trls                  = trls;
seq_props.trueprobs             = trueprobs;
seq_props.pings                 = ping_idx;
seq_props.frameRate             = dots.params.frameRate;
seq_props.fieldSize             = dots.params.fieldSize;

%% add a column indicating what feedback should be give

% this is not trivial
% some trials have equal left and right dots so fb should reflect
% the 'true' block probability
% when left motion = right motion and true block prob = 0.5 then feedback
% should be random
% whatever it is, it needs to be recorded

seq_props.fb = NaN(size(seq_props.rel));

% the easy case, when left motion > or < right motion
seq_props.fb(seq_props.rel<0.5) = 0;
seq_props.fb(seq_props.rel>0.5) = 1;

% when left motion = right motion, reflect true block prob
try
    eq_trls=find(seq_props.rel==0.5);
    seq_props.fb(eq_trls)=rand(1,length(eq_trls)) <= seq_props.trueprobs(eq_trls);
catch
    % no 0.5 trials, doesn't matter
end

%% last but not least, create a tag

% We'll save this tag in the stimulus file, and send it as a set of
% trigers at the start of the MEG file.

% This is to make sure we will always be able to match a given stimulus
% sequence to a given MEG file. It's the sort of thing you can easily mix
% up...


nTags=4;

for i=1:nTags
    tag(i)=100+round(rand*100);
end

%% save matfile

save(fullfile(outdir,outfile),'RDK_coordinates_incoh','RDK_coordinates_coh','seq_props','tag');
disp('saved!');