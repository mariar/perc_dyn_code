%% GENERATE LOTS OF RDKs

function generate_lots_of_rdks_for_meg_exp(ntrials,npings,nblocks,AB,REL,outdir,outfile)

addpath(genpath('/Users/marshall/Documents/MATLAB/meg_study/'));

%% user-specified (uncomment for debugging)

% outdir='C:\Users\Tom\Documents\MATLAB\rdk\mod\stim';
% outfile='shortlife.mat';

%%% stimulus properties

% ntrials=432;  % total number of trials in the experiment
% nblocks=6;
% AB=[0.25 0.5 0.75];   % different levels of absolute coherence
% REL=[0.9 0.8 0.7];    % different levels of relative coherence

ntrials=32;
nblocks=2;
AB=[0.6 0.8];
REL=[0.6 0.8];
npings=10;

%% set up stimulus sequence, blocks, trials etc

ntpb=ntrials/nblocks;

nppb=npings/nblocks;

% we've specified relative coherence, but now we need to take into account
% the fact that '90% dots go right', is different from '90% dots go left',
% which of course is the same is '10% dots go right'. So we make a new
% vector which - if it previously included 0.9 - now includes 0.1 and 0.9.
REL_for_calc=sort([1-REL REL]);

nconds=length(REL_for_calc)*length(AB);

nreps_of_cond_per_block=ntpb/nconds;

nreps_total=ntrials/nconds;

if round(ntpb)~=ntpb
    disp('n trials per block is not an integer.');
    disp('Choose better values for ntrials and nblocks');
elseif round(nreps_of_cond_per_block)~=nreps_of_cond_per_block
    disp('Can''t fit an integer of each condition into a block. Change something.');
else
    disp([num2str(nconds) ' conditions, each repeated ' num2str(nreps_of_cond_per_block) ' times per block,']);
    disp(['over ' num2str(nblocks) ' blocks, giving ' num2str(ntrials) ' trials. ntpb = ' num2str(ntpb)]);
    disp(['plus ' num2str(npings) ' pings, so ' num2str(nppb) '/block, total blocklength ' num2str(ntpb+nppb) ', total trials ' num2str(ntrials+npings) '.']);
end

all_condtypes=CombVec(AB,REL_for_calc);

alltrials=repmat(all_condtypes,1,nreps_total);

shuftrials=alltrials(:,randperm(size(alltrials,2)));

ab=shuftrials(1,:);
rel=shuftrials(2,:);

%% parameters for totally random motion before coherent motion begins

possible_start_latencies=[1:0.1:3];

for i=1:ntrials
    tmp=randperm(length(possible_start_latencies));
    start_latencies(i)=possible_start_latencies(tmp(1));
end

start_latencies_in_frames=round(60*start_latencies);

%% generate a bunch of RDKs from those parms
%fixed dot parameters
dots = [];
dots.params.fieldSize = [300 300];
dots.params.fieldShape = 1; % 0~1 : Spare factor, 1~ : Square-like field
dots.params.nDots = 100; % number of dots
% dots.params.coherence.ab = .6; % 0~1 : Proportion of total coherent dots (in both directions)
% dots.params.coherence.rel = 0.6; % 0~1 : Proportion of dots coherens in ori1 vs ori2 (1 = all ori1, 0.5 = even, 0 = all ori2)

% dots.params.speed = 100; % Pixel per second
dots.params.speed = 60;

dots.params.duration = 3; %
dots.params.ori1 = 90; % 0~360 : Orientation
dots.params.ori2 = 270;
dots.params.frameRate = 60;
dots.params.dotLifeOption = 'N'; % 'N' for normal dist, 'U' for uniform dist
% dots.params.dotLife = [50, 5]; % frames, [ Mean, SD ] of dots' life, SD is optional for 'N'
% dots.params.dotLife = [18, 6]; % 18 frames = 300ms. Most dots have life of 200ms - 400mss
dots.params.dotLife = [6,3];

nFrames = dots.params.frameRate * dots.params.duration;

% make all the RDKs and store
tic
for i=1:ntrials
    disp(i);
    dots.params.coherence.rel=rel(i);
    dots.params.coherence.abs=ab(i);
    dots.params.coherent_motion_onset_in_frames=start_latencies_in_frames(i);
    [RDKcoordinates{i},dots] = tom_gen_one_RDK_meg(dots);
end
toc

%% put together other output

dotProps.nFrames=start_latencies_in_frames+nFrames;
dotProps.start_latencies_in_frames=start_latencies_in_frames;
dotProps.ab=ab;
dotProps.rel=rel;
dotProps.frameRate=dots.params.frameRate;
dotProps.fieldSize=dots.params.fieldSize;
dotProps.ntrials=ntrials;
dotProps.nblocks=nblocks;
dotProps.ntpb=ntpb;
dotProps.nconds=nconds;
dotProps.nreps_of_cond_per_block=nreps_of_cond_per_block;
dotProps.ab_levels=AB;
dotProps.rel_levels=REL_for_calc;

%% save matfile

save(fullfile(outdir,outfile),'RDKcoordinates','dotProps');
disp('saved!');