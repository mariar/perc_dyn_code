function [epoched , saccdir, rt, corans, acc, ab rel] = eyetracker_quick_and_dirty_analysis(fulldir,flz)

% subid = 's111';
% session='practice';

%% path
addpath('/Users/marshall/data/perc_dyn/piloting');
addpath('/Users/marshall/data/perc_dyn');


%% load everything into memory

%     infile=[stem num2str(xten(i)) suff];
[rt,saccdir,epoched,blinktrials,bl,tr,ab,rel,tpb] = convert_eyelink_output_to_behavdat(fulldir,flz,0,[-50 950],40);


%% detect outliers

% faster than 150ms = too fast
badtrials.toofast=find(rt<150);

mnrt=nanmean(rt);
strt=nanstd(rt);

% 3 standard deviations above mean rt = too slow
badtrials.tooslow=find(rt>mnrt+(3*strt));

badtrials.nort = find(isnan(rt));

% get them all
badtrials.all=sort([badtrials.toofast; badtrials.tooslow; badtrials.nort]);

% create a vector to 'flag' trials as bad
badidx = zeros(size(rt));
badidx(badtrials.all) = 1;

%% compute corans

% corans=rel<0.5;

corans=repmat(NaN,length(ab),1);
corans(rel<0.5)=0;
corans(rel>0.5)=1;

% acc=saccdir==corans;
acc=repmat(NaN,length(ab),1);
acc(corans == 0 & saccdir ==0)=1;
acc(corans == 1 & saccdir ==1)=1;
acc(corans == 1 & saccdir ==0)=0;
acc(corans == 0 & saccdir ==1)=0;
% NB: The above way allows for 'correct', 'error', and 'no answer is
% correct'(when rel = 0.5);

%% cannot compute prior as feedback is in stimulus file

%% extract summary data by level

ab_levels=unique(ab);
rel_levels=unique(rel);

for i=1:length(ab_levels)
    for j=1:length(rel_levels)
        inds= ab==ab_levels(i) & rel==rel_levels(j);
        try
        rt_bycond(i,j)=nanmean(rt(find(inds)));
        end
        acc_bycond(i,j)=nanmean(acc(find(inds)));
        saccdir_bycond(i,j)=nanmean(saccdir(find(inds)));
    end
end

%% plot summary data

figure;
try
subplot(1,3,1),imagesc(rel_levels,ab_levels,rt_bycond);colormap gray
title('saccade rt');
ylabel('absolute coherence');
xlabel('relative coherence');
colorbar
% set(gca,'CLim',[300 350]);
axis xy
end

subplot(1,3,2),imagesc(rel_levels,ab_levels,saccdir_bycond);colormap gray
title('p(sacc right)');
ylabel('absolute coherence');
xlabel('relative coherence');
colorbar
set(gca,'CLim',[0 1]);
axis xy

subplot(1,3,3),imagesc(rel_levels,ab_levels,acc_bycond);colormap gray
title('accuracy');
ylabel('absolute coherence');
xlabel('relative coherence');
colorbar
set(gca,'CLim',[0.5 1]);
axis xy

%% display summary output

disp(['accuracy on this block was ' num2str(100*nanmean(acc)) '%.']);