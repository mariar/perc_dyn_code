function [trl_interped,foo]=interp_over_blink(trl,pad,zeroval,makeplot)

% find the blink values
blinkidx=find(trl==zeroval);

% add a pad to the front and back
blinkidx=[min(blinkidx)-pad max(blinkidx)+pad];

% make sure these don't go over the beginning and end of the window
if blinkidx(1)<1
    blinkidx(1)=1;
end

if blinkidx(2)>length(trl)
    blinkidx(2)=length(trl);
end

% nan the blink out
trl(blinkidx(1):blinkidx(2))=NaN;

% interpolate over the gap
[trl_interped,foo]=fillmissing(trl,'linear','SamplePoints',1:length(trl));

if makeplot
    figure
    plot(trl_interped)
    hold on
    plot(find(foo),trl_interped(foo),'r')
end

foo=NaN;

if min(trl_interped) == zeroval
    [trl_interped,foo] = interp_over_blink(trl_interped,pad,zeroval,makeplot);
end
% determine whether there are zerovals left in the trial, if not call
% interp_over_blink again


% determine whether the blink occured during the saccade
% blink_gap=diff(trl(blinkidx+ [-1 1]));