function [rt,saccdir,epoched,blinktrials,bl,tr,ab,rel,tpb]=convert_eyelink_output_to_behavdat(indir,infile,makeplot,wind,pad)

%% options for blink removal (could turn into function arguments)

zeroval=-32768; % 'zero' value of x and y position; where the signal goes when the eye is lost (e.g., during a blink)

% pad=40; % number of timepoints to cut out around a detected blink

% wind=[-99 1000];

tidx = wind(1):1:wind(2);
%% import file

% indir='/Users/marshall/data/percdes/piloting';

% infile='eye_p3.edf';

edf0 = Edf2Mat(fullfile(indir,infile),0);

%% find relevant events

stringtofind='resp'; % onset of response period (ie time when saccade might occur)

% anonymous functions ftw!
F = cellfun(@(x) strfind(x,stringtofind), edf0.Events.Messages.info, 'UniformOutput', false);
F = cellfun(@(x) ~isempty(x), F);
resp_events=find(F);clear F

resp_onsets=edf0.Events.Messages.time(resp_events);
for i = 1:length(resp_onsets)
    resp_onsets_samples(i)=find(edf0.Samples.time == resp_onsets(i));
    [bl(i),tr(i),ab(i),rel(i),tpb(i)] = parse_eyetracker_msg(edf0.Events.Messages.info{resp_events(i)});
end

% columns
bl=bl';
tr=tr';
ab=ab';
rel=rel';
tpb=tpb';
%% get stimulus information from msges


%% play at plotting some individual trials

if makeplot
    try % data file may have <10 trials in it
        origin=[900 900 530 530];
        box=[420 560 -70 70];
        
        trlz=1:1:10;
        
        figure;
        
        for i = 1:10
            trl=trlz(i);
            toi=resp_onsets_samples(trl)-100:resp_onsets_samples(trl)+1000;
            toplot=[edf0.Samples.posX(toi); edf0.Samples.posY(toi)];
            subplot(5,2,i),sacc_plot(toplot);
            set(gca,'XLim',[900 900] + [-600 600]);
            set(gca,'YLim',[530 530] + [-100 100]);
            
            square(origin+box);
            square(origin-box);
        end
    catch
        disp('<10 trials. cannot plot individual trials.');
    end
end

%% epoch data using those markers

epoched=[];
for i = 1:length(resp_onsets_samples)
    rsp=resp_onsets_samples(i);
    thiswind=[rsp rsp ] + wind;
    epoched.px(i,:)=edf0.Samples.px(thiswind(1):thiswind(2));
    epoched.py(i,:)=edf0.Samples.py(thiswind(1):thiswind(2));
    epoched.posX(i,:)=edf0.Samples.posX(thiswind(1):thiswind(2));
    epoched.posY(i,:)=edf0.Samples.posY(thiswind(1):thiswind(2));
end

%% find trials that contain blinks

for i = 1:size(epoched.px, 1)
    blinktrials(i) = sum(epoched.px(i,:) == zeroval) > 0;
end
blinktrials = find(blinktrials);

for i = 1:size(epoched.px, 1)
    if ismember(i, blinktrials)
        [epoched.px_interped(i,:)] = interp_over_blink(epoched.px(i,:), pad, zeroval, 0);
        [epoched.py_interped(i,:)] = interp_over_blink(epoched.py(i,:), pad, zeroval, 0);
    else
        
        epoched.px_interped(i,:) = epoched.px(i,:);
        epoched.py_interped(i,:) = epoched.py(i,:);
    end
end

%% plot interpolated data to see if everything looks sane

if makeplot
    figure;
    subplot(2,1,1), plot(epoched.px_interped'); ylabel('xpos'); xlabel('time');
    subplot(2,1,2), plot(epoched.py_interped'); ylabel('ypos'); xlabel('time');
end


%% pofsX and posY are already NaNned out, so can be interped too

% disp('stop');

% for i = 1:size(epoched.posX,1)
%     trl=epoched.posX(i,:);
%     [epoched.posX_interped(i,:)]=fillmissing(trl,'linear','SamplePoints',1:length(trl));
%     
%     trl=epoched.posY(i,:);
%     [epoched.posY_interped(i,:)]=fillmissing(trl,'linear','SamplePoints',1:length(trl));
% end

wnd = 10;
for j = 1:size(epoched.posX, 1)
    trl = epoched.posX(j,:);
    nans = find(isnan(trl));
    
    tmp = [];
    for i=1:length(nans)
        tmp(i,:) = [(nans(i) - wnd):1:(nans(i) + wnd)];
    end
    
    newnans = unique(tmp);
    newnans(newnans < 1) = [];
    newnans(newnans > numel(trl)) = [];
    
    epoched.posX_interped(j,:) = epoched.posX(j,:);
    epoched.posX_interped(j, newnans) = NaN;
    epoched.posY_interped(j,:) = epoched.posY(j,:);
    epoched.posY_interped(j, newnans) = NaN;
end
%% saccade direction and rt
toi = [500 950];
toi_idx = [find(tidx == toi(1)):find(tidx == toi(2))];
% currently the method is basically, 'which one of these two areas on the x-axis gets entered first?'

% init
zl=NaN(size(epoched.posX,1),1);
zr=NaN(size(epoched.posX,1),1);

% define areas on x-axis
% leftbox=[1320 1460]; % x position only for now
% rightbox=[340 480]; % x position only for now

% estimate leftbox and rightbox from the data by modelling it as the sum of
% three gaussians (eye positions at fixation, left and right)
tmpdata = myvect(epoched.posX(:,toi_idx));
gaussfit = fitgmdist(tmpdata,3);
n_sds = [-3 3];

% get the means and standard deviations for the saccade positions
mu = gaussfit.mu;
sd = myvect(gaussfit.Sigma) .^ 0.5;

% sort these into a sensible order (left, middle, right)
[~, q] = sort(mu);
mu = mu(q);
sd = sd(q);

leftbox = sort(mu(1) + (n_sds .* sd(1)));
rightbox = sort(mu(3) - (n_sds .* sd(3)));

% leftbox=[400 600];
% rightbox=[1300 1500];

% determine first time point (if any) that each zone is entered
for i = 1:size(epoched.posX,1)
    try
        zl(i)=min(find(epoched.posX_interped(i,:)>leftbox(1) & epoched.posX_interped(i,:)<leftbox(2)));
    end
end

for i = 1:size(epoched.posX,1)
    try
        zr(i)=min(find(epoched.posX_interped(i,:)>rightbox(1) & epoched.posX_interped(i,:)<rightbox(2)));
    end
end

% then rt is min of the two and direction is the index of the min
[rtdx,saccdir]=min([zl zr],[],2);

rt = repmat(NaN, size(rtdx));
for i=1:length(rtdx)
    try
        rt(i) = tidx(rtdx(i));
    catch
        rt(i) = NaN;
    end
end

% easier to have saccdir coded as 0 and 1, not 1 and 2
saccdir=saccdir-1;

% if there's no rt, there's also no dir
saccdir(isnan(rt))=NaN;

%%

epoched.leftbox = repmat(leftbox,size(epoched.px,1),1);
epoched.rightbox = repmat(rightbox,size(epoched.px,1),1);
%% alternative way to calculate saccdir and rt

% for i = 1:size(epoched.posX,1)
%     [saccdir(i), rt(i)] = saccade_dir_and_rt(epoched.posX_interped(i,:),tidx);
% end

%% plot all saccades and we can see if they are going the right way

% saccades_per_plot = 40;
% ntrials = length(saccdir);
% nplots = ceil(ntrials / saccades_per_plot);
% 
% for i = 1:nplots
%     figure
%     for j=1:saccades_per_plot
%         subplot(5,8,j)
%         n = j+((i-1)*saccades_per_plot);
%         try
%             plot(epoched.posX_interped(n,:));
%             title([num2str(n) '   [' num2str(saccdir(n)) ']']);
%         end
%     end
% end
