% parse eyetracker output to get stimulus info
function [bl,tr,ab,rel,tpb]=parse_eyetracker_msg(msg)

% msg=edf0.Events.Messages.info{35};

strngs=[strfind(msg,'B') strfind(msg,'T') strfind(msg,'A') strfind(msg,'R') strfind(msg,'P') strfind(msg,':')];

bl=str2double(msg(strngs(1)+1:strngs(2)-1));
tr=str2double(msg(strngs(2)+1:strngs(3)-1));
ab=str2double(msg(strngs(3)+1:strngs(4)-1))/100;
rel=str2double(msg(strngs(4)+1:strngs(5)-1))/100;
tpb=str2double(msg(strngs(5)+1:strngs(6)-1))/100;
end