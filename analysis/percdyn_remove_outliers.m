clear all
close all

options = generate_perc_dyn_options();


beamformed = 0;
% flag for beamformed data

if beamformed
    
    subjectList = [];
    runList = [];
    % load behavioural data
    load(options.paths.behaviouralData,'B');
    % check that behavioural data and Meg data match
    for subject = 1 : options.raw.numSubjects
        
        
        %
        % % specify one subject and run (you can adapt this to make a loop over all subjects and runs)% subid = 533;
        
        
        details = generate_perdyn_subject_details(subject,options);
        
        for run = 1:details.raw.subject.runs
            runid = details.raw.subject.runIDs(run);
            % load the run. This loads a fieldtrip strufcture 'ftData' and a bunch of regressors 'reg_xxx'
            clear data
            data = load(details.raw.datafile(run).run);
            [subjectList, runList] =  match_behav_MEG_data(subjectList,runList,B,options,runid,data)
            
            % do some outlier / bad trial detection
        end
        
    end
end


for subject = 1 : options.raw.numSubjects
    
    
    %
    % % specify one subject and run (you can adapt this to make a loop over all subjects and runs)% subid = 533;
    
    
    details = generate_perdyn_subject_details(subject,options);
    
    for run = 1:details.raw.subject.runs
        
        runid = details.raw.subject.runIDs(run);
        idxRun = (runid-1) * 150 +1;
        % load the run. This loads a fieldtrip strufcture 'ftData' and a bunch of regressors 'reg_xxx'
        clear data
        if beamformed
            data = load(details.raw.beamformed.datafile(run).run);
        else
            data = load(details.raw.channel.datafile(run).run);
        end
        
        
        
        if beamformed
            data.reg_fb = B{options.BehaviourMatch(subject)}.fb(idxRun: idxRun + 149);
        end
        
        
        data.reg_correctAnswers = zeros(150,1);
        
        correctRight = data.reg_saccdir == 2 & data.reg_fb == 1;
        correctLeft  = data.reg_saccdir == 1 & data.reg_fb == 0;
        
        
        % prepare possible prior regressors
        data.reg_correctAnswers(correctRight|correctLeft) = 1;
        
        data.reg_absoluted_prior_diff_to_0 = [abs(data.reg_pri(1));diff(abs(data.reg_pri-0.5))];
        
        
        %%%%%%%%%%%%%%%%%%
        prior = diff(data.reg_pri-0.5);
        
        idxSigned = sign(prior);
        
        idxNegative = idxSigned(1:end-1)==0 & idxSigned(2:end)==0;
        idxSwitch = idxSigned(1:end-1)==0 & idxSigned(2:end)==1;
        
        prior(idxNegative) = prior(idxNegative) .* -1;
        prior(idxSwitch) = prior(idxSwitch) .* -1;
        
        data.reg_signed_prior_diff_to_0 = [abs(data.reg_pri(1));prior];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        prior = data.reg_pri-0.5;
        
        idxSigned = sign(prior);

        idxSwitch1 = idxSigned(1:end-1)==0 & idxSigned(2:end)==1;
        idxSwitch2 = idxSigned(1:end-1)==1 & idxSigned(2:end)==0;
        
        
        switchReg = idxSwitch1+idxSwitch2;
        
        if any(switchReg == 2) 
            
            keyboard; 
            
        end 
        
        data.reg_switchprior = [0;switchReg];
        
        
        % initialise
        badTrials           = [];
        
        % look at reg_rt (reaction times)
        % if rt is nan, the subject didn't make an eye movement. We don't want those trials
        badTrials.rtIsNan   = isnan(data.reg_rt);
        
        badTrials.corransIsNan = isnan(data.reg_correctAnswers);
        badTrials.feedback = isnan(data.reg_fb);
        % also if the rt is faster than some cutoff we can discard as it is too fast
        
        badTrials.rtTooFast = data.reg_rt < options.rtCutoff;
        
        % and if the saccade direction is nan, also subj made no eye movement
        badTrials.saccIsNaN = isnan(data.reg_saccdir);
        
        % finally, if there is no MEG data (e.g., because MaxFilter removed it) we want to mark that trial as bad
        % this code might be a bit tricky to understand. If you're curious, look up 'anonymous functions'.
        badTrials.noMegData = cellfun(@(x) any(any(isnan(x))), data.ftData.trial)';
        
        % maybe there are other reasons you want to discard a trial
        % if there are, you can add them here
        
        % once we have all the possible reasons for discarding a trial,
        % we want one logical vector that says 'is there ANYTHING bad about this trial?'
        
        badTrials.anyReason = any([badTrials.rtIsNan badTrials.rtTooFast badTrials.saccIsNaN badTrials.noMegData badTrials.corransIsNan badTrials.feedback],2);
        
        % indices of the GOOD trials that we want to keep
        goodTrials  = find(~badTrials.anyReason);
        
        % select the good trials only from the data structure
        % note: I'm over-writing the old structure to save memory, you might want
        % to create a new structure. That's up to you
        
        
        
        
        
        cfg         = [];
        cfg.trials  = goodTrials;
        data.ftData      = ft_selectdata(cfg,data.ftData);
        [data.ftData] = perc_dyn_combine_planar(data.ftData);
        
        % zscore data because eye blink results for single subjects has
        % shown that the pwr spectrum is up to a scale different between
        % different subjects
        
        [data.ftData] = perc_dyn_zscore(data.ftData);
        
        
        % then keep only the goodtrials in the regressors too
        data.reg_ab      = data.reg_ab(goodTrials);
        data.reg_rt      = data.reg_rt(goodTrials);
        data.reg_saccdir = data.reg_saccdir(goodTrials);
        data.reg_fbPos   = data.reg_fbPos(goodTrials);
        data.reg_lh      = data.reg_lh(goodTrials);
        data.reg_MLE_prior = data.reg_MLE_prior(goodTrials);
        data.reg_MLE_withinTrial_posterior = data.reg_MLE_withinTrial_posterior(goodTrials);
        data.reg_pri     = data.reg_pri(goodTrials);
        data.reg_pri2    = data.reg_pri2(goodTrials);
        data.reg_pris    = data.reg_pris(goodTrials);
        data.reg_rel     = data.reg_rel(goodTrials);
        data.reg_relf    = data.reg_relf(goodTrials);
        data.reg_surp    = data.reg_surp(goodTrials);
        data.reg_trn     = data.reg_trn(goodTrials);
        data.reg_wtPos   = data.reg_wtPos(goodTrials);
        data.reg_correctAnswers = data.reg_correctAnswers(goodTrials);
        data.reg_feedback = data.reg_fb(goodTrials);
        data.reg_switchprior = data.reg_switchprior(goodTrials);
        data.reg_signed_prior_diff_to_0 = data.reg_signed_prior_diff_to_0(goodTrials);
        data.reg_absoluted_prior_diff_to_0 =data.reg_absoluted_prior_diff_to_0(goodTrials);
        
        keyboard;
        if beamformed
            
            save(details.badTrialsRemoved.beamformed.datafile(run).run,'data')
        else
            save(details.badTrialsRemoved.channel.datafile(run).run,'data')
        end
        
    end
end