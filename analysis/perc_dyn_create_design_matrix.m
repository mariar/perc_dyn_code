function cfg = perc_dyn_create_design_matrix(data,options,glmFlag,MEG,details,selectTrialIdx)

if MEG
    
    numRegressors = length(options.TrialGLM.(glmFlag).regressors);
    
    designMatrixAll = [];
    for run = 1:details.badTrialsRemoved.subject.runs
        
        numTrials = length(data{run}.ftData.trial);
        
        designMatrix = zeros(numTrials,numRegressors);
        label = cell(numRegressors,1);
        for regressor = 1:numRegressors
            
            regressorName = options.TrialGLM.(glmFlag).regressors(regressor).regressorName;
            
            switch regressorName
                
                
                case 'reg_rel_signed'
                    
                    designMatrix(:,regressor) = data{run}.reg_rel-0.5;
                    
                case 'reg_saccdir'
                    
                    designMatrix(:,regressor) = data{run}.reg_saccdir;
                    designMatrix(designMatrix(:,regressor)==1,regressor) = -1;
                    designMatrix(designMatrix(:,regressor)==2,regressor) = 1;
                    
                case 'reg_fb_signed'
                    
                    designMatrix(:,regressor) = data{run}.reg_feedback;
                    designMatrix(designMatrix(:,regressor)==0,regressor) = -1;
                    
                case 'reg_interaction_rel_abs'
                    
                    designMatrix(:,regressor) = (data{run}.reg_rel-0.5) .* data{run}.reg_ab;
                    %                     meanReg = mean(designMatrix(:,regressor));
                    %                     designMatrix(:,regressor) = designMatrix(:,regressor) - meanReg;
                    
                case 'reg_absoluted_scaled_prior'
                    
                    designMatrix(:,regressor) = abs(data{run}.reg_pri-0.5);
                    %                     meanReg = mean(designMatrix(:,regressor));
                    %                     designMatrix(:,regressor) = designMatrix(:,regressor) - meanReg;
                    %case 'reg_KL_divergence_prior'
                    
                case 'reg_const'
                    
                    designMatrix(:,regressor) = 1;
                    
                case 'reg_left_corr'
                    
                    designMatrix(data{run}.reg_correctAnswers==1 & data{run}.reg_saccdir==1,regressor) = 1;
                    
                case 'reg_right_corr'
                    
                    designMatrix(data{run}.reg_correctAnswers==1 & data{run}.reg_saccdir==2,regressor) = 1;
                    
                case 'reg_left_wrong'
                    
                    designMatrix(data{run}.reg_correctAnswers==0 & data{run}.reg_saccdir==1,regressor) = 1;
                    
                case 'reg_right_wrong'
                    designMatrix(data{run}.reg_correctAnswers==0 & data{run}.reg_saccdir==2,regressor) = 1;
                    
                case 'reg_corr_wrong'
                    designMatrix(data{run}.reg_correctAnswers==0, regressor) = -1;
                    designMatrix(data{run}.reg_correctAnswers==1, regressor) =  1;
                    
                case 'reg_right_left'
                    
                    designMatrix(data{run}.reg_saccdir==2,regressor) = 1;
                    designMatrix(data{run}.reg_saccdir==1,regressor) = -1;
                    
                case 'reg_correct_incorrect_contrast'
                    
                    designMatrix(data{run}.reg_correctAnswers==1,regressor) = 0.1;
                    designMatrix(data{run}.reg_correctAnswers==0,regressor) = -0.1;
                    
                    
                    
                case 'reg_interaction_reg_abs_folded'
                    
                    designMatrix(:,regressor) = data{run}.reg_relf .* data{run}.reg_ab;
                    %                     meanReg = mean(designMatrix(:,regressor));
                    %                     designMatrix(:,regressor) = designMatrix(:,regressor) - meanReg;
                    
                case 'reg_signed_prior'
                    
                    designMatrix(:,regressor) = data{run}.reg_pri-0.5;
                    
                case 'reg_wtPos'
                    
                    designMatrix(:,regressor) = abs(data{run}.reg_wtPos-0.5);
                    
                case 'reg_reward_probability'
                    
                    designMatrix(:,regressor) = data{run}.reg_wtPos;
                    
                    designMatrix(data{run}.reg_saccdir==1,regressor) = 1 - data{run}.reg_wtPos(data{run}.reg_saccdir==1);
                    
                case 'reg_feedback_surprise'
                    
                    designMatrix(:,regressor) = data{run}.reg_pri-0.5;
                    
                    signedFeedback = data{run}.reg_feedback;
                    signedFeedback(data{run}.reg_feedback==0)= -1;
                    
                    designMatrix(:,regressor) = designMatrix(:,regressor) .* signedFeedback;
                    
                case 'reg_response_surprise'
                    
                    designMatrix(:,regressor) = data{run}.reg_pri-0.5;
                    
                    signedResponse = data{run}.reg_saccdir;
                    signedResponse(data{run}.reg_saccdir==1)= -1;
                    
                    designMatrix(:,regressor) = designMatrix(:,regressor) .* signedResponse;
                    
                    
                case 'reg_orthog_correct_incorr_rew_prob'
                    
                    rew_prob = data{run}.reg_wtPos;
                    
                    rew_prob(data{run}.reg_saccdir==1) = 1 - data{run}.reg_wtPos(data{run}.reg_saccdir==1);
                    
                    
                    corr_incorr_cont = zeros(numTrials,1);
                    corr_incorr_cont(data{run}.reg_correctAnswers==1) = 1;
                    corr_incorr_cont(data{run}.reg_correctAnswers==0) = -1;
                    
                    
                    %
                    %             betas = glmfit(rew_prob, corr_incorr_cont, 'normal');
                    %
                    %
                    %             dm = [ones(length(rew_prob),1),rew_prob];
                    %             fit = dm * betas;
                    %
                    %             residuals = corr_incorr_cont-fit;
                    %
                    %             designMatrix(:,regressor) = residuals;
                    %
                    
                    
                    betas = glmfit(rew_prob, corr_incorr_cont, 'normal','constant','off');
                    
                    
                    dm = rew_prob;
                    fit = dm * betas;
                    
                    residuals = corr_incorr_cont - fit;
                    
                    designMatrix(:,regressor) = residuals;
                    
                case 'reg_prior_signed_for_left'
                    
                    prior = data{run}.reg_pri;
                    response = data{run}.reg_saccdir;
                    
                    
                    designMatrix(:,regressor) = prior;
                    designMatrix(response == 1,regressor) = 1-prior(response == 1);
                    
                    
                    meanReg = mean(designMatrix(:,regressor));
                    designMatrix(:,regressor) = designMatrix(:,regressor) - meanReg;
                    
                case 'reg_interaction_abXrel_signed_for_left'
                    
                    Interaction = (data{run}.reg_rel-0.5) .* data{run}.reg_ab;
                    response = data{run}.reg_saccdir;
                    Interaction(response == 1) = -1.* Interaction(response==1);
                    
                    
                    designMatrix(:,regressor) = Interaction;
                    %meanReg = mean(designMatrix(:,regressor));
                    %designMatrix(:,regressor) = designMatrix(:,regressor) - meanReg;
                    
                    
                case 'reg_abXrelfolded_orthog'
                    
                    abXrel = data{run}.reg_relf .* data{run}.reg_ab;
                    betas_abxrel = glmfit([data{run}.reg_ab data{run}.reg_relf],abXrel);
                    
                    dm = [ones(length(data{run}.reg_ab),1) data{run}.reg_ab data{run}.reg_relf];%first column is for grand mean
                    
                    designMatrix(:,regressor) = abXrel - (dm*betas_abxrel);
                    
                otherwise
                    
                    
                    designMatrix(:,regressor) = data{run}.(regressorName);

            end
            
            label{regressor} = options.TrialGLM.(glmFlag).regressors(regressor).regressorLabel;
            
        end
        
        designMatrixAll = [designMatrixAll; designMatrix];
    end
    
    cfg = [];
    
    cfg.design = designMatrixAll(logical(selectTrialIdx),:);
    cfg.label       = label;
    cfg.parameter   = 'trial';
    
else
    
    numRegressors = length(options.behaviouralGLM.(glmFlag).regressors);
    
    
    lags = options.behaviouralGLM.lags;
    designMatrixAll = [];
    
    for run = 1:details.badTrialsRemoved.subject.runs
        regressorIdx = 1;
        numTrials = length(data{run}.ftData.trial);
        designMatrix = zeros(numTrials,20);
        for regressor = 1 : numRegressors
            
            
            regressorName = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorName;
            
            
            switch regressorName
                
                
                case 'reg_lagged_feedback'
                    
                    feedbackSigned = data{run}.reg_feedback;
                    feedbackSigned(feedbackSigned==0) = -1;
                    
                    for lagIdx = 1:lags
                        
                        designMatrix(:,regressorIdx) = [zeros(lagIdx,1); feedbackSigned(1:end-lagIdx)];
                        label{regressorIdx} = ['fb_t',num2str(lagIdx)];
                        regressorIdx = regressorIdx + 1;
                    end
                    
                    
                case 'reg_lagged_response'
                    
                    responseSigned = data{run}.reg_saccdir;
                    responseSigned(responseSigned==1) = -1;
                    responseSigned(responseSigned==2) = 1;
                    
                    
                    
                    for lagIdx = 1:lags
                        designMatrix(:,regressorIdx) = [zeros(lagIdx,1); responseSigned(1:end-lagIdx)];
                        label{regressorIdx} = ['RP_t',num2str(lagIdx)];
                        regressorIdx = regressorIdx + 1;
                    end
                    
                case 'reg_lagged_response_x_fb'
                    
                    responseSigned = data{run}.reg_saccdir;
                    responseSigned(responseSigned==1) = -1;
                    responseSigned(responseSigned==2) = 1;
                    
                    feedbackSigned = data{run}.reg_feedback;
                    feedbackSigned(feedbackSigned==0) = -1;
                    
                    
                    feedbackXresponse = feedbackSigned .* responseSigned;
                    
                    
                    for lagIdx = 1:lags
                        designMatrix(:,regressorIdx) = [zeros(lagIdx,1); feedbackXresponse(1:end-lagIdx)];
                        label{regressorIdx} = ['RpXFb_t',num2str(lagIdx)];
                        regressorIdx = regressorIdx + 1;
                    end
                    
                    
                case 'reg_signed_prior'
                    
                    designMatrix(:,regressorIdx) = data{run}.reg_pri-0.5;
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
                    
                    
                case 'reg_prior_x_rel'
                    
                    prior = data{run}.reg_pri;
                    rel =  data{run}.reg_rel-0.5;
                    designMatrix((prior > 0.5 & rel > 0),regressorIdx) = 1;
                    designMatrix((prior < 0.5 & rel < 0),regressorIdx) = -1;
                    
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
                    
                case 'reg_prior_x_ab'
                    
                    prior = data{run}.reg_pri-0.5;
                    ab =  data{run}.reg_ab;
                    
                    designMatrix(:,regressorIdx) = prior .* ab;
                    
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
                    
                case 'reg_prior_x_abs_x_rel'
                    
                    prior = data{run}.reg_pri-0.5;
                    ab = data{run}.reg_ab;
                    rel = data{run}.reg_rel-0.5;
                    abXrel = ab .* rel;
                    
                    designMatrix((prior > 0.5 & abXrel > 0),regressorIdx) = 1;
                    designMatrix((prior < 0.5 & abXrel < 0),regressorIdx) = -1;
                    
                    %designMatrix(:,regressorIdx) = prior .* abXrel;
                    
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
                    
                case 'reg_interaction_rel_abs'
                    
                    designMatrix(:,regressorIdx) = (data{run}.reg_rel-0.5) .* data{run}.reg_ab;
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
                    
                case 'reg_rel_signed'
                    
                    designMatrix(:,regressorIdx) = data{run}.reg_rel-0.5;
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
                    
                case 'reg_abXrel_lagged'
                    
                    Interaction = (data{run}.reg_rel-0.5) .* data{run}.reg_ab;
                    
                    designMatrix(:,regressorIdx) = Interaction;
                    label{regressorIdx} = ['RELxAbs_t',num2str(0)];
                    regressorIdx = regressorIdx + 1;
                    
                    designMatrix(:,regressorIdx) = [ 0; Interaction(1:end-1)];
                    label{regressorIdx} = ['RELxAbs_t',num2str(-1)];
                    regressorIdx = regressorIdx + 1;
                    
                    case 'reg_rel_lagged'
                    
                        rel = data{run}.reg_rel-0.5;
                        designMatrix(:,regressorIdx) = rel;
                    label{regressorIdx} = ['REL_t',num2str(0)];
                    regressorIdx = regressorIdx + 1;
                    
                    designMatrix(:,regressorIdx) = [ 0; rel(1:end-1)];
                    label{regressorIdx} = ['REL_t',num2str(-1)];
                    regressorIdx = regressorIdx + 1;
                        
                        case 'reg_ab_lagged'
                            
                            
                         absC = data{run}.reg_ab-0.5;
                        designMatrix(:,regressorIdx) = absC;
                    label{regressorIdx} = ['AB_t',num2str(0)];
                    regressorIdx = regressorIdx + 1;
                    
                    designMatrix(:,regressorIdx) = [ 0; absC(1:end-1)];
                    label{regressorIdx} = ['AB_t',num2str(-1)];
                    regressorIdx = regressorIdx + 1;
                    
                otherwise
                    designMatrix(:,regressorIdx) = data{run}.(regressorName);
                    meanReg = mean(designMatrix(:,regressor));
                    designMatrix(:,regressor) = designMatrix(:,regressor) - meanReg;
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
            end
            
        end
        
        designMatrixAll = [designMatrixAll; designMatrix];
        
        
    end
    
    cfg.design = designMatrixAll(:,1:regressorIdx-1);
    cfg.label = {'constant',label{:}};
    
end



end