function [selectedData,selectTrialIdx ]  =  perc_dyn_select_correct_incorrect_trials(data,appendedData,details,selectFlag,selectTypeFlag)

selectTrialIdx = [];

for run = 1:details.badTrialsRemoved.subject.runs
    
    numTrials = length(data{run}.ftData.trial);
    
    if selectFlag
        
        
        
        switch selectTypeFlag
            
            case 'correct'
                
                TrialSelected = data{run}.reg_correctAnswers == 1;
                
                
            case 'wrong'
                
                TrialSelected = data{run}.reg_correctAnswers == 0;
        end
        
        selectTrialIdx = [selectTrialIdx; TrialSelected];
        
        DesignMatirxSelectedTrials{run} = TrialSelected;
        
        
    else
        
        
        selectTrialIdx = [selectTrialIdx; ones(numTrials,1)];
         DesignMatirxSelectedTrials{run} = ones(numTrials,1);
    end
    
    
end


if selectFlag 
    
    
    cfg = []; 
    cfg.trials = logical(selectTrialIdx); 
    
    
    [selectedData] = ft_selectdata(cfg, appendedData);
else 
    
    selectedData = appendedData; 
end 



end