
clear all

options = generate_perc_dyn_options();
load(options.paths.behaviouralData);

B([23 24 25])=[];


for subject = 1:length(B)
    
    clear  Rightt  Leftt LLeftt LRightt
    
    
    
    Rightt(:,1) = [0; B{subject}.fb(1:end-1) == 1 & B{subject}.saccdir(1:end-1) == 1];
    Rightt(:,2) = [0; 0; B{subject}.fb(1:end-2) == 1 & B{subject}.saccdir(1:end-2) == 1];
    Rightt(:,3) = [0; 0; 0; B{subject}.fb(1:end-3) == 1 & B{subject}.saccdir(1:end-3) == 1];
    Rightt(:,4) = [0; 0; 0; 0; B{subject}.fb(1:end-4) == 1 & B{subject}.saccdir(1:end-4) == 1];
    Rightt(:,5) = [0; 0; 0; 0; 0; B{subject}.fb(1:end-5) == 1 & B{subject}.saccdir(1:end-5) == 1];
    
    
    
    Leftt(:,1) = [0; B{subject}.fb(1:end-1) == 0 & B{subject}.saccdir(1:end-1) == 0].*-1;
    Leftt(:,2) = [0; 0; B{subject}.fb(1:end-2) == 0 & B{subject}.saccdir(1:end-2) == 0].*-1;
    Leftt(:,3) = [0; 0; 0; B{subject}.fb(1:end-3) == 0 & B{subject}.saccdir(1:end-3) == 0].*-1;
    Leftt(:,4) = [0; 0; 0; 0; B{subject}.fb(1:end-4) == 0 & B{subject}.saccdir(1:end-4) == 0].*-1;
    Leftt(:,5) = [0; 0; 0; 0; 0; B{subject}.fb(1:end-5) == 0 & B{subject}.saccdir(1:end-5) == 0].*-1;
    
    
    LRightt(:,1) = [0; B{subject}.fb(1:end-1) == 1 & B{subject}.saccdir(1:end-1) == 0];
    LRightt(:,2) = [0; 0; B{subject}.fb(1:end-2) == 1 & B{subject}.saccdir(1:end-2) == 0];
    LRightt(:,3) = [0; 0; 0; B{subject}.fb(1:end-3) == 1 & B{subject}.saccdir(1:end-3) == 0];
    LRightt(:,4) = [0; 0; 0; 0; B{subject}.fb(1:end-4) == 1 & B{subject}.saccdir(1:end-4) == 0];
    LRightt(:,5) = [0; 0; 0; 0; 0; B{subject}.fb(1:end-5) == 1 & B{subject}.saccdir(1:end-5) == 0];
    
    
    
    LLeftt(:,1) = [0; B{subject}.fb(1:end-1) == 0 & B{subject}.saccdir(1:end-1) == 1].*-1;
    LLeftt(:,2) = [0; 0; B{subject}.fb(1:end-2) == 0 & B{subject}.saccdir(1:end-2) == 1].*-1;
    LLeftt(:,3) = [0; 0; 0; B{subject}.fb(1:end-3) == 0 & B{subject}.saccdir(1:end-3) == 1].*-1;
    LLeftt(:,4) = [0; 0; 0; 0; B{subject}.fb(1:end-4) == 0 & B{subject}.saccdir(1:end-4) == 1].*-1;
    LLeftt(:,5) = [0; 0; 0; 0; 0; B{subject}.fb(1:end-5) == 0 & B{subject}.saccdir(1:end-5) == 1].*-1;
    
    
    Y = B{subject}.saccdir;
    %X = [Rightt, Leftt, LRightt, LLeftt, B{subject}.rel B{subject}.ab];
    X = [Leftt, LLeftt, B{subject}.rel B{subject}.ab];
    %X = [Rightt, LRightt, B{subject}.rel B{subject}.ab];
    %X = [B{subject}.rel B{subject}.ab];
    
    idx = isnan(Y);
    
    Y(idx) = [];
    X(idx,:) = [];
    
    [PMF_fit(subject,:),~,logit_stats{subject}] = glmfit(X,Y,'binomial','link','logit');
    
end


PMF_fit_All = nanmean(PMF_fit,1);

bar(PMF_fit_All)
%%
close all

clear all

options = generate_perc_dyn_options();
load(options.paths.behaviouralData);



for subject = 1:length(B)
    
    clear  Rightt  Leftt LLeftt LRightt laggedRegressor
    
    
    
    
    
    Rightt = B{subject}.fb == 1;
    
    
    
    Leftt = B{subject}.fb== 0;
    Leftt = Leftt .* -1;
    
    
    RightLeft = Rightt + Leftt;
    
    if subject == 26 || subject == 28
        laggedRegressor  = zeros(450,5);
    else
        laggedRegressor  = zeros(600,5);
        
    end
    for lag = 1:5
        
        laggedRegressor(:,lag) =  [zeros(lag,1); RightLeft(1:end-lag)];
        
    end
    
    
    relativeCoherence = B{subject}.rel;
    
    meanRelativeCoherence = mean(unique( B{subject}.rel));
    relativeCoherence = relativeCoherence-meanRelativeCoherence;
    
    interactionRelativeAbsolute = relativeCoherence .* B{subject}.ab;
    
    Y = B{subject}.saccdir;
    % X = [Rightt, Leftt,  B{subject}.rel B{subject}.ab];
    X = [laggedRegressor,  relativeCoherence interactionRelativeAbsolute B{subject}.ab];
    % X = [Leftt,  B{subject}.rel B{subject}.ab];
    %X = [B{subject}.rel B{subject}.ab];
    
    idx = isnan(Y);
    
    Y(idx) = [];
    X(idx,:) = [];
    
    [PMF_fit(subject,:),~,logit_stats{subject}] = glmfit(X,Y,'binomial','link','logit');
    
    tstatSubject(subject,:) = logit_stats{subject}.t;
end


PMF_fit_All = nanmean(PMF_fit,1);
figure
subplot(2,1,1)
bar(PMF_fit_All)
ylabel('betas')
set(gca,'xticklabel',{'constant','t-1','t-2','t-3','t-4','t-5','rel coh sign','interact','abs coh'})
tidyfig;

for reg = 1:size(PMF_fit,2)
    
    [~,P(reg),~,stats] = ttest(PMF_fit(:,reg));
    
    tstatReg(reg) = stats.tstat;
    
end

subplot(2,1,2)
bar(tstatReg)
ylabel('tstats')
set(gca,'xticklabel',{'constant','t-1','t-2','t-3','t-4','t-5','rel coh sign','interact','abs coh'})
tidyfig;

%% close all

clear all

options = generate_perc_dyn_options();

for subject = 1 : options.raw.numSubjects
    
    
    %
    
    
    details = generate_perdyn_subject_details(subject,options);
    
    fb = [];
    prior = [];
    saccdir = [];
    rel = [];
    ab = [];
    
    
    
    for run = 1:details.raw.subject.runs
        
        
        
        data = load(details.raw.channel.datafile(run).run);
        
        
        fb = [fb; data.reg_fb];
        prior = [prior; data.reg_pri];
        saccdir = [saccdir; data.reg_saccdir];
        rel = [rel; data.reg_rel-0.5];
        ab = [ab; data.reg_ab];
        
        
        
        
        
        
    end
    
    interaction = rel .* ab;
    
    Y = saccdir;
    Y(Y == 1) = 0;
    Y(Y==2) = 1;
    % X = [Rightt, Leftt,  B{subject}.rel B{subject}.ab];
    X = [(prior-0.5) rel ab interaction];
    % X = [Leftt,  B{subject}.rel B{subject}.ab];
    %X = [B{subject}.rel B{subject}.ab];
    
    idx = isnan(Y);
    
    Y(idx) = [];
    X(idx,:) = [];
    
    [PMF_fit(subject,:),~,logit_stats{subject}] = glmfit(X,Y,'binomial','link','logit');
    
    tstatSubject(subject,:) = logit_stats{subject}.t;
    
    
    
    
end



PMF_fit_All = nanmean(PMF_fit,1);
figure
subplot(2,1,1)
bar(PMF_fit_All)
ylabel('betas')
set(gca,'xticklabel',{'constant','prior','rel','ab','interact'})
tidyfig;

for reg = 1:size(PMF_fit,2)
    
    [~,P(reg),~,stats] = ttest(PMF_fit(:,reg));
    
    tstatReg(reg) = stats.tstat;
    
end

subplot(2,1,2)
bar(tstatReg)
ylabel('tstats')
set(gca,'xticklabel',{'constant','prior','rel','ab','interact'});
tidyfig;

%%


clear all

options = generate_perc_dyn_options();

for subject = 1 : options.raw.numSubjects
    
    
    %
    
    
    details = generate_perdyn_subject_details(subject,options);
    
    fb = [];
    prior = [];
    saccdir = [];
    rel = [];
    ab = [];
    
    
    
    for run = 1:details.raw.subject.runs
        
        
        
        data = load(details.raw.channel.datafile(run).run);
        
        
        fb = [fb; data.reg_fb];
        prior = [prior; data.reg_pri];
        saccdir = [saccdir; data.reg_saccdir];
        rel = [rel; data.reg_rel-0.5];
        ab = [ab; data.reg_ab];
        
        
        
        
        
        
        
        
    end
    
    
    
    
    RighttFB = fb == 1;
    LefttFB = fb == 0;
    LefttFB = LefttFB .* -1;
    
    
    
    RighttRP = saccdir == 2;
    LefttRP = saccdir == 1;
    LefttRP = LefttRP .* -1;
    
    
    
    
    
    RightLeftFB = RighttFB + LefttFB;
    RightLeftRP = RighttRP + LefttRP;
    
    if subject == 12 || subject == 23 || subject == 24 || subject == 25
        laggedRegressorFB  = zeros(450,3);
        laggedRegressorRP  = zeros(450,3);
        interactionFBRP = zeros(450,3);
    else
        laggedRegressorFB  = zeros(600,3);
        laggedRegressorRP  = zeros(600,3);
        interactionFBRP = zeros(600,3);
    end
    for lag = 1:3
        
        laggedRegressorFB(:,lag) =  [zeros(lag,1); RightLeftFB(1:end-lag)];
        laggedRegressorRP(:,lag) =  [zeros(lag,1); RightLeftRP(1:end-lag)];
        interactionFBRP(:,lag)   =  laggedRegressorFB(:,lag) .* laggedRegressorRP(:,lag);
        
    end
    
    
    
    
    interaction = rel .* ab;
    
    
    Y = saccdir;
    Y(Y == 1) = 0;
    Y(Y==2) = 1;
    % X = [Rightt, Leftt,  B{subject}.rel B{subject}.ab];
    X_RP = [laggedRegressorRP rel ab interaction];
    X_FB = [laggedRegressorFB rel ab interaction];
    X_RPFB = [laggedRegressorFB laggedRegressorRP interactionFBRP rel ab interaction];
    % X = [Leftt,  B{subject}.rel B{subject}.ab];
    %X = [B{subject}.rel B{subject}.ab];
    
    idx = isnan(Y);
    
    Y(idx) = [];
    X_RP(idx,:) = [];
    X_FB(idx,:) = [];
    X_RPFB(idx,:) = [];
    
    
    [PMF_fitFB(subject,:),~,logit_stats{subject}] = glmfit(X_FB,Y,'binomial','link','logit');
    [PMF_fitRP(subject,:),~,logit_stats{subject}] = glmfit(X_RP,Y,'binomial','link','logit');
    [PMF_fitRPFB(subject,:),~,logit_stats{subject}] = glmfit(X_RPFB,Y,'binomial','link','logit');
    
    
    
    
    
    
end


% plot RP
PMF_fit_All = nanmean(PMF_fitRP,1);
figure
subplot(2,1,1)
title('Response')
bar(PMF_fit_All)
ylabel('betas')
set(gca,'xticklabel',{'constant','t-1','t-2','t-3','rel','ab','interact'})
tidyfig;

for reg = 1:size(PMF_fitRP,2)
    
    [~,P(reg),~,stats] = ttest(PMF_fitRP(:,reg));
    
    tstatReg(reg) = stats.tstat;
    
end

subplot(2,1,2)
bar(tstatReg)
ylabel('tstats')
set(gca,'xticklabel',{'constant','t-1','t-2','t-3','rel','ab','interact'});
tidyfig;


% plot FB
PMF_fit_All = nanmean(PMF_fitFB,1);
figure
subplot(2,1,1)
title('feedback')
bar(PMF_fit_All)
ylabel('betas')
set(gca,'xticklabel',{'constant','t-1','t-2','t-3','rel','ab','interact'})
tidyfig;
tstatReg = [];

for reg = 1:size(PMF_fitFB,2)
    
    [~,P(reg),~,stats] = ttest(PMF_fitFB(:,reg));
    
    tstatReg(reg) = stats.tstat;
    
end

subplot(2,1,2)
bar(tstatReg)
ylabel('tstats')
set(gca,'xticklabel',{'constant','t-1','t-2','t-3','rel','ab','interact'});
tidyfig;



% plot FB
PMF_fit_All = nanmean(PMF_fitRPFB,1);
figure
subplot(2,1,1)
title('feedback + Response')
bar(PMF_fit_All(1:end-1))
ylabel('betas')
set(gca,'xticklabel',{'constant','FBt-1','FBt-2','FBt-3','RPt-1','RPt-2','RPt-3','It-1','It-2','It-3','rel','ab','interact'})
tidyfig;
tstatReg = [];

for reg = 1:size(PMF_fitRPFB,2)
    
    [~,P(reg),~,stats] = ttest(PMF_fitRPFB(:,reg));
    
    tstatReg(reg) = stats.tstat;
    
end

subplot(2,1,2)
bar(tstatReg(1:end-1))
ylabel('tstats')
set(gca,'xticklabel',{'constant','FBt-1','FBt-2','FBt-3','RPt-1','RPt-2','RPt-3','It-1','It-2','It-3','rel','ab'});
tidyfig;
%% relative coherence x prior interaction



clear all

options = generate_perc_dyn_options();

for subject = 1 : options.raw.numSubjects
    
    
    %
    
    
    details = generate_perdyn_subject_details(subject,options);
    
    fb = [];
    prior = [];
    saccdir = [];
    rel = [];
    ab = [];
    
    
    
    for run = 1:details.raw.subject.runs
        
        
        
        data = load(details.raw.channel.datafile(run).run);
        
        
        fb = [fb; data.reg_fb];
        prior = [prior; data.reg_pri-0.5];
        saccdir = [saccdir; data.reg_saccdir];
        rel = [rel; data.reg_rel-0.5];
        ab = [ab; data.reg_ab];
        
        
        
        
        
        
        
        
    end
    
    
    
    
    
    interaction = rel .* ab;
    
    
    if subject == 12 || subject == 23 || subject == 24 || subject == 25
        
        prior_rel_reg = zeros(450,1);
    else
        prior_rel_reg  = zeros(600,1);
        
    end
    
    prior_rel_reg(prior > 0.5 & rel > 0) = 1;
    prior_rel_reg(prior < 0.5 & rel < 0) = -1;
    
    Y = saccdir;
    Y(Y == 1) = 0;
    Y(Y==2) = 1;
    % X = [Rightt, Leftt,  B{subject}.rel B{subject}.ab];
    X = [ prior prior_rel_reg rel ab interaction];
 
    % X = [Leftt,  B{subject}.rel B{subject}.ab];
    %X = [B{subject}.rel B{subject}.ab];
    
    idx = isnan(Y);
    
    Y(idx) = [];
    X(idx,:) = [];

    
    
    [PMF_fit(subject,:),~,logit_stats{subject}] = glmfit(X,Y,'binomial','link','logit');
    
    
    
    
end

% plot 
PMF_fit_All = nanmean(PMF_fit,1);
figure
subplot(2,1,1)
title('prior rel interaction')
bar(PMF_fit_All)
ylabel('betas')
set(gca,'xticklabel',{'constant','prior','prior rel interact','rel','ab','interact'})
tidyfig;
tstatReg = [];

for reg = 1:size(PMF_fit,2)
    
    [~,P(reg),~,stats] = ttest(PMF_fit(:,reg));
    
    tstatReg(reg) = stats.tstat;
    
end

subplot(2,1,2)
bar(tstatReg)
ylabel('tstats')
set(gca,'xticklabel',{'constant','prior','prior rel interact','rel','ab','interact'});
tidyfig;







