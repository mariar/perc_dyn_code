clear all

glmFlag = 'SignedPriorIntWithAllEvidenceRegressors';

options = generate_perc_dyn_options();

for subject = 1: options.badTrialsRemoved.numSubjects
    disp(subject)
    details = generate_perdyn_subject_details(subject,options);
    
    Y_saccade = [];
    for runid = 1:details.badTrialsRemoved.subject.runs
        
        dataLoad =  load(details.badTrialsRemoved.channel.datafile(runid).run);
        
        data{runid} = dataLoad.data;
        
        saccadeResponse = data{runid}.reg_saccdir;
        
        saccadeResponse(saccadeResponse == 1) = 0;
        saccadeResponse(saccadeResponse == 2) = 1;
        
        Y_saccade = [Y_saccade; saccadeResponse];
        
    end
    
    cfg = perc_dyn_create_design_matrix(data,options,glmFlag,0,details,[]);
    
    
    [PMF_fit(subject,:),~,logit_stats{subject}] = glmfit(cfg.design,Y_saccade,'binomial','link','logit');
    
end

for reg = 1:size(PMF_fit,2)
    
    [~,P(reg),~,stats] = ttest(PMF_fit(:,reg));
    
    tstatReg(reg) = stats.tstat;
  
end





bar(tstatReg)
ylabel('tstats')
set(gca,'xticklabel',cfg.label);
tidyfig; 
