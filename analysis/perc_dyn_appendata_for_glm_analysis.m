function timeLockedAppendedData = perc_dyn_appendata_for_glm_analysis(timeLock)

        cfg = [];
        cfg.keepsampleinfo= 'no';
        appendedData = ft_appenddata(cfg, timeLock{:});
        appendedData.dimord = 'rpt_chan_time';

        timeLockedAppendedData = perc_dyn_timelock_analysis(appendedData);

end 