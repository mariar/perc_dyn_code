function data = perc_dyn_zscore(data) 

for label = 1:11 
    
    trialCombined = []; 
    
    for trial = 1:length(data.trial)
        
        trialCombined = [trialCombined; data.trial{trial}(label,:)]; 
        
    end 

    meanZ = mean(trialCombined(:)); 
    stdZ = std(trialCombined(:)); 
    
    trialNew = (trialCombined - meanZ)./ stdZ; 
    
    
    for trial = 1:length(data.trial)
        
        if label == 11 
            
        trialCell{trial}(label,:) = data.trial{trial}(label,:);   
        else 
        
        trialCell{trial}(label,:) = trialNew(trial,:); 
        
        end 
        
        
    end 
    
end


data.trial = trialCell; 



end 