% how to remove bad trials and outliers from the data

% you'll have to adapt this to run in every subject

%% load one data file (which is one run)
clear all 
close all 

options = generate_perc_dyn_options();
for subject = 1% options.numSubjects

% THIS IS THE DIRECTORY ON *MY* COMPUTER, REPLACE IT WITH YOURS
% rootDir = '/Users/marshall/data/tmp/4Maria';
% 
% % specify one subject and run (you can adapt this to make a loop over all subjects and runs)% subid = 533;

details = generate_perdyn_subject_details(subject,options);

% determine names of files and directories
% subDir = fullfile(options.paths.PreprocessedDataPath,options.subjectFolderList{1});
% 
% fileName = [options.subjectFolderList{1} '_run' num2str(runid) '_ftData.mat'];
% 
% assert(exist(fullfile(subDir,fileName)) == 2, 'the file you want doesn''t exist. Did you change the directory name to match where the data is on *your* computer???');
for run = 1:details.subject.runs 
% load the run. This loads a fieldtrip strufcture 'ftData' and a bunch of regressors 'reg_xxx'

load(details.raw.datafile(run).run);

%% do some outlier / bad trial detection

% initialise
badTrials           = [];

% look at reg_rt (reaction times)
% if rt is nan, the subject didn't make an eye movement. We don't want those trials
badTrials.rtIsNan   = isnan(reg_rt);

% also if the rt is faster than some cutoff we can discard as it is too fast
rtCutoff            = 50;
badTrials.rtTooFast = reg_rt < rtCutoff;

% and if the saccade direction is nan, also subj made no eye movement
badTrials.saccIsNaN = isnan(reg_saccdir);

% finally, if there is no MEG data (e.g., because MaxFilter removed it) we want to mark that trial as bad
% this code might be a bit tricky to understand. If you're curious, look up 'anonymous functions'.
badTrials.noMegData = cellfun(@(x) any(any(isnan(x))), ftData.trial)';

% maybe there are other reasons you want to discard a trial
% if there are, you can add them here

%% once we have all the possible reasons for discarding a trial, 
% we want one logical vector that says 'is there ANYTHING bad about this trial?'

badTrials.anyReason = any([badTrials.rtIsNan badTrials.rtTooFast badTrials.saccIsNaN badTrials.noMegData],2);

%% now we have that we can remove those bad trials from the data structure and regressors

% indices of the GOOD trials that we want to keep
goodTrials  = find(~badTrials.anyReason);

% select the good trials only from the data structure
% note: I'm over-writing the old structure to save memory, you might want
% to create a new structure. That's up to you
cfg         = [];
cfg.trials  = goodTrials;
ftData      = ft_selectdata(cfg,ftData);

% then keep only the goodtrials in the regressors too
reg_ab      = reg_ab(goodTrials);
reg_rt      = reg_rt(goodTrials);
reg_saccdir = reg_saccdir(goodTrials);
% and so on... do this for all regressors you are interested in keeping

%% simple timelock analysis

cfg             = [];
cfg.keeptrials  = 'yes';
timeLock        = ft_timelockanalysis(cfg,ftData);

% cfg             = [];
% % plot one parietal channel
% cfg.channel     = {'pIPS_L'};
% figure('position',[0 0 1000 300])
% ft_singleplotER(cfg,timeLock);

%% simple GLM 

cfg = [];
cfg.design(:,2) = zscore(reg_saccdir);
cfg.design(:,1) = 1;
cfg.label       = {'constant','saccade_dir'};
cfg.parameter   = 'trial';
glm             = fast_glm_wrapper(cfg, timeLock);

cfg             = [];
cfg.channel     = {'pIPS_L'};
cfg.parameter   = 'saccade_dir_t';
figure('position',[0 300 1000 300])
ft_singleplotER(cfg,glm)

% you can see that the early peaks in the timelock analysis (the evoked
% response to the dots coming on) don't predict the saccade direction, but
% the later activity does around 3.5s (which is about the time the subj
% makes a saccade!) 
end
end