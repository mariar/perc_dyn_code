% look at rt distribution 
clear all;
close all 
options = generate_perc_dyn_options();

load(options.paths.behaviouralData);
B([3 7 25 27])=[];
rts = [];
for s =  1:length(B)
    
    rts = [rts; B{s}.rt(~isnan(B{s}.rt))];
end 

histogram(rts)
xlabel('rt ms?')
title('rt distribution across all subjects')
tidyfig;