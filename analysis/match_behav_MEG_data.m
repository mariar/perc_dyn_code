function [subjectList, runList] =  match_behav_MEG_data(subjectList,runList,B,options,runid,data)




idxRun = (runid-1) * 150 +1; 


behaviourRunRts = B{options.BehaviourMatch(subject)}.rt(idxRun: idxRun + 149); 
MegRunRts = data.reg_rt; 

% make NaN to 0 to compare both 

behaviourRunRts(isnan(behaviourRunRts)) = 0; 
MegRunRts(MegRunRts<50) = 0; 
MegRunRts(isnan(MegRunRts)) = 0; 



 if  ~all(behaviourRunRts==MegRunRts)
     subjectList = [subjectList,subject]; 
     runList = [runList, runid]; 
     idx = behaviourRunRts~=MegRunRts;
     sum(idx)

 end
 


end 