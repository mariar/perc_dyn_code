%% ERP ANALYSIS


clear all
close all
options = generate_perc_dyn_options();
glmFlag = 'correctIncorrectTR';
for subject = 1: options.badTrialsRemoved.numSubjects
    disp(subject)
    details = generate_perdyn_subject_details(subject,options);
    
    for runid = 1:details.badTrialsRemoved.subject.runs
        
        clear data
        load(details.badTrialsRemoved.channel.datafile(runid).run);
        
        
        
        timeLock = perc_dyn_timelock_analysis(data.ftData);
        
        channels = {'MEG1822MEG1823';'MEG1843MEG1842'};
        timeLockLeft = perc_dyn_timelock_analysis(data.ftData, channels);
        
        channels =     'MEG2212MEG2213'; 'MEG2312MEG2313';
        timeLockRight = perc_dyn_timelock_analysis(data.ftData, channels);
        
        channels = {'MEG0733MEG0732';'MEG0743MEG0742';'MEG1832MEG1833';    'MEG2013MEG2012'; ...
            'MEG2023MEG2022'; 'MEG2242MEG2243'};
        timeLockCentre = perc_dyn_timelock_analysis(data.ftData, channels);
        
        channels = {'MEG0733MEG0732'; 'MEG0743MEG0742'; 'MEG1822MEG1823';'MEG1832MEG1833';...
            'MEG1843MEG1842'; 'MEG2013MEG2012'; 'MEG2023MEG2022';'MEG2212MEG2213';...
            'MEG2242MEG2243'; 'MEG2312MEG2313'};
        timeLockAll = perc_dyn_timelock_analysis(data.ftData, channels);
        
        design = perc_dyn_create_design_matrix(data,options,glmFlag);
        
        
      
        %         cfg = [];
        %         cfg.trials = design.design(:,1);
        %         ERP1{runid} = ft_timelockanalysis(cfg, timeLock);
        %         ERP2{runid} = ft_timelockanalysis(cfg, timeLock);
        %         ERP3{runid} = ft_timelockanalysis(cfg, timeLock);
        %         ERP1{runid} = ft_timelockanalysis(cfg, timeLock);
        
        %         cfg = [];
        %         cfg.trials = design.design(:,1);
        %         ERPLeft{runid} = ft_timelockanalysis(cfg, timeLockLeft);
        %
        %         cfg = [];
        %         cfg.trials = design.design(:,1);
        %         ERPRight{runid} = ft_timelockanalysis(cfg, timeLockRight);
        %
        %         cfg = [];
        %         cfg.trials = design.design(:,1);
        %         ERPCentre{runid} = ft_timelockanalysis(cfg, timeLockCentre);
       
        cfg = [];
        cfg.trials = logical(design.design(:,1));
        ERPAll1{runid} = ft_timelockanalysis(cfg, timeLockAll);
        
        cfg = [];
        cfg.trials = logical(design.design(:,2));
        ERPAll2{runid} = ft_timelockanalysis(cfg, timeLockAll);
        
        cfg = [];
        cfg.trials = logical(design.design(:,3));
        ERPAll3{runid} = ft_timelockanalysis(cfg, timeLockAll);
        
        cfg = [];
        cfg.trials = logical(design.design(:,4));
        ERPAll4{runid} = ft_timelockanalysis(cfg, timeLockAll);
        %         cfg             = [];
        %         cfg.channel     = {'pIPS_L'};
        %         cfg.parameter   = 'saccade_dir_t';
        %         figure('position',[0 300 1000 300])
        %         ft_singleplotER(cfg,glm)
        
        
        
        
    end
    
%     [timelockAverage(subject)] = perc_dyn_timelock_grand_average(glm,options,'within',glmFlag);
%     [timelockAverageLeft(subject)] = perc_dyn_timelock_grand_average(glmLeft,options,'within',glmFlag);
%     [timelockAverageRight(subject)] = perc_dyn_timelock_grand_average(glmRight,options,'within',glmFlag);
%     [timelockAverageCentre(subject)] = perc_dyn_timelock_grand_average(glmCentre,options,'within',glmFlag);


    [timelockAverageAll1{subject}] = perc_dyn_timelock_grand_average(ERPAll1,options,'within',glmFlag,1);
    [timelockAverageAll2{subject}] = perc_dyn_timelock_grand_average(ERPAll2,options,'within',glmFlag,1);
    [timelockAverageAll3{subject}] = perc_dyn_timelock_grand_average(ERPAll3,options,'within',glmFlag,1);
    [timelockAverageAll4{subject}] = perc_dyn_timelock_grand_average(ERPAll4,options,'within',glmFlag,1);
end

% glmResults.Original = perc_dyn_timelock_grand_average(timelockAverage,options,'across',glmFlag);
% glmResults.Left = perc_dyn_timelock_grand_average(timelockAverageLeft,options,'across',glmFlag);
% glmResults.Right = perc_dyn_timelock_grand_average(timelockAverageRight,options,'across',glmFlag);
% glmResults.Centre = perc_dyn_timelock_grand_average(timelockAverageCentre,options,'across',glmFlag);
ERPResults.All1 = perc_dyn_timelock_grand_average(timelockAverageAll1,options,'across',glmFlag,1);
ERPResults.All2 = perc_dyn_timelock_grand_average(timelockAverageAll2,options,'across',glmFlag,1);
ERPResults.All3 = perc_dyn_timelock_grand_average(timelockAverageAll3,options,'across',glmFlag,1);
ERPResults.All4 = perc_dyn_timelock_grand_average(timelockAverageAll4,options,'across',glmFlag,1);

save(options.ERPAnalysis.correctIncorrectTR.data,'ERPResults');
%%
figure 
hold on  
plot(ERPResults.All1.time, ERPResults.All1.avg); 
plot(ERPResults.All1.time, ERPResults.All2.avg); 
plot(ERPResults.All1.time, ERPResults.All3.avg); 
plot(ERPResults.All1.time, ERPResults.All4.avg); 
plot(ones(6,1)*4.5, [6:0.5:8.5],'k--','LineWidth',2)
xlim([0 6])
hold off 



