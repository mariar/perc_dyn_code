 
clear all;
options = generate_perc_dyn_options();
% space to search for :
% run length, apparent number of dots, softmax inverse temp
load(options.paths.behaviouralData);

B([8 25 27])=[]; %subjects for whom no frequency tag data available
h=25; k=10:5:100; T=0.1:0.01:4; 
[hh,kk,TT]=ndgrid(h,k,T);

for b=1:length(B)
%disp(int2str(b));
 for hix=1:length(h) 
%     disp(int2str(hix)); 
     for kix=1:length(k) 
%        disp(int2str(kix));         
        for tix=1:length(T) 
%              disp(int2str(tix)); 
             [LL(hix,kix,tix)]=learningModel2(B{b},k(kix),h(hix),T(tix));
            
         end 
     end 
 end
 [~,ix]=max(LL(:));
 [~,E(b),dst(b)]=learningModel2(B{b},kk(ix),hh(ix),TT(ix),1);
 keyboard;
 params(b,:)=[kk(ix),hh(ix),TT(ix)];
  Enew(b).dkl= nansum(dst(b).wtPos.*(log((dst(b).wtPos)./(dst(b).prior))),2); % not sure what is happening here.... something about different between within trial posterior and normal prior? 
 % how much distribution has changed KL-divergence - how much info you gain
 % - measure surprise/conflict between dots and prior - comparing how whole
 % distribuiton moves 
end 