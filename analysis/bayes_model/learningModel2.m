%load(b);
function [modelLL, bestGuess, fullDist] = learningModel2(b,k,h,T,makePlot)

%%

disp(nargin);
if nargin < 5
    makePlot = 0;
end


H = 1/h;

b.dots_moving_right = b.ab .* b.rel ;
b.dots_moving_left  = b.ab .* (1-b.rel) ;

% nTrials = max(b.bl).*max(b.tr);
nTrials = length(b.ab);
pSp     = [0:0.01:1]; % hypothesis space/ candidate values of pRight

fb      = pSp./sum(pSp); % lh function based on a single 'ans=right' feedback event
leak    = ones(size(pSp)); leak = leak./sum(leak); leak = leak.*H; 

% set up outputs
lh          = ones(nTrials, length(pSp)); lh        = lh./sum(lh(:));
prior       = ones(nTrials, length(pSp)); prior     = prior./sum(prior(:));
posterior   = ones(nTrials, length(pSp)); posterior = posterior./sum(posterior(:));
withinTrial_posterior = posterior;

for t = 1:nTrials
    % uniform prior at start of each block
    if mod(t, 150) == 1
        prior(t,:) = ones(size(pSp)) ./ length(pSp);
    end
    
    % likelihood function from number of dots going L and R - beta binomial
    % beta binomial model: lh(q) = beta(k+1, (n-k)+1); n = num observations
    lh(t,:) = betapdf(pSp, k*(b.dots_moving_right(t))+1, k*(b.dots_moving_left(t))+1); 
    lh(t,:) = lh(t,:)./sum(lh(t,:)); % area under curve = 1
    % update my belief within this trial --> response
    withinTrial_posterior(t,:) = prior(t,:).*lh(t,:); %Bayes theorem % how surprised participants are - how much they should change their mind
    withinTrial_posterior(t,:) = withinTrial_posterior(t,:)./sum(withinTrial_posterior(t,:)); % area under curve = 1
    
    % update my belief based on veridical feedback
    if b.fb(t) == 1 % veridical fb is right
        posterior(t,:) = prior(t,:).*fb; 
        posterior(t,:) = posterior(t,:)./sum(posterior(t,:));
    elseif b.fb(t) == 0 % veridical fb is left
        posterior(t,:) = prior(t,:).*fliplr(fb); 
        posterior(t,:) = posterior(t,:)./sum(posterior(t,:));
    end
    
    % update prior for next time
    prior(t+1,:) = (posterior(t,:)).*(1-H) +leak; %???????? probability + leak is scaled by H so posterior has to be scaled by 1-H
    prior(t+1,:) = prior(t+1,:)./sum(prior(t+1,:));
end

% don't need the prior from AFTER the last trial, so chop it off
prior = prior(1:end-1,:);

%% work out how much the dots conflicted with or supported the prior on each trial

% within trial posterior does not influence the model at all? 
%r = sum(withinTrial_posterior(:,find(pSp>0.5)),2)>0.5;
r   = sum(withinTrial_posterior(:,find(pSp>0.5)),2);
pR  = exp(r/T)./(exp(r/T)+exp((1-r)/T)); % softmax

r_prior     = sum(prior(:,find(pSp>0.5)),2); r_prior=r_prior; % %belief for right - from prior
pR_prior    = exp(r_prior/T)./(exp(r_prior/T)+exp((1-r_prior)/T)); % softmax

% confidence weighted belief update in favour of 'right' - positive if p(respond right | within trial posterior) > p(respond right | prior)
delta_pR    = NaN(size(pR));
delta_pR    = pR-pR_prior;
% this reflects relative coherence
if makePlot == 1
    figure
    plot(b.rel, delta_pR, 'o'); 
    xlabel('relative coherence'); 
    ylabel('delta_pR - confidence weighted update');
end
% shift in peak of belief distribution from prior to w/t posterior - positive if the peak moves towards 'definitely right', negative if the peak moves towards 'definitely left'
[~,ix]      = max(prior');
MLE_prior   = pSp(ix);
[~,ix]      = max(withinTrial_posterior');
MLE_withinTrial_posterior=pSp(ix);
delta_MLE   = MLE_withinTrial_posterior-MLE_prior;

% these are quite correlated
if makePlot == 1
    figure
    plot(delta_MLE, delta_pR,'o')
    xlabel('delta MLE')
    ylabel('delta pR')
end
%% set up design matrix for GLM of frequency tagging data
% use MLE - not sure if this is better, but it is =slightly more defensible
% as we can't actually fit softmax on the prior, so we don't know what the
% appropriate value of T would be for fitting behaviour on prior alone

ix_R    = find(MLE_prior>0.5); % trials with rightward prior
ix_L    = find(MLE_prior<=0.5);
dm      = [ MLE_prior(ix_R) delta_MLE(ix_R)             1-MLE_prior(ix_L)   -1*delta_MLE(ix_L)];

% %%%%%% TOM ATTEMPTING TO INFER WHAT JILL INTENDED
dm          = repmat(0,length(b.ab),4);
dm(ix_R, 1) = zscore(MLE_prior(ix_R));
dm(ix_R, 2) = zscore(delta_MLE(ix_R));
dm(ix_L, 3) = zscore(1-MLE_prior(ix_L));
dm(ix_L, 4) = zscore(-1*delta_MLE(ix_L));

if makePlot == 1
    figure;imagesc(dm);
    title('design matrix')
end

% if we want to get the log likelihood we need to compare the model output
% with the actual behaviour. For that we need the saccade directions.
% if we just want to *evaluate* the model, not to fit it, we don't need this

if isfield(b,'saccdir') % have behaviour, return likelihood
    
    ix  = find(isfinite(b.saccdir));
    % work out overall model log likelihood as
    % sum(log(p(human response given softmax))) over all trials
    modelLL = nansum(log((b.saccdir(ix).*pR(ix)) + (~b.saccdir(ix).*(1-pR(ix)))));
    
else % don't have behaviour, just evaluate model
    
    modelLL = NaN;
    
end
% disp('stop here');


% organise the rest out of the output

% 'bestGuess' is already collapsed into the 'best guess' for every parameter
bestGuess.wtPos    = withinTrial_posterior * pSp';
bestGuess.fbPos    = posterior * pSp';
bestGuess.prior    = prior * pSp';
bestGuess.lh       = lh * pSp';
bestGuess.dm       = dm;
bestGuess.MLE_prior = MLE_prior';
bestGuess.MLE_withinTrial_posterior = MLE_withinTrial_posterior';

% 'fullDist' is the full distributions for everything
fullDist.wtPos    = withinTrial_posterior;
fullDist.fbPos    = posterior;
fullDist.prior    = prior;
fullDist.lh       = lh;

end

%% we do all this stuff in a different script now

% I *think* this is right - the intential was to make a desogn matrix as
% follows, where columns 1 and 2 are used for trials wher prior favours R,
% and columns 3 and 4 are used on trials where prior favours L
%    [ p(R|prior)       p(R|posterior)-p(R|prior)   p(L|prior)          p(L|posterior)-p(L|prior) ]
% dmNorm  = normalise(dm); %subtract mean fromm each EV and set stdev of each EV to 1)

% b_RH    = glmfit(dmNorm, freqtagdata_RH, 'normal');
% b_LH    = glmfit(dmNorm, freqtagdata_LH, 'normal');
%
% % plot effects for contralateral prior commbined across R_IPS and L_IPS ROIs.
% figure
% plot(b_RH(3) + b_LH(1))
% hold on
% plot(b_RH(4) + b_LH(2))

% if we want to get the log likelihood we need to compare the model output
% with the actual behaviour. For that we need the saccade directions.
% if we just want to *evaluate* the model, not to fit it, we don't need this
