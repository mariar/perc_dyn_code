clear all 
close all 


options = generate_perc_dyn_options();

details = generate_perdyn_subject_details(	1,options);

diffAll = [];

for subject = 1 : options.raw.numSubjects
    
    difftSubject{subject} = [];
    
for run = 1:details.raw.subject.runs 
   
    data = load(details.raw.events(run).run);
allEvents_spm = data.allEvents_spm; 

val = {allEvents_spm.value}; 

Cellindx = cellfun(@(x) (ismember(4,x)), val, 'UniformOutput',false);

indxVal = find(cell2mat(Cellindx)==1);

indxVal4 = indxVal(1:2:end); 


Cellindx2 = cellfun(@(x) (ismember([8],x)), val, 'UniformOutput',false);
indxVal = find(cell2mat(Cellindx2)==1);

indxfb1 = indxVal(1:2:end); 


Cellindx3 = cellfun(@(x) (ismember([9],x)), val, 'UniformOutput',false);
indxVal = find(cell2mat(Cellindx3)==1);
indxfb2 = indxVal(1:2:end); 

indxfb = sort([indxfb1, indxfb2]); 

time = [allEvents_spm.time];

time4 = time(indxVal4); 
timefb = time(indxfb); 

difft = timefb - time4; 

difftSubject{subject} = [difftSubject{subject} difft]; 
    
end 

diffAll = [diffAll difftSubject{subject}]; 
end 

% histogram across subjects 
figure 
 histogram(diffAll)
 tidyfig; 
 xlabel('seconds?'); 
 title('trigger distance trial start fb')
 
 
