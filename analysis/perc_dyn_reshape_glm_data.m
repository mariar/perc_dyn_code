function [data] = perc_dyn_reshape_glm_data(glmData, reglabel, numSubjects)

data = zeros(numSubjects, length(glmData{1}.(reglabel)));

for subject = 1:numSubjects
    
  data(subject,:) =  glmData{subject}.(reglabel);  
    
    
    
end 

end 