function options = generate_perc_dyn_options()
% subject 158 had to be removed because behavioural data could not be
% matched with meg data to find out correct and incorrect trials 

addpath('/Users/maria/Documents/MATLAB/fieldtrip')
ft_defaults
 addpath(genpath('/Users/maria/Documents/MATLAB/cbrewer'));

options.paths.VolumePath = '/Volumes/perc_dyn_MEG';
options.paths.PreprocessedDataPath = fullfile(options.paths.VolumePath,'preprocessedData');
options.paths.ChannelDataPath = fullfile(options.paths.VolumePath,'channelData');
options.paths.badTrialsRemovedPath = fullfile(options.paths.VolumePath,'badTrialsRemoved');
options.paths.behaviouralData = fullfile(options.paths.VolumePath,'behaviour','B.mat');
options.paths.analysisData = fullfile(options.paths.VolumePath,'analysis');

fileID = fopen(fullfile(options.paths.PreprocessedDataPath,'subjectFolderList.txt'),'r');
fileList = textscan(fileID,'%s');
options.raw.subjectFolderList = fileList{1};
options.raw.numSubjects = length(options.raw.subjectFolderList);


fileID = fopen(fullfile(options.paths.badTrialsRemovedPath,'subjectFolderList.txt'),'r');
fileList = textscan(fileID,'%s');
options.badTrialsRemoved.subjectFolderList = fileList{1};
options.badTrialsRemoved.numSubjects = length(options.badTrialsRemoved.subjectFolderList);



options.rtCutoff = 50;

options.regressors(1).regressorLabel    = 'absoluted_coherence';
options.regressors(1).regressorName     = 'reg_ab';
options.regressors(2).regressorLabel    = 'relative_coherence_signed';
options.regressors(2).regressorName     = 'reg_rel_signed';
options.regressors(3).regressorLabel    = 'relative_coherence_folded';
options.regressors(3).regressorName     = 'reg_relf';
options.regressors(4).regressorLabel    = 'saccade_direction';
options.regressors(4).regressorName     = 'reg_saccdir';
options.regressors(5).regressorLabel    = 'constant';
options.regressors(5).regressorName     = 'reg_const';
options.regressors(6).regressorLabel    = 'prior';
options.regressors(6).regressorName     = 'reg_pri';
options.regressors(7).regressorLabel    = 'prior_strength';
options.regressors(7).regressorName     = 'reg_pris';
options.regressors(8).regressorLabel    = 'left_correct_trials';
options.regressors(8).regressorName     = 'reg_left_corr';
options.regressors(9).regressorLabel    = 'right_correct_trials';
options.regressors(9).regressorName     = 'reg_right_corr';
options.regressors(10).regressorLabel   = 'right_wrong_trials';
options.regressors(10).regressorName    = 'reg_right_wrong';
options.regressors(11).regressorLabel   = 'left_wrong_trials';
options.regressors(11).regressorName    = 'reg_left_wrong';
options.regressors(12).regressorLabel   = 'difference_in_prior'; 
options.regressors(12).regressorName    = 'reg_surp';
options.regressors(13).regressorLabel   = 'within_trial_posterior'; 
options.regressors(13).regressorName    = 'reg_wtPos';
options.regressors(14).regressorLabel   = 'KL_divergence_prior'; 
options.regressors(14).regressorName    = 'reg_KL_divergence_prior'; 
options.regressors(15).regressorLabel   = 'feedback_signed'; 
options.regressors(15).regressorName    = 'reg_fb_signed'; 
options.regressors(16).regressorLabel   = 'interaction_rel_abs'; 
options.regressors(16).regressorName    = 'reg_interaction_rel_abs'; 
options.regressors(17).regressorLabel   = 'absoluted_scaled_prior';
options.regressors(17).regressorName    = 'reg_absoluted_scaled_prior';
options.regressors(18).regressorLabel   = 'interaction_absolute_relative_coherence_folded'; 
options.regressors(18).regressorName    = 'reg_interaction_reg_abs_folded';
options.regressors(19).regressorLabel   = 'signed_prior'; 
options.regressors(19).regressorName    = 'reg_signed_prior'; 
options.regressors(20).regressorLabel   = 'correct_incorrect_contrast';
options.regressors(20).regressorName    = 'reg_correct_incorrect_contrast';
options.regressors(21).regressorLabel   = 'reward_probability'; 
options.regressors(21).regressorName    = 'reg_reward_probability'; 
options.regressors(22).regressorLabel   = 'feedback_surprise';
options.regressors(22).regressorName    = 'reg_feedback_surprise';
options.regressors(23).regressorLabel   = 'response_surprise';
options.regressors(23).regressorName    = 'reg_response_surprise';
options.regressors(24).regressorLabel   = 'orthog_correct_incorr_rew_prob';
options.regressors(24).regressorName   = 'reg_orthog_correct_incorr_rew_prob';
options.regressors(25).regressorLabel   = 'lagged_feedback';
options.regressors(25).regressorName   = 'reg_lagged_feedback';
options.regressors(26).regressorLabel   = 'lagged_response'; 
options.regressors(26).regressorName    = 'reg_lagged_response';
options.regressors(27).regressorLabel = 'interaction_lagged_response_feedback';
options.regressors(27).regressorName = 'reg_lagged_response_x_fb';
options.regressors(28).regressorLabel = 'interaction_prior_relative_coherence'; 
options.regressors(28).regressorName = 'reg_prior_x_rel';
options.regressors(29).regressorLabel = 'interaction_prior_absolute_cohrence'; 
options.regressors(29).regressorName = 'reg_prior_x_ab';
options.regressors(30).regressorLabel = 'interaction_prior_x_ab_x_rel';
options.regressors(30).regressorName = 'reg_prior_x_abs_x_rel';
options.regressors(31).regressorLabel = 'prior_signed_for_left';
options.regressors(31).regressorName = 'reg_prior_signed_for_left';
options.regressors(32).regressorLabel = 'interaction_abXrel_signed_for_left';
options.regressors(32).regressorName = 'reg_interaction_abXrel_signed_for_left';
options.regressors(33).regressorLabel = 'abXrel_folded_orthog';
options.regressors(33).regressorName = 'reg_abXrelfolded_orthog';
options.regressors(34).regressorLabel = 'abXrel_lagged';
options.regressors(34).regressorName = 'reg_abXrel_lagged';
options.regressors(35).regressorLabel = 'rel_lagged';
options.regressors(35).regressorName = 'reg_rel_lagged';
options.regressors(36).regressorLabel = 'ab_lagged';
options.regressors(36).regressorName = 'reg_ab_lagged';



options.TrialGLM.first_glm.name = 'first_glm';
options.TrialGLM.first_glm.regressors = options.regressors(1:7);

options.TrialGLM.correctIncorrectTR.name = 'correctIncorrectTR';
options.TrialGLM.correctIncorrectTR.regressors = options.regressors(8:11);

options.TrialGLM.absoluted_prior.name = 'absoluted_prior'; 
options.TrialGLM.absoluted_prior.regressors = options.regressors([18, 17, 20]);

options.TrialGLM.surprise_fb.name = 'surprise_fb'; 
options.TrialGLM.surprise_fb.regressors = options.regressors([1, 3, 20, 18, 22]);

options.TrialGLM.surprise_saccade.name = 'surprise_saccade'; 
options.TrialGLM.surprise_saccade.regressors = options.regressors([1, 3, 20, 18, 23]);

options.TrialGLM.surprise_wtPst.name = 'surprise_wtPst'; 
options.TrialGLM.surprise_wtPst.regressors = options.regressors([ 4, 13, 15, 19]);

options.TrialGLM.surprise_wtPst_with_rel_ab_reg.name = 'surprise_wtPst_with_rel_ab_reg'; 
options.TrialGLM.surprise_wtPst_with_rel_ab_reg.regressors = options.regressors([ 4, 13, 15, 19, 16, 1, 2]);

options.TrialGLM.update_prior.name = 'update_prior'; 
options.TrialGLM.update_prior.regressors = options.regressors([ 1, 3,  18, 12, 20]);

options.TrialGLM.prediction_error.name = 'prediction_error'; 
options.TrialGLM.prediction_error.regressors = options.regressors([20, 21]);

options.TrialGLM.prediction_error_orthog.name = 'prediction_error_orthog'; 
options.TrialGLM.prediction_error_orthog.regressors = options.regressors([24, 21]);

options.TrialGLM.prediction_error_correctTrials.name = 'prediction_error_correctTrials'; 
options.TrialGLM.prediction_error_correctTrials.regressors = options.regressors([5, 21]);
options.TrialGLM.prediction_error_correctTrials.selectTypeFlag = 'correct';

options.TrialGLM.prediction_error_wrongTrials.name = 'prediction_error_wrongTrials'; 
options.TrialGLM.prediction_error_wrongTrials.regressors = options.regressors([5, 21]);
options.TrialGLM.prediction_error_wrongTrials.selectTypeFlag = 'wrong';

options.TrialGLM.priorInteraction_correctTrials.name = 'priorInteraction_correctTrials'; 
options.TrialGLM.priorInteraction_correctTrials.regressors = options.regressors([5, 31, 32]);
options.TrialGLM.priorInteraction_correctTrials.selectTypeFlag = 'correct';

options.TrialGLM.priorInteraction_wrongTrials.name = 'priorInteraction_wrongTrials'; 
options.TrialGLM.priorInteraction_wrongTrials.regressors = options.regressors([5, 31, 32]);
options.TrialGLM.priorInteraction_wrongTrials.selectTypeFlag = 'wrong';



options.TrialGLM.priorLeftResp_correctTrials.name = 'priorLeftResp_correctTrials'; 
options.TrialGLM.priorLeftResp_correctTrials.regressors = options.regressors([5, 31]);
options.TrialGLM.priorLeftResp_correctTrials.selectTypeFlag = 'correct';

options.TrialGLM.priorLeftResp_wrongTrials.name = 'priorLeftResp_wrongTrials'; 
options.TrialGLM.priorLeftResp_wrongTrials.regressors = options.regressors([5, 31]);
options.TrialGLM.priorLeftResp_wrongTrials.selectTypeFlag = 'wrong';


options.TrialGLM.abXrel_LeftResp_correctTrials.name = 'abXrel_LeftResp_correctTrials'; 
options.TrialGLM.abXrel_LeftResp_correctTrials.regressors = options.regressors([5, 32]);
options.TrialGLM.abXrel_LeftResp_correctTrials.selectTypeFlag = 'correct';

options.TrialGLM.abXrel_LeftResp_wrongTrials.name = 'abXrel_LeftResp_wrongTrials'; 
options.TrialGLM.abXrel_LeftResp_wrongTrials.regressors = options.regressors([5, 32]);
options.TrialGLM.abXrel_LeftResp_wrongTrials.selectTypeFlag = 'wrong';



options.TrialGLM.abXrel_allTrials.name = 'abXrel_allTrials'; 
options.TrialGLM.abXrel_allTrials.regressors = options.regressors([20, 32]);

options.TrialGLM.priorSignedLeft_allTrials.name = 'priorSignedLeft_allTrials'; 
options.TrialGLM.priorSignedLeft_allTrials.regressors = options.regressors([20, 31]);

options.TrialGLM.priorSignedLeft_abXrel_allTrials.name = 'priorSignedLeft_abXrel_allTrials'; 
options.TrialGLM.priorSignedLeft_abXrel_allTrials.regressors = options.regressors([20, 31, 32]);



options.TrialGLM.absoluted_prior_abXrelFold_correctTrials.name = 'absoluted_prior_abXrelFold_correctTrials'; 
options.TrialGLM.absoluted_prior_abXrelFold_correctTrials.regressors = options.regressors([1, 3, 18, 17, 5]);
options.TrialGLM.absoluted_prior_abXrelFold_correctTrials.selectTypeFlag = 'correct';

options.TrialGLM.absoluted_prior_abXrelFold_wrongTrials.name = 'absoluted_prior_abXrelFold_wrongTrials'; 
options.TrialGLM.absoluted_prior_abXrelFold_wrongTrials.regressors = options.regressors([1, 3, 18, 17, 5]);
options.TrialGLM.absoluted_prior_abXrelFold_wrongTrials.selectTypeFlag = 'wrong';





options.TrialGLM.mean_correctTrials.name = 'mean_correctTrials'; 
options.TrialGLM.mean_correctTrials.regressors = options.regressors([5]);
options.TrialGLM.mean_correctTrials.selectTypeFlag = 'correct';

options.TrialGLM.mean_wrongTrials.name = 'mean_wrongTrials'; 
options.TrialGLM.mean_wrongTrials.regressors = options.regressors([5]);
options.TrialGLM.mean_wrongTrials.selectTypeFlag = 'wrong';









options.TrialGLM.regressor_correlation.name = 'regressor_correlation';
options.TrialGLM.regressor_correlation.regressors = options.regressors([1, 3, 6, 12, 16, 17, 18, 20, 21, 22, 23, 24, 31, 32]);

options.behaviouralGLM.regressor_correlation_behaviour.name = 'regressor_correlation_behaviour';
options.behaviouralGLM.regressor_correlation_behaviour.regressors = options.regressors([25:30, 1 2 16 19, 34:36]);


 

options.behaviouralGLM.lagFeedback.name = 'lagFeedback'; 
options.behaviouralGLM.lagFeedback.regressors = options.regressors([25 1 2 16]);

options.behaviouralGLM.lagResponse.name = 'lagResponse'; 
options.behaviouralGLM.lagResponse.regressors = options.regressors([26 1 2 16]);

options.behaviouralGLM.lagResponseIntFb.name = 'lagResponseIntFb'; 
options.behaviouralGLM.lagResponseIntFb.regressors = options.regressors([25 26 27 1 2 16]);

options.behaviouralGLM.SignedPrior.name = 'SignedPrior'; 
options.behaviouralGLM.SignedPrior.regressors = options.regressors([19 1 2 34]);

options.behaviouralGLM.SignedPriorIntRel.name = 'SignedPriorIntRel'; 
options.behaviouralGLM.SignedPriorIntRel.regressors = options.regressors([19 28 1 2 16]);

options.behaviouralGLM.SignedPriorIntAbs.name = 'SignedPriorIntAbs'; 
options.behaviouralGLM.SignedPriorIntAbs.regressors = options.regressors([19 29 1 2 16]);

options.behaviouralGLM.SignedPriorIntAbsRel.name = 'SignedPriorIntAbsRel'; 
options.behaviouralGLM.SignedPriorIntAbsRel.regressors = options.regressors([19 30 1 2 16]);

options.behaviouralGLM.lagFeedbackAbRelt_1.name = 'lagFeedbackAbRelt_1'; 
options.behaviouralGLM.lagFeedbackAbRelt_1.regressors = options.regressors([25 1 2 34]);

options.behaviouralGLM.SignedPriorIntWithAllEvidenceRegressors.name = 'SignedPriorIntWithAllEvidenceRegressors'; 
options.behaviouralGLM.SignedPriorIntWithAllEvidenceRegressors.regressors = options.regressors([19 28:30 1 2 16]);

options.behaviouralGLM.lagFeedbackAbXRelAbRelt_1.name = 'lagFeedbackAbXRelAbRelt_1'; 
options.behaviouralGLM.lagFeedbackAbXRelAbRelt_1.regressors = options.regressors([25 34 35 36]);


options.TrialGLM.first_glm.data             = fullfile(options.paths.analysisData,'first_glm_timeLockData.mat');
options.TrialGLM.correctIncorrectTR.data    = fullfile(options.paths.analysisData,'correctIncorrectTR_timeLockData.mat');
options.TrialGLM.absoluted_prior.data       = fullfile(options.paths.analysisData,'absoluted_prior_timeLockData.mat');
options.TrialGLM.surprise_fb.data           = fullfile(options.paths.analysisData,'surprise_fb_timeLockData.mat');
options.TrialGLM.surprise_saccade.data      = fullfile(options.paths.analysisData,'surprise_saccade_timeLockData.mat');
options.TrialGLM.surprise_wtPst.data        = fullfile(options.paths.analysisData,'surprise_wtPst_timeLockData.mat');
options.TrialGLM.update_prior.data          = fullfile(options.paths.analysisData,'update_prior_timeLockData.mat');
options.TrialGLM.surprise_wtPst_with_rel_ab_reg.data          = fullfile(options.paths.analysisData,'surprise_wtPst_with_rel_ab_reg_timeLockData.mat');
options.TrialGLM.prediction_error.data      = fullfile(options.paths.analysisData,'prediction_error_timeLockData.mat');
options.TrialGLM.prediction_error_orthog.data      = fullfile(options.paths.analysisData,'prediction_error_orthog_timeLockData.mat');
options.TrialGLM.prediction_error_correctTrials.data      = fullfile(options.paths.analysisData,'prediction_error_correctTrials_timeLockData.mat');
options.TrialGLM.prediction_error_wrongTrials.data      = fullfile(options.paths.analysisData,'prediction_error_wrongTrials_timeLockData.mat');
options.TrialGLM.priorInteraction_correctTrials.data      = fullfile(options.paths.analysisData,'priorInteraction_correctTrials_timeLockData.mat');
options.TrialGLM.priorInteraction_wrongTrials.data      = fullfile(options.paths.analysisData,'priorInteraction_wrongTrials_timeLockData.mat');


options.TrialGLM.priorLeftResp_correctTrials.data      = fullfile(options.paths.analysisData,'priorLeftResp_correctTrials_timeLockData.mat');
options.TrialGLM.priorLeftResp_wrongTrials.data      = fullfile(options.paths.analysisData,'priorLeftResp_wrongTrials_timeLockData.mat');



options.TrialGLM.abXrel_LeftResp_correctTrials.data      = fullfile(options.paths.analysisData,'abXrel_LeftResp_correctTrials_timeLockData.mat');
options.TrialGLM.abXrel_LeftResp_wrongTrials.data      = fullfile(options.paths.analysisData,'abXrel_LeftResp_wrongTrials_timeLockData.mat');



options.TrialGLM.abXrel_allTrials.data      = fullfile(options.paths.analysisData,'abXrel_allTrials_timeLockData.mat');
options.TrialGLM.priorSignedLeft_allTrials.data      = fullfile(options.paths.analysisData,'priorSignedLeft_allTrials_timeLockData.mat');
options.TrialGLM.priorSignedLeft_abXrel_allTrials.data      = fullfile(options.paths.analysisData,'priorSignedLeft_abXrel_allTrials_timeLockData.mat');


options.TrialGLM.absoluted_prior_abXrelFold_correctTrials.data      = fullfile(options.paths.analysisData,'absoluted_prior_abXrelFold_correctTrials_timeLockData.mat');
options.TrialGLM.absoluted_prior_abXrelFold_wrongTrials.data      = fullfile(options.paths.analysisData,'absoluted_prior_abXrelFold_wrongTrials_timeLockData.mat');



options.TrialGLM.mean_correctTrials.data      = fullfile(options.paths.analysisData,'mean_correctTrials_timeLockData.mat');
options.TrialGLM.mean_wrongTrials.data      = fullfile(options.paths.analysisData,'mean_wrongTrials_timeLockData.mat');






options.TrialGLM.correctIncorrectTR.dataSubjects    = fullfile(options.paths.analysisData,'correctIncorrectTR_timeLockDataSubjects.mat');
options.TrialGLM.correctIncorrectTR.dataSubjects    = fullfile(options.paths.analysisData,'correctIncorrectTR_timeLockDataSubjects.mat');
options.TrialGLM.absoluted_prior.dataSubjects       = fullfile(options.paths.analysisData,'absoluted_prior_timeLockDataSubjects.mat');
options.TrialGLM.surprise_fb.dataSubjects           = fullfile(options.paths.analysisData,'surprise_fb_timeLockDataSubjects.mat');
options.TrialGLM.surprise_saccade.dataSubjects      = fullfile(options.paths.analysisData,'surprise_saccade_timeLockDataSubjects.mat');
options.TrialGLM.surprise_wtPst.dataSubjects        = fullfile(options.paths.analysisData,'surprise_wtPst_timeLockDataSubjects.mat');
options.TrialGLM.update_prior.dataSubjects          = fullfile(options.paths.analysisData,'update_prior_timeLockDataSubjects.mat');
options.TrialGLM.surprise_wtPst_with_rel_ab_reg.dataSubjects          = fullfile(options.paths.analysisData,'surprise_wtPst_with_rel_ab_reg_timeLockDataSubjects.mat');
options.TrialGLM.prediction_error.dataSubjects      = fullfile(options.paths.analysisData,'prediction_error_timeLockDataSubjects.mat');
options.TrialGLM.prediction_error_orthog.dataSubjects      = fullfile(options.paths.analysisData,'prediction_error_orthog_timeLockDataSubjects.mat');
options.TrialGLM.prediction_error_wrongTrials.dataSubjects      = fullfile(options.paths.analysisData,'prediction_error_wrongTrials_timeLockDataSubjects.mat');
options.TrialGLM.prediction_error_correctTrials.dataSubjects      = fullfile(options.paths.analysisData,'prediction_error_correctTrials_timeLockDataSubjects.mat');
options.TrialGLM.priorInteraction_correctTrials.dataSubjects      = fullfile(options.paths.analysisData,'priorInteraction_correctTrials_timeLockDataSubjects.mat');
options.TrialGLM.priorInteraction_wrongTrials.dataSubjects      = fullfile(options.paths.analysisData,'priorInteraction_wrongTrials_timeLockDataSubjects.mat');


options.TrialGLM.priorLeftResp_correctTrials.dataSubjects      = fullfile(options.paths.analysisData,'priorLeftResp_correctTrials_timeLockDataSubjects.mat');
options.TrialGLM.priorLeftResp_wrongTrials.dataSubjects      = fullfile(options.paths.analysisData,'priorLeftResp_wrongTrials_timeLockDataSubjects.mat');


options.TrialGLM.abXrel_LeftResp_correctTrials.dataSubjects      = fullfile(options.paths.analysisData,'abXrel_LeftResp_correctTrials_timeLockDataSubjects.mat');
options.TrialGLM.abXrel_LeftResp_wrongTrials.dataSubjects      = fullfile(options.paths.analysisData,'abXrel_LeftResp_wrongTrials_timeLockDataSubjects.mat');


options.TrialGLM.abXrel_allTrials.dataSubjects      = fullfile(options.paths.analysisData,'abXrel_allTrials_timeLockDataSubjects.mat');
options.TrialGLM.priorSignedLeft_allTrials.dataSubjects      = fullfile(options.paths.analysisData,'priorSignedLeft_allTrials_timeLockDataSubjects.mat');
options.TrialGLM.priorSignedLeft_abXrel_allTrials.dataSubjects      = fullfile(options.paths.analysisData,'priorSignedLeft_abXrel_allTrials_timeLockDataSubjects.mat');




options.TrialGLM.absoluted_prior_abXrelFold_correctTrials.dataSubjects      = fullfile(options.paths.analysisData,'absoluted_prior_abXrelFold_correctTrials_timeLockDataSubjects.mat');
options.TrialGLM.absoluted_prior_abXrelFold_wrongTrials.dataSubjects      = fullfile(options.paths.analysisData,'absoluted_prior_abXrelFold_wrongTrials_timeLockDataSubjects.mat');


options.TrialGLM.mean_correctTrials.dataSubjects      = fullfile(options.paths.analysisData,'mean_correctTrials_timeLockDataSubjects.mat');
options.TrialGLM.mean_wrongTrials.dataSubjects      = fullfile(options.paths.analysisData,'mean_wrongTrials_timeLockDataSubjects.mat');




options.behaviouralGLM.lags = 3; 

options.ERPAnalysis.correctIncorrectTR.data = fullfile(options.paths.analysisData,'correctIncorrectTR_ERPData.mat');

options.beamformed.chanLabels = {'V123_L','V123_R','hMT_L','hMT_R','pIPS_L','pIPS_R','aIPS_L','aIPS_R'};
options.channel.chanLabels = {
    'MEG0733MEG0732';
    'MEG0743MEG0742';
    'MEG1822MEG1823';
    'MEG1832MEG1833';
    'MEG1843MEG1842';
    'MEG2013MEG2012';
    'MEG2023MEG2022';
    'MEG2212MEG2213';
    'MEG2242MEG2243';
    'MEG2312MEG2313';
    'leftEye'       };

options.BehaviourMatch = [ 1 2 4 5 6 8 : 24 26 28 29 ];

end