clear all
close all
options = generate_perc_dyn_options();
for subject = 1: options.badTrialsRemoved.numSubjects
    disp(subject)
    details = generate_perdyn_subject_details(subject,options);
    
    
    MEGDATRUN = nan(details.badTrialsRemoved.subject.runs,205);
    for runid = 1:details.badTrialsRemoved.subject.runs
        
        clear data
        load(details.badTrialsRemoved.channel.datafile(runid).run);
        
        % identify eyeblinks
        
        timeLock = perc_dyn_timelock_analysis(data.ftData);
        
        
        channels = {'MEG0733MEG0732'; 'MEG0743MEG0742'; 'MEG1822MEG1823';'MEG1832MEG1833';...
            'MEG1843MEG1842'; 'MEG2013MEG2012'; 'MEG2023MEG2022';'MEG2212MEG2213';...
            'MEG2242MEG2243'; 'MEG2312MEG2313'};
        timeLockAll = perc_dyn_timelock_analysis(data.ftData, channels);
        
        MEGDATTRIAL = nan(size(timeLock.trial,1),205); 
        for trial = 1:size(timeLock.trial,1)
            
            EyeTrace = squeeze(timeLock.trial(trial,11,:));
            
            idxEye1 = find((EyeTrace(1:end-1)> -1.5*(10^6) & EyeTrace(2:end)<-1.5*(10^6)) );
            
            idxEye2 = find((EyeTrace(1:end-4)<0.5*(10^6) & EyeTrace(5:end)>1.5*(10^6)) ); %| (EyeTrace(1:end-2)<-0.5*(10^6) & EyeTrace(3:end)>-1*(10^6)) );
            
            
            
            %         figure
            %         hold on
            %          plot(timeLock.time,EyeTrace)
            %             plot(timeLock.time(idxEye),ones(length(idxEye),1).*-3*(10^6),'kx','MarkerSize',3)
            %             plot(timeLock.time(idxEye2),ones(length(idxEye2),1).*1*(10^6),'kx','MarkerSize',3)
            %
            idxEye = [idxEye1; idxEye2];
            idxEye = [idxEye1];
            
            idxEye = [idxEye-102 idxEye+102];
            
            
            idxEye(idxEye(:,1) < 1,:) = [];
            idxEye(idxEye(:,2) > 1600,:) = [];
            
            
            
            
            MEGDATBlink = nan(size(idxEye,1),205); 
            for blink = 1:length(idxEye(:,1))
               
               MEGDATBlink(blink,:) =  squeeze(timeLockAll.trial(trial,1,[idxEye(blink,1) : idxEye(blink,2) ]));
                

            end
            
            
            
            MEGDATTRIAL(trial,:) = nanmean(MEGDATBlink,1);
            
            
        end
        
        MEGDATRUN(runid,:) = nanmean(MEGDATTRIAL,1);
        
    end
    
    MEGDATSubject(subject, :) = nanmean(MEGDATRUN,1);
    
end

AVGTotal = nanmean(MEGDATSubject,1);

keyboard;
figure 
plot(timeLock.time(301 : 301+204),AVGTotal)
for subject = 1: options.badTrialsRemoved.numSubjects
    
 subplot(5,5,subject)
 plot(timeLock.time(301 : 301+204), MEGDATSubject(subject,:))
 
    
    
end 

%%


clear all
close all
options = generate_perc_dyn_options();
for subject = 1: options.badTrialsRemoved.numSubjects
    disp(subject)
    details = generate_perdyn_subject_details(subject,options);
    
    
    MEGDATRUN = nan(details.badTrialsRemoved.subject.runs,205,11);
    for runid = 1:details.badTrialsRemoved.subject.runs
        
        clear data
        load(details.badTrialsRemoved.channel.datafile(runid).run);
        
        % identify eyeblinks
        
        timeLock = perc_dyn_timelock_analysis(data.ftData);
        
        

        
        MEGDATTRIAL = nan(size(timeLock.trial,1),205,11); 
        for trial = 1:size(timeLock.trial,1)
            
            EyeTrace = squeeze(timeLock.trial(trial,11,:));
            
            idxEye1 = find((EyeTrace(1:end-1)> -1.5*(10^6) & EyeTrace(2:end)<-1.5*(10^6)) );
            
            idxEye2 = find((EyeTrace(1:end-4)<0.5*(10^6) & EyeTrace(5:end)>1.5*(10^6)) ); %| (EyeTrace(1:end-2)<-0.5*(10^6) & EyeTrace(3:end)>-1*(10^6)) );
            
            
            
%                     figure
%                     hold on
%                      plot(timeLock.time,EyeTrace)
%                         plot(timeLock.time(idxEye1),ones(length(idxEye1),1).*-3*(10^6),'kx','MarkerSize',3)
%                         plot(timeLock.time(idxEye2),ones(length(idxEye2),1).*1*(10^6),'kx','MarkerSize',3)
%             
            idxEye = [idxEye1; idxEye2];
            idxEye = [idxEye1];
            
            idxEye = [idxEye-102 idxEye+102];
            
            
            idxEye(idxEye(:,1) < 1,:) = [];
            idxEye(idxEye(:,2) > 1600,:) = [];
            
            
            
            
            MEGDATBlink = nan(size(idxEye,1),205,11); 
            for blink = 1:length(idxEye(:,1))
               
               for channel = 1:11 
               MEGDATBlink(blink,:,channel) =  squeeze(timeLock.trial(trial,channel,[idxEye(blink,1) : idxEye(blink,2) ]));
               end 

            end
            
          
            
            MEGDATTRIAL(trial,:,:) = nanmean(MEGDATBlink,1);
           
            
        end
      
        MEGDATRUN(runid,:,:) = nanmean(MEGDATTRIAL,1);
        
    end
    
    MEGDATSubject(subject, :,:) = nanmean(MEGDATRUN,1);
    
end

AVGTotal = squeeze(nanmean(MEGDATSubject,1));


% 
figure 
for channel = 1:11 
    
    subplot(6,2,channel)
    plot(timeLock.time(301 : 301+204), AVGTotal(:,channel))
    title(timeLock.label{channel})
    
    
    
end 

%% 
clear all
close all
options = generate_perc_dyn_options();
glmFlag =  'correctIncorrectTR';
for subject = 1: options.badTrialsRemoved.numSubjects
    disp(subject)
    details = generate_perdyn_subject_details(subject,options);
    
    
    MEGDATRUN = nan(details.badTrialsRemoved.subject.runs,205,4);
    for runid = 1:details.badTrialsRemoved.subject.runs
        
        clear data
        load(details.badTrialsRemoved.channel.datafile(runid).run);
      
        % identify eyeblinks
        
        timeLock = perc_dyn_timelock_analysis(data.ftData);
        
        
        channels = {'MEG0733MEG0732'; 'MEG0743MEG0742'; 'MEG1822MEG1823';'MEG1832MEG1833';...
            'MEG1843MEG1842'; 'MEG2013MEG2012'; 'MEG2023MEG2022';'MEG2212MEG2213';...
            'MEG2242MEG2243'; 'MEG2312MEG2313'};
        timeLockAll = perc_dyn_timelock_analysis(data.ftData, channels);
        
        MEGDATTRIAL = nan(size(timeLock.trial,1),205); 
        for trial = 1:size(timeLock.trial,1)
            
            EyeTrace = squeeze(timeLock.trial(trial,11,:));
            
            idxEye1 = find((EyeTrace(1:end-1)> -1.5*(10^6) & EyeTrace(2:end)<-1.5*(10^6)) );
            
            idxEye2 = find((EyeTrace(1:end-4)<0.5*(10^6) & EyeTrace(5:end)>1.5*(10^6)) ); %| (EyeTrace(1:end-2)<-0.5*(10^6) & EyeTrace(3:end)>-1*(10^6)) );
            
            
            
            %         figure
            %         hold on
            %          plot(timeLock.time,EyeTrace)
            %             plot(timeLock.time(idxEye),ones(length(idxEye),1).*-3*(10^6),'kx','MarkerSize',3)
            %             plot(timeLock.time(idxEye2),ones(length(idxEye2),1).*1*(10^6),'kx','MarkerSize',3)
            %
            idxEye = [idxEye1; idxEye2];
            idxEye = [idxEye1];
            
            idxEye = [idxEye-102 idxEye+102];
            
            
            idxEye(idxEye(:,1) < 1,:) = [];
            idxEye(idxEye(:,2) > 1600,:) = [];
            
            
            
            
            MEGDATBlink = nan(size(idxEye,1),205); 
            for blink = 1:length(idxEye(:,1))
               
               MEGDATBlink(blink,:) =  squeeze(timeLockAll.trial(trial,1,[idxEye(blink,1) : idxEye(blink,2) ]));
                

            end
            
            
            
            MEGDATTRIAL(trial,:) = nanmean(MEGDATBlink,1);
            
            
        end
      
        cfg = perc_dyn_create_design_matrix(data,options,glmFlag);
        
        for reg = 1:4
            idx = cfg.design(:,reg);
        MEGDATRUN(runid,:,reg) = nanmean(MEGDATTRIAL(logical(idx),:),1);
        end
    end
 
    MEGDATSubject(subject, :,:) = nanmean(MEGDATRUN,1);
    
end

AVGTotal = nanmean(MEGDATSubject,1);


figure 
for reg = 1:4
    hold on 
    plot(timeLock.time(301 : 301+204),AVGTotal(1,:,reg),'LineWidth',3)
    legend(cfg.label)
    
    
end 
tidyfig; 

% plot(timeLock.time(301 : 301+204),AVGTotal)
% for subject = 1: options.badTrialsRemoved.numSubjects
%     
%  subplot(5,5,subject)
%  plot(timeLock.time(301 : 301+204), MEGDATSubject(subject,:))
%  
%     
%     
% end 
