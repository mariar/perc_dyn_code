function [data] = perc_dyn_combine_planar(data)




for trial = 1:length(data.trial)
    newChannel = 1;
    for channel = [1:2:19]
        
        channelData1 = data.trial{trial}(channel,:);
        channelData2 = data.trial{trial}(channel+1,:);
        
        combined = sqrt(channelData1.^2 + channelData2.^2);
        
        
        CopyData.trial{trial}(newChannel,:) = combined;
        CopyData.label{newChannel} =strcat(data.label{channel},data.label{channel+1});
        
        
        newChannel = newChannel+1;
    end
    
    CopyData.trial{trial}(newChannel,:) = data.trial{trial}(21,:);
    CopyData.label{newChannel} = 'leftEye';
    
end

data.trial = CopyData.trial; 
data.label = CopyData.label; 














end