function data = perc_dyn_smooth(data, field)


switch field 
    
    case 'normal' 
        
        for channel = 1:10 
            
            newChannel(channel,:) = conv(data.avg(channel,:), (1/10).* ones(10,1),'same');
            data.avg = newChannel; 
        end 
        
    case 'meanGLM' 
        
        newChannel = conv(data, (1/15).* ones(15,1),'same');
        data = newChannel;
    otherwise 
        
        newChannel = conv(data.avg, (1/15).* ones(15,1),'same');
        data.avg = newChannel; 
end 



end 