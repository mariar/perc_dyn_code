clear all

flags = {'absoluted_prior'};




for flag = 1:length(flags)
    
    
    
    options = generate_perc_dyn_options();
    glmFlag = flags{flag};
    
    
    if isfield(options.TrialGLM.(glmFlag),'selectTypeFlag')
    selectTypeFlag = options.TrialGLM.(glmFlag).selectTypeFlag; 
    selectFlag = 1; 
  
    else 
        selectTypeFlag = [];
        selectFlag = 0; 
   
    end 
    
    clear glm timelockAverage glmResults
    
    for subject = 1: options.badTrialsRemoved.numSubjects
        disp(subject)
        details = generate_perdyn_subject_details(subject,options);
        
        clear timeLock timeLockLeft timeLockRight timeLockCentre timeLockAll
        for runid = 1:details.badTrialsRemoved.subject.runs
            
            
           dataLoad =  load(details.badTrialsRemoved.channel.datafile(runid).run);
            
            data{runid} = dataLoad.data; 
            
            
            
            timeLock{runid} = perc_dyn_timelock_analysis(data{runid}.ftData);
            
            channels = {'MEG1822MEG1823';'MEG1843MEG1842'};
            timeLockLeft{runid} = perc_dyn_timelock_analysis(data{runid}.ftData, channels);
            
            channels =     'MEG2212MEG2213'; 'MEG2312MEG2313';
            timeLockRight{runid} = perc_dyn_timelock_analysis(data{runid}.ftData, channels);
            
            channels = {'MEG0733MEG0732';'MEG0743MEG0742';'MEG1832MEG1833';    'MEG2013MEG2012'; ...
                'MEG2023MEG2022'; 'MEG2242MEG2243'};
            timeLockCentre{runid} = perc_dyn_timelock_analysis(data{runid}.ftData, channels);
            
            channels = {'MEG0733MEG0732'; 'MEG0743MEG0742'; 'MEG1822MEG1823';'MEG1832MEG1833';...
                'MEG1843MEG1842'; 'MEG2013MEG2012'; 'MEG2023MEG2022';'MEG2212MEG2213';...
                'MEG2242MEG2243'; 'MEG2312MEG2313'};
            timeLockAll{runid} = perc_dyn_timelock_analysis(data{runid}.ftData, channels);
            
         
            
            % combine data across runs
            
            
        end
        
        timeLockAllappended = perc_dyn_appendata_for_glm_analysis(timeLockAll);
        
        timeLockCentreappended = perc_dyn_appendata_for_glm_analysis(timeLockCentre);
      
        timeLockRightappended= perc_dyn_appendata_for_glm_analysis(timeLockRight);
       
        timeLockLeftappended= perc_dyn_appendata_for_glm_analysis(timeLockLeft);

        timeLockappended= perc_dyn_appendata_for_glm_analysis(timeLock);

        [selectedDataAll,selectTrialIdx]  =  perc_dyn_select_correct_incorrect_trials(data,timeLockAllappended,details,selectFlag,selectTypeFlag);
        [selectedDataCentre,selectTrialIdx]  =  perc_dyn_select_correct_incorrect_trials(data,timeLockCentreappended,details,selectFlag,selectTypeFlag);
        [selectedDataLeft,selectTrialIdx]  =  perc_dyn_select_correct_incorrect_trials(data,timeLockLeftappended,details,selectFlag,selectTypeFlag);
        [selectedDataRight,selectTrialIdx]  =  perc_dyn_select_correct_incorrect_trials(data,timeLockRightappended,details,selectFlag,selectTypeFlag);
        [selectedData,selectTrialIdx]  =  perc_dyn_select_correct_incorrect_trials(data,timeLockappended,details,selectFlag,selectTypeFlag);
        
        cfg = perc_dyn_create_design_matrix(data,options,glmFlag,1,details,selectTrialIdx);
 
    %subjectDist{subject} = perc_dyn_calculate_rewardProb_error_distribution(cfg);
    
        
        [timelockAverage.normal{subject}] = fast_glm_wrapper(cfg, selectedData);
        [timelockAverage.Left{subject}] = fast_glm_wrapper(cfg, selectedDataLeft);
        [timelockAverage.Right{subject}] = fast_glm_wrapper(cfg, selectedDataRight);
        [timelockAverage.Centre{subject}] = fast_glm_wrapper(cfg, selectedDataCentre);
        [timelockAverage.All{subject}] = fast_glm_wrapper(cfg, selectedDataAll);
%         
%      
%         %         cfg             = [];
        %         cfg.channel     = {'pIPS_L'};
        %         cfg.parameter   = 'saccade_dir_t';
        %         figure('position',[0 300 1000 300])
        %         ft_singleplotER(cfg,glm)
        
        
        
        
        
        
%         [timelockAverage.normal(subject)] = perc_dyn_timelock_grand_average(glm,options,'within',glmFlag);
%         [timelockAverage.Left(subject)] = perc_dyn_timelock_grand_average(glmLeft,options,'within',glmFlag);
%         [timelockAverage.Right(subject)] = perc_dyn_timelock_grand_average(glmRight,options,'within',glmFlag);
%         [timelockAverage.Centre(subject)] = perc_dyn_timelock_grand_average(glmCentre,options,'within',glmFlag);
%         [timelockAverage.All(subject)] = perc_dyn_timelock_grand_average(glmAll,options,'within',glmFlag);
    end
    
    
  
    glmResults.Original = perc_dyn_timelock_grand_average(timelockAverage.normal,options,'within',glmFlag);
    glmResults.Left = perc_dyn_timelock_grand_average(timelockAverage.Left,options,'within',glmFlag);
    glmResults.Right = perc_dyn_timelock_grand_average(timelockAverage.Right,options,'within',glmFlag);
    glmResults.Centre = perc_dyn_timelock_grand_average(timelockAverage.Centre,options,'within',glmFlag);
    glmResults.All = perc_dyn_timelock_grand_average(timelockAverage.All,options,'within',glmFlag);
    
    save(options.TrialGLM.(glmFlag).data,'glmResults');
    save(options.TrialGLM.(glmFlag).dataSubjects,'timelockAverage');
end

%% plot subjectDist 
figure
for subject = 1:25 
    
    subplot(5,5,subject) 
    
    plot(1:length(subjectDist{subject}),subjectDist{subject}); 
    ylim([0 1])
    if subject ==1 
        
        title('reward Probability for wrong trials')
        ylabel('reward Probability')
        xlabel('trial')
    end 
    
    tidyfig;
end 

%% contrast analysis

clear all
close all

options = generate_perc_dyn_options();
glmFlag = 'correctIncorrectTR';
load(options.TrialGLM.(glmFlag).dataSubjects)
% contrast correct - wrong 1 1 -1 -1

contrast = [-1  -1  1 1]';
field = 'All';

% for betas of each subject

for subject = 1:length(timelockAverage.(field))
    
    % smooth data
    
    for label = 1:length(options.TrialGLM.(glmFlag).regressors)
        regLabel = [options.TrialGLM.(glmFlag).regressors(label).regressorLabel,'_b'];
        timelockAverage.(field)(subject).(regLabel) = perc_dyn_smooth(timelockAverage.(field)(subject).(regLabel), field);
    end
    
    Cope = [timelockAverage.(field)(subject).left_correct_trials_b.avg; timelockAverage.(field)(subject).right_correct_trials_b.avg;  timelockAverage.(field)(subject).right_wrong_trials_b.avg;  timelockAverage.(field)(subject).left_wrong_trials_b.avg ]';
    
    diffSubject(subject,:) = Cope * contrast;
    
    
    
    
end
% ttest for each point

for time = 1:size(diffSubject,2)
    
    [~,~,~,stats] = ttest(diffSubject(:,time));
    timetTstat(time) = stats.tstat;
    
    
end



meanDiff = nanmean(diffSubject,1);



figure
subplot(2,1,1)
plot(timelockAverage.(field)(1).left_correct_trials_b.time, meanDiff)
ylabel('beta')
xlim([3 5])
tidyfig;

subplot(2,1,2)
plot(timelockAverage.(field)(1).left_correct_trials_b.time, timetTstat)
yticks(-6 : 6)
ylabel('tstat')
xlabel('time sec')
xlim([3 5])
tidyfig

%%
% cfg             = [];
% cfg.channel     = {'pIPS_R'};
% %cfg.parameter   = 'saccade_direction_t';
% figure('position',[0 300 1000 300])
% ft_singleplotER(cfg,timelockAverageAllSubjects.saccade_direction_t)