clear all

options = generate_perc_dyn_options();
glmFlag = 'regressor_correlation_behaviour';
[cl] = cbrewer('div', 'RdGy', 400);
cl = flip(cl);
for subject = 1: options.badTrialsRemoved.numSubjects
    disp(subject)
    details = generate_perdyn_subject_details(subject,options);
    designMatrix = [];
    for runid = 1:details.badTrialsRemoved.subject.runs
        
        
        dataLoad =  load(details.badTrialsRemoved.channel.datafile(runid).run);
        
        data{runid} = dataLoad.data;
        
        
        channels = {'MEG0733MEG0732'; 'MEG0743MEG0742'; 'MEG1822MEG1823';'MEG1832MEG1833';...
            'MEG1843MEG1842'; 'MEG2013MEG2012'; 'MEG2023MEG2022';'MEG2212MEG2213';...
            'MEG2242MEG2243'; 'MEG2312MEG2313'};
        timeLockAll{runid} = perc_dyn_timelock_analysis(data{runid}.ftData, channels);;
    end
    
    timeLockAllappended = perc_dyn_appendata_for_glm_analysis(timeLockAll);
    [selectedDataAll,selectTrialIdx]  =  perc_dyn_select_correct_incorrect_trials(data,timeLockAllappended,details,0,[]);
    
    cfg = perc_dyn_create_design_matrix(data,options,glmFlag,0,details,selectTrialIdx);
    
    
    designMatrix = [designMatrix; cfg.design];
    
    
  
    
    




correlationMatrix(:,:,subject) = corr(designMatrix);


%
%   subplot(5,5,subject)
%   imagesc(squeeze(correlationMatrix(:,:,subject)))
%   colormap(cl)
%    h = colorbar;
%    h.Limits = [-1 1];
%





end

correlationMatrixAvg = round(mean(correlationMatrix,3),2);



figure
h = heatmap(correlationMatrixAvg);
colormap(cl)
h.ColorLimits =[-1 1];
h.XDisplayLabels = {cfg.label{2:end}};
h.YDisplayLabels = {cfg.label{2:end}};

% h = colorbar;
% h.Limits = [-1 1];