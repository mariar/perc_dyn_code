% plot eye traces for subjects to check whether they influence the MEG
% signal 

clear all
close all
options = generate_perc_dyn_options();

for subject = 1: options.badTrialsRemoved.numSubjects
    disp(subject)
    details = generate_perdyn_subject_details(subject,options);
    figure
    for runid = 1:details.badTrialsRemoved.subject.runs
        
        clear data
        load(details.badTrialsRemoved.channel.datafile(runid).run);
        
        timeLock = perc_dyn_timelock_analysis(data.ftData);
        cfg = []; 
        cfg.channel = {'leftEye'}; 
       cfg.parameter = 'trial';
     
        eyeTraceAvg = ft_timelockanalysis(cfg, timeLock);
     
        subplot(2,2,runid)
        cfg = []; 
          cfg.xlim          = [3.5 6];
        ft_singleplotER(cfg,eyeTraceAvg); 
        
    end
end

%% plot traces for each trial on top of each other 
for subject = 1: options.badTrialsRemoved.numSubjects
    disp(subject)
    details = generate_perdyn_subject_details(subject,options);
    figure
    for runid = 1:details.badTrialsRemoved.subject.runs
        
        clear data
        load(details.badTrialsRemoved.channel.datafile(runid).run);
        
        timeLock = perc_dyn_timelock_analysis(data.ftData);

 subplot(2,2,runid)
 plot(timeLock.time, squeeze(timeLock.trial(:,11,:))) 
    

xlim([3.5 6])
hold off 
      
      
    end
end