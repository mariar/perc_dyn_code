function timeLock = perc_dyn_timelock_analysis2(data, channelFlag)


channels = {'MEG0733MEG0732'; 'MEG0743MEG0742'; 'MEG1822MEG1823';'MEG1832MEG1833';...
                'MEG1843MEG1842'; 'MEG2013MEG2012'; 'MEG2023MEG2022';'MEG2212MEG2213';...
                'MEG2242MEG2243'; 'MEG2312MEG2313'};
 
if channelFlag 
    cfg = [];
    cfg.channel = channels; 
    cfg.avgoverchan = 'yes'; 
    cfg.nanmean = 'yes'; 
    
    data = ft_selectdata(cfg,data); 
    
end 
cfg = [];

cfg.keeptrials = 'yes'; 

timeLock        = ft_timelockanalysis(cfg,data);


end 