function [details] = generate_perdyn_subject_details2(id, options)

subid.raw = options.raw.subjectFolderList{id};
subid.badTrialsRemoved = options.badTrialsRemoved.subjectFolderList{id};
switch subid.raw
    
    
    
    
    
    
    
    
    
    case 'sub918' 
        details.raw.subject.runs = 3;
        details.raw.subject.runIDs = [1 2 3]; 
    case 'sub996'
        details.raw.subject.runs = 3;
        details.raw.subject.runIDs = [1 2 3]; 
    case 'sub574'
        details.raw.subject.runs = 3;
        details.raw.subject.runIDs = [2 3 4]; 
    case 'sub998'
        details.raw.subject.runs = 3;
        details.raw.subject.runIDs = [1 3 4];
    otherwise
        details.raw.subject.runs = 4;
        details.raw.subject.runIDs = [1 2 3 4]; 
end

switch subid.badTrialsRemoved
    case 'sub918' 
        details.badTrialsRemoved.subject.runs = 3;
        details.badTrialsRemoved.subject.runIDs = [1 2 3]; 
    case 'sub996'
        details.badTrialsRemoved.subject.runs = 3;
        details.badTrialsRemoved.subject.runIDs = [1 2 3]; 
        case 'sub574'
        details.badTrialsRemoved.subject.runs = 3;
        details.badTrialsRemoved.subject.runIDs = [2 3 4];
        case 'sub998'
        details.badTrialsRemoved.subject.runs = 3;
        details.badTrialsRemoved.subject.runIDs = [1 3 4];
    otherwise
        details.badTrialsRemoved.subject.runs = 4;
        details.badTrialsRemoved.subject.runIDs = [1 2 3 4]; 
end

details.subject.path.beamformed.raw = fullfile(options.paths.PreprocessedDataPath,subid.raw);
details.subject.path.channel.raw = fullfile(options.paths.ChannelDataPath,subid.raw);
details.subject.path.beamformed.badTrialsRemoved = fullfile(options.paths.badTrialsRemovedPath,'beamformed',subid.badTrialsRemoved);
details.subject.path.channel.badTrialsRemoved = fullfile(options.paths.badTrialsRemovedPath,'channel',subid.badTrialsRemoved);



if exist(details.subject.path.beamformed.badTrialsRemoved) ~= 7
    mkdir(details.subject.path.beamformed.badTrialsRemoved);
end

if exist(details.subject.path.channel.badTrialsRemoved) ~= 7
    mkdir(details.subject.path.channel.badTrialsRemoved);
end

for run = 1:details.raw.subject.runs
    runid = details.raw.subject.runIDs(run);
    
    details.raw.beamformed.datafile(run).run = fullfile(details.subject.path.beamformed.raw,sprintf([subid.raw '_run%1.0f_ftData.mat'],runid));
    details.raw.channel.datafile(run).run = fullfile(details.subject.path.channel.raw,sprintf([subid.raw '_run%1.0f_ftData.mat'],runid));

    details.raw.events(run).run = fullfile(details.subject.path.beamformed.raw,sprintf(['maria_' subid.raw(4:end) '_%1.0f_events.mat'],runid));
end

for run = 1:details.badTrialsRemoved.subject.runs
    runid = details.badTrialsRemoved.subject.runIDs(run);
    details.badTrialsRemoved.beamformed.datafile(run).run = fullfile(details.subject.path.beamformed.badTrialsRemoved,sprintf([subid.badTrialsRemoved '_run%1.0f_ftDataBadTrRm.mat'],runid));
    details.badTrialsRemoved.channel.datafile(run).run = fullfile(details.subject.path.channel.badTrialsRemoved,sprintf([subid.badTrialsRemoved '_run%1.0f_ftDataBadTrRm.mat'],runid));

    
    
    
end

details.Trials.AllTrials.datafile = fullfile(options.paths.dataForAnalysis,sprintf([subid.badTrialsRemoved '_All_Trials_For_GLM.mat']));
details.Trials.WrongTrials.datafile = fullfile(options.paths.dataForAnalysis,sprintf([subid.badTrialsRemoved '_Wrong_Trials_For_GLM.mat']));
details.Trials.CorrectTrials.datafile = fullfile(options.paths.dataForAnalysis,sprintf([subid.badTrialsRemoved '_Correct_Trials_For_GLM.mat']));
end