function dataAppended = perc_dyn_append_raw_struct2(data, regressors, subjectRuns)



% append MEG data
cfg = [];
cfg.keepsampleinfo = 'yes';
dataAppended.ftData = ft_appenddata(cfg, data{:});


dataAppended.reg_ab      = [];
dataAppended.reg_rt      =  [];
dataAppended.reg_saccdir = [];
dataAppended.reg_fbPos   = [];
dataAppended.reg_lh      = [];
dataAppended.reg_MLE_prior = [];
dataAppended.reg_MLE_withinTrial_posterior = [];
dataAppended.reg_pri     = [];
dataAppended.reg_pri2    = [];
dataAppended.reg_pris    = [];
dataAppended.reg_rel     = [];
dataAppended.reg_relf    = [];
dataAppended.reg_surp    = [];
dataAppended.reg_trn     = [];
dataAppended.reg_wtPos   = [];
dataAppended.reg_correctAnswers = [];
dataAppended.reg_feedback = [];
dataAppended.reg_switchprior = [];
dataAppended.reg_signed_prior_diff_to_0 = [];
dataAppended.reg_absoluted_prior_diff_to_0 =[];



for runid = 1:subjectRuns
    dataAppended.reg_ab      = [dataAppended.reg_ab; regressors{runid}.reg_ab];
    dataAppended.reg_rt      =  [dataAppended.reg_rt; regressors{runid}.reg_rt];
    dataAppended.reg_saccdir = [dataAppended.reg_saccdir; regressors{runid}.reg_saccdir];
    dataAppended.reg_fbPos   = [dataAppended.reg_fbPos; regressors{runid}.reg_fbPos];
    dataAppended.reg_lh      = [dataAppended.reg_lh; regressors{runid}.reg_lh];
    dataAppended.reg_MLE_prior = [dataAppended.reg_MLE_prior; regressors{runid}.reg_MLE_prior'];
    dataAppended.reg_MLE_withinTrial_posterior = [dataAppended.reg_MLE_withinTrial_posterior; regressors{runid}.reg_MLE_withinTrial_posterior'];
    dataAppended.reg_pri     = [dataAppended.reg_pri; regressors{runid}.reg_pri];
    dataAppended.reg_pri2    = [dataAppended.reg_pri2; regressors{runid}.reg_pri2];
    dataAppended.reg_pris    = [dataAppended.reg_pris; regressors{runid}.reg_pris];
    dataAppended.reg_rel     = [dataAppended.reg_rel; regressors{runid}.reg_rel];
    dataAppended.reg_relf    = [dataAppended.reg_relf; regressors{runid}.reg_relf];
    dataAppended.reg_surp    = [dataAppended.reg_surp; regressors{runid}.reg_surp];
    dataAppended.reg_trn     = [dataAppended.reg_trn; regressors{runid}.reg_trn];
    dataAppended.reg_wtPos   = [dataAppended.reg_wtPos; regressors{runid}.reg_wtPos];
    dataAppended.reg_correctAnswers = [dataAppended.reg_correctAnswers; regressors{runid}.reg_correctAnswers];
    dataAppended.reg_feedback = [dataAppended.reg_feedback; regressors{runid}.reg_feedback];
    dataAppended.reg_switchprior = [dataAppended.reg_switchprior; regressors{runid}.reg_switchprior];
    dataAppended.reg_signed_prior_diff_to_0 = [dataAppended.reg_signed_prior_diff_to_0; regressors{runid}.reg_signed_prior_diff_to_0];
    dataAppended.reg_absoluted_prior_diff_to_0 = [dataAppended.reg_absoluted_prior_diff_to_0; regressors{runid}.reg_absoluted_prior_diff_to_0];
end

end