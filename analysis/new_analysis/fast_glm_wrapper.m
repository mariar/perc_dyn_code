% fits regressors x trials glm to every channel and time point (for
% timelock data) or every channel, freq bin and time point (for freq data)
% gl = fast_glm_wrapper(cfg,data)
% where:
% cfg.design: design matrix (regressors x trials)
% cfg.labels: cell array of labels for regressors in dmat
% cfg.parameter: parameter of 'in' to fit glm to (eg 'powspctrm')
% cfg.error: return error or not (necessary for computing contrasts)

function [gl, ERR] = fast_glm_wrapper(cfg, data)

dmat        = cfg.design;
labels      = cfg.label;
parameter   = cfg.parameter;

if ~isfield(cfg,'error')
    cfg.error = 'no';
end

Y           = data.(parameter);

if strcmp(cfg.error, 'yes')
    ERR     = zeros(size(Y));
else
    ERR     = NaN;
end
% can consider log-transforming as optional input argument

% initialise output structure
gl          = [];
gl.label    = data.label;
gl.time     = data.time;
gl.cfg      = [];

% dimensions of parameter-of-interest
dimname     = tokenize(data.dimord, '_');
dimsize     = size(data.(parameter));
num_d       = length(dimsize);

tic
% preallocate for speed  
% B = zeros(size(Y));
% T = zeros(size(Y));

switch num_d
    
    case 3 % probably timelock data (trl x chan x time) or fft (trl x chan x freq)
        
        for i = 1:size(Y,2)         % loop over chan
            disp([num2str(i) ' / ' num2str(size(Y,2))]);
            for j = 1:size(Y,3)     % loop over time
                try
                    y           = (Y(:,i,j));
                    [b,t,err]   = fast_glm(dmat,y);
                    B(:,i,j)    = b;
                    T(:,i,j)    = t;
                    if strcmp(cfg.error,'yes')
                        ERR(:,i,j) = err;
                    end
                catch
                    B(:,i,j) = NaN;
                    T(:,i,j) = NaN;
                    if strcmp(cfg.error,'yes')
                        ERR(:,i,j) = NaN;
                    end
                end
            end
        end
        
        gl.dimord   = [dimname{2} '_' dimname{3}];
        
        szs = size(B);
        
        % give the output arguments names
        for i = 1:length(labels)
            
            gl.([labels{i} '_b']) = reshape(B(i,:,:),szs(2:end));
            gl.([labels{i} '_t']) = reshape(T(i,:,:),szs(2:end));
        end
        
    case 4 % probably timefreq data (trl x chan x freq x time)
        
        for i = 1:size(Y,2)             % loop over chan
            disp([num2str(i) ' / ' num2str(size(Y,2))]);
            for j = 1:size(Y,3)         % loop over freq
                for k = 1:size(Y,4)     % loop over time
                    try
                        y            = (Y(:,i,j,k));
                        [b, t, err]  = fast_glm(dmat,y);
                        B(:,i,j,k)   = b;
                        T(:,i,j,k)   = t;
                        if strcmp(cfg.error,'yes')
                            ERR(:,i,j,k) = err;
                        end
                    catch
                        disp('catch path');
                        B(:,i,j,k)   = NaN;
                        T(:,i,j,k)   = NaN;
                        if strcmp(cfg.error,'yes')
                            ERR(:,i,j,k) = NaN;
                        end
                    end
                end
            end
        end
        
        gl.dimord   = [dimname{2} '_' dimname{3} '_' dimname{4}];
        gl.freq     = data.freq;
        
        szs = size(B);
        
        % give the output arguments names
        for i = 1:length(labels)
            gl.([labels{i} '_b']) = reshape(B(i,:,:,:),szs(2:end));
            gl.([labels{i} '_t']) = reshape(T(i,:,:,:),szs(2:end));
        end
end

for i = 1:num_d
    disp(['dim ' num2str(i) ' is ' dimname{i} ' and is ' num2str(dimsize(i)) ' elements long.']);
end

toc


