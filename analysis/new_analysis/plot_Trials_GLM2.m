clear all
%close all
options = generate_perc_dyn_options2();


glmFlag = 'correctIncorrectTR';%'prediction_error_orthog'; %'prediction_error'%'prediction_error_orthog';
sensors = 'All'; 
trialFlag = 'all';

%% plot average across subjects 

  switch trialFlag
        case 'all'
            saveName = options.TrialGLM.(glmFlag).dataSubjects;
            
        case 'wrong'
            saveName = options.TrialGLM.Wrong.(glmFlag).dataSubjects;
            
        case 'correct'
            saveName = options.TrialGLM.Correct.(glmFlag).dataSubjects;
            
    end

load(saveName)

%% run a group GLM across all participants
numSubjects = length(timelockAverage.(sensors));
% make a design matrix across participants - just calculates the mean
dm = ones(numSubjects,1);

for regressor = 1:length(options.TrialGLM.(glmFlag).regressors) % loop through regressors
    regLabel = [options.TrialGLM.(glmFlag).regressors(regressor).regressorLabel,'_b'];
    [data] = perc_dyn_reshape_glm_data(timelockAverage.(sensors), regLabel, numSubjects);

    

    [cope,~,tstat] = ols(data(:,:),dm,1); %run GLM across participants
    
    avg_across_subjects{regressor} = reshape(cope,[size(data,2),size(data,3), size(data,4)]);
    tstat_across_subjects{regressor} = reshape(tstat,[size(data,2),size(data,3), size(data,4)]);
end




%% 

% for correct incorrect contrast
% cl = cbrewer('div', 'PRGn', 8);
% cl = cl([1, 3, 6, 8],:);

% for absolute prior all trials 
cl = cbrewer('div', 'Spectral', 12);
cl = cl([1, 3, 4, 10, 12],:);

figure ; 

chan = 1;


subplot(2,1,2)
     hold on 
    %figure('position',[0 300 1000 300])
    for regressor = 1:length(options.TrialGLM.(glmFlag).regressors)
        regLabel = [options.TrialGLM.(glmFlag).regressors(regressor).regressorLabel,'_b'];
 %subplot(2,2,regressor) 

cfg             = [];
cfg.channel     = options.channel.chanLabels;

smoothedData = perc_dyn_smooth(tstat_across_subjects{regressor},'meanGLM');
%figure('position',[0 300 1000 300])
%ft_singleplotER(cfg,timelockAverageAllSubjects.(regLabel).avg)
plot(timelockAverage.(sensors){1}.time, smoothedData,'LineWidth',2,'Color',cl(regressor,:))


 
    end 
%     hold on 
% plot(ones(6,1)*4.5, [6:0.5:8.5],'k--','LineWidth',2)
%  hold off
 
%xlim([3 5])
% ylim([-1.5 1.5])
ylabel('tstat')
%title(options.channel.chanLabels(chan))
title('average across sensors')
xlabel('sec')
ylim([-8 8])
%xticks(3.5:0.25:5)
legend(options.TrialGLM.(glmFlag).regressors(:).regressorLabel,'constant wrong trials','prior wrong trials')
%legend('correct', 'incorrect')
tidyfig;
hold off 

subplot(2,1,1)
     hold on 
    %figure('position',[0 300 1000 300])
    for regressor = 1:length(options.TrialGLM.(glmFlag).regressors)
        regLabel = [options.TrialGLM.(glmFlag).regressors(regressor).regressorLabel,'_b'];
 %subplot(2,2,regressor) 

cfg             = [];
cfg.channel     = options.channel.chanLabels;

smoothedData = perc_dyn_smooth(avg_across_subjects{regressor},'meanGLM');
%figure('position',[0 300 1000 300])
%ft_singleplotER(cfg,timelockAverageAllSubjects.(regLabel).avg)
plot(timelockAverage.(sensors){1}.time, smoothedData,'LineWidth',2,'Color',cl(regressor,:))


 
    end 
%     hold on 
% plot(ones(6,1)*4.5, [6:0.5:8.5],'k--','LineWidth',2)
%  hold off
 
%xlim([3 5])
% ylim([-1.5 1.5])
ylabel('betas')
%title(options.channel.chanLabels(chan))
title('average across sensors')
xlabel('sec')
% ylim([-1.5 1.5])
%xticks(3.5:0.25:5)
legend(options.TrialGLM.(glmFlag).regressors(:).regressorLabel,'constant wrong trials','prior wrong trials')
%legend('correct', 'incorrect')
tidyfig;
hold off 

%% 
for chan = 1%:length(options.channel.chanLabels)
   figure; 
     hold on 
    %figure('position',[0 300 1000 300])
    for regressor = 1:length(options.TrialGLM.(glmFlag).regressors)
        regLabel = [options.TrialGLM.(glmFlag).regressors(regressor).regressorLabel,'_b'];
 %subplot(2,2,regressor) 

cfg             = [];
cfg.channel     = options.channel.chanLabels;

smoothedData = perc_dyn_smooth(glmResults.(sensors).(regLabel),sensors);
%figure('position',[0 300 1000 300])
%ft_singleplotER(cfg,timelockAverageAllSubjects.(regLabel).avg)
plot(glmResults.(sensors).(regLabel).time, smoothedData.avg(chan,:),'LineWidth',2)


 
    end 
%     hold on 
% plot(ones(6,1)*4.5, [6:0.5:8.5],'k--','LineWidth',2)
%  hold off
 
%xlim([3 5])
% ylim([-1.5 1.5])
ylabel('betas')
%title(options.channel.chanLabels(chan))
title('average across sensors')
xlabel('sec')
%xticks(3.5:0.25:5)
legend(options.TrialGLM.(glmFlag).regressors(:).regressorLabel)
tidyfig; 
end 


%%
for chan = 1%:length(options.channel.chanLabels)
   figure; 
     hold on 
    %figure('position',[0 300 1000 300])
    for regressor = 1:length(options.TrialGLM.(glmFlag).regressors)
        regLabel = [options.TrialGLM.(glmFlag).regressors(regressor).regressorLabel,'_b'];
 %subplot(2,2,regressor) 

cfg             = [];
cfg.channel     = options.channel.chanLabels;

smoothedData = perc_dyn_smooth(glmResults.(sensors).(regLabel),sensors);
%figure('position',[0 300 1000 300])
%ft_singleplotER(cfg,timelockAverageAllSubjects.(regLabel).avg)
plot(glmResults.(sensors).(regLabel).time, smoothedData.avg(chan,:),'LineWidth',2)


 
    end 
%     hold on 
% plot(ones(6,1)*4.5, [6:0.5:8.5],'k--','LineWidth',2)
%  hold off
 
%xlim([3 5])
% ylim([-1.5 1.5])
ylabel('betas')
%title(options.channel.chanLabels(chan))
title('average across sensors')
xlabel('sec')
%xticks(3.5:0.25:5)
legend(options.TrialGLM.(glmFlag).regressors(:).regressorLabel)
tidyfig; 
end 
%%  calculate difference curves for averaged across all electrodes 
%% plot betas and tstats for this 







%% plot difference curves left vs right for outermost sensors only 
%contrast analysis

clear all
close all
cl = cbrewer('div', 'PRGn', 8);
cl = cl(1:2:end,:);
options = generate_perc_dyn_options2();
glmFlag = 'correctIncorrectTR';
load(options.TrialGLM.(glmFlag).dataSubjects)
% contrast correct - wrong 1 1 -1 -1

contrast = [-1  -1  1 1]';
field = 'All';
%%
% for betas of each subject

for subject = 1:length(timelockAverage.(field))
    
    % smooth data
    
    for label = 1:length(options.TrialGLM.(glmFlag).regressors)
        regLabel = [options.TrialGLM.(glmFlag).regressors(label).regressorLabel,'_b'];
        timelockAverage.(field){subject}.(regLabel) = perc_dyn_smooth(timelockAverage.(field){subject}.(regLabel), 'meanGLM');
    end
    
    Cope = [timelockAverage.(field){subject}.left_correct_trials_b; timelockAverage.(field){subject}.right_correct_trials_b;  timelockAverage.(field){subject}.right_wrong_trials_b;  timelockAverage.(field){subject}.left_wrong_trials_b ]';
    
    diffSubject(subject,:) = Cope * contrast;
    
    
    
    
end
% ttest for each point
%%
for time = 1:size(diffSubject,2)
    
    [~,~,~,stats] = ttest(diffSubject(:,time));
    timetTstat(time) = stats.tstat;
    
    
end



meanDiff = nanmean(diffSubject,1);



figure
subplot(2,1,1)
plot(timelockAverage.(field){1}.time, meanDiff)
ylabel('beta')
xlim([3 5])
tidyfig;

subplot(2,1,2)
plot(timelockAverage.(field){1}.time, timetTstat)
yticks(-6 : 6)
ylabel('tstat')
xlabel('time sec')
xlim([3 5])
tidyfig










%% plot comparison ERP GLM 

load(options.ERPAnalysis.correctIncorrectTR.data)
load(options.TrialGLM.(glmFlag).data)
chan = 1; 
hold on 
for regressor = 1:length(options.TrialGLM.(glmFlag).regressors)
        regLabel = [options.TrialGLM.(glmFlag).regressors(regressor).regressorLabel,'_b'];
 %subplot(2,2,regressor) 

cfg             = [];
cfg.channel     = options.channel.chanLabels;
%figure('position',[0 300 1000 300])
%ft_singleplotER(cfg,timelockAverageAllSubjects.(regLabel).avg)
plot(glmResults.(sensors).(regLabel).time, glmResults.(sensors).(regLabel).avg(chan,:),'LineWidth',2)


    end 

plot(ERPResults.All1.time, ERPResults.All1.avg,'LineWidth',2); 
plot(ERPResults.All1.time, ERPResults.All2.avg,'LineWidth',2); 
plot(ERPResults.All1.time, ERPResults.All3.avg,'LineWidth',2); 
plot(ERPResults.All1.time, ERPResults.All4.avg,'LineWidth',2); 
plot(ones(6,1)*4.5, [6:0.5:8.5],'k--','LineWidth',2)
xlim([0 6])

legend(options.TrialGLM.(glmFlag).regressors(:).regressorLabel, 'ERP left correct', 'ERP right correct', 'ERP right wrong', 'ERP Left wrong')
tidyfig

%% plot subjects 

load(options.TrialGlM.(glmFlag).dataSubjects)
figure
for subjects = 1:options.badTrialsRemoved.numSubjects
      
for chan = 3%:length(options.chanLabels)
  
    subplot(5,5,subjects)
    %figure('position',[0 300 1000 300])
    for regressor = 1%length(options.TrialGLM.(glmFlag).regressors)
        regLabel = [options.TrialGLM.(glmFlag).regressors(regressor).regressorLabel,'_b'];
 %subplot(2,2,regressor) 

cfg             = [];
cfg.channel     = options.channel.chanLabels{chan};
%figure('position',[0 300 1000 300])
%ft_singleplotER(cfg,timelockAverageAllSubjects.(regLabel).avg)
plot(timelockAverage(subjects).(regLabel).time, timelockAverage(subjects).(regLabel).avg(chan,:))
%plot(ones(4,1)*4.5, [-1.5:2],'k--','LineWidth',2)
hold on 
plot(ones(5,1)*4.5, [-0.2:0.1:0.2],'k--','LineWidth',2)
 hold off
xlim([4 6])
% ylim([-1.5 1.5])
ylabel(regLabel)
title(options.chanLabels(chan))
%xticks(3.5:0.25:5)
%legend({'left', 'right'})
tidyfig; 

    end 
end 
end