%% ERP ANALYSIS


clear all
close all
options = generate_perc_dyn_options2();
glmFlag = 'correctIncorrectTR';
for subject = 1: options.badTrialsRemoved.numSubjects
    disp(subject)
    details = generate_perdyn_subject_details2(subject,options);
    
    
    dataLoad =  load(details.Trials.WrongTrials.datafile);
    dataWrong = dataLoad.wrongTrials;
    
    
    dataLoad =  load(details.Trials.CorrectTrials.datafile);
    dataCorrect = dataLoad.correctTrials;
    
    
    cfg = [];
    cfg.keeptrials = 'no';
    [timelockWrong{subject}] = ft_timelockanalysis(cfg, dataWrong.ftData);
    
    
    cfg = [];
    cfg.keeptrials = 'no';
    [timelockCorrect{subject}] = ft_timelockanalysis(cfg, dataCorrect.ftData);
    
    
    
end

% average across subjects
cfg = [];
cfg.method = 'across';
[grandavgWrong] = ft_timelockgrandaverage(cfg, timelockWrong{:});


cfg = [];
cfg.method = 'across';
[grandavgCorrect] = ft_timelockgrandaverage(cfg, timelockCorrect{:});



% plot
 figure
 hold on
 plot(grandavgWrong.time, grandavgWrong.avg);
 plot(grandavgCorrect.time, grandavgCorrect.avg);
ylabel('average')
 legend({'wrong','correct'})
%
%%
% calculate t-tstats at each time point


% trasnform timelock data in one huge matrix

for subject = 1: options.badTrialsRemoved.numSubjects
    
    CorrectSubjects(subject,:) = timelockCorrect{subject}.avg;
    
    WrongSubjects(subject,:) = timelockWrong{subject}.avg;
    
    
end

for t = 1:1600
    
    
    [~,PcorrectTr(t),~,statsCorrectTr] = ttest(CorrectSubjects(:,t));
    tstatCorrectTr(t) = statsCorrectTr.tstat;
    
    [~,PwrongTr(t),~,statsWrongTr] = ttest(WrongSubjects(:,t));
    tstatWrongTr(t) = statsWrongTr.tstat;
    
end



% plot
figure
hold on
plot(grandavgWrong.time, tstatWrongTr)
plot(grandavgCorrect.time, tstatCorrectTr)
ylabel('tstat')
legend({'wrong','correct'})


%% contrast correct - incorrect 

    difference =  WrongSubjects- CorrectSubjects;

meanDifference = nanmean(difference,1);

idx1 = find(grandavgCorrect.time == 3); 
idx2 = find(grandavgCorrect.time == 5); 


plot(grandavgCorrect.time(idx1:idx2), meanDifference(idx1:idx2))


%% check grand mean 
clear all
%close all
options = generate_perc_dyn_options2();
glmFlag = 'correctIncorrectTR';
for subject = 1: options.badTrialsRemoved.numSubjects
    disp(subject)
    details = generate_perdyn_subject_details2(subject,options);


dataLoad =  load(details.Trials.AllTrials.datafile);
                data = dataLoad.AllTrials;



    cfg = [];
    cfg.keeptrials = 'no';
    [timelock{subject}] = ft_timelockanalysis(cfg, data.ftData);
    
    
    
    
%         cfg = [];
%     cfg.baseline = [-2 -1];
%    
%        
%  [timelock{subject}] = ft_timelockbaseline(cfg, timelock1);
    
end 
    % average across subjects
cfg = [];
cfg.method = 'across';
[grandavg] = ft_timelockgrandaverage(cfg, timelock{:});




% plot
figure

plot(grandavg.time,grandavg.avg)

ylabel('�V')
xlabel('time')
tidyfig;
