function cfg = perc_dyn_create_design_matrix2(data,options,glmFlag,MEG,details)

if MEG
  
    numRegressors = length(options.TrialGLM.(glmFlag).regressors);
    
    
    for run = 1:details.badTrialsRemoved.subject.runs
        
        numTrials = size(data.ftData.trial,1);
     
        designMatrix = zeros(numTrials,numRegressors);
        label = cell(numRegressors,1);
        for regressor = 1:numRegressors
            
            regressorName = options.TrialGLM.(glmFlag).regressors(regressor).regressorName;
            
            switch regressorName
                
                
                case 'reg_rel_signed'
                    
                    designMatrix(:,regressor) = data.reg_rel-0.5;
                    
                case 'reg_saccdir'
                    
                    designMatrix(:,regressor) = data.reg_saccdir;
                    designMatrix(designMatrix(:,regressor)==1,regressor) = -1;
                    designMatrix(designMatrix(:,regressor)==2,regressor) = 1;
                    
                case 'reg_fb_signed'
                    
                    designMatrix(:,regressor) = data.reg_feedback;
                    designMatrix(designMatrix(:,regressor)==0,regressor) = -1;
                    
                case 'reg_interaction_rel_abs'
                    
                    designMatrix(:,regressor) = (data.reg_rel-0.5) .* data.reg_ab;
%                     meanReg = mean(designMatrix(:,regressor));
%                     designMatrix(:,regressor) = designMatrix(:,regressor) - meanReg; 
                    
                case 'reg_absoluted_scaled_prior'
                    
                    designMatrix(:,regressor) = abs(data.reg_pri-0.5);
%                     meanReg = mean(designMatrix(:,regressor));
%                     designMatrix(:,regressor) = designMatrix(:,regressor) - meanReg; 
                    %case 'reg_KL_divergence_prior'
                    
                case 'reg_const'
                    
                    designMatrix(:,regressor) = 1;
                    
                case 'reg_left_corr'
                    
                    designMatrix(data.reg_correctAnswers==1 & data.reg_saccdir==1,regressor) = 1;
                    
                case 'reg_right_corr'
                    
                    designMatrix(data.reg_correctAnswers==1 & data.reg_saccdir==2,regressor) = 1;
                    
                case 'reg_left_wrong'
                    
                    designMatrix(data.reg_correctAnswers==0 & data.reg_saccdir==1,regressor) = 1;
                    
                case 'reg_right_wrong'
                    designMatrix(data.reg_correctAnswers==0 & data.reg_saccdir==2,regressor) = 1;
                    
                case 'reg_corr_wrong'
                    designMatrix(data.reg_correctAnswers==0, regressor) = -1;
                    designMatrix(data.reg_correctAnswers==1, regressor) =  1;
                    
                case 'reg_right_left'
                    
                    designMatrix(data.reg_saccdir==2,regressor) = 1;
                    designMatrix(data.reg_saccdir==1,regressor) = -1;
                    
                case 'reg_correct_incorrect_contrast'
                    
                    designMatrix(data.reg_correctAnswers==1,regressor) = 0.1;
                    designMatrix(data.reg_correctAnswers==0,regressor) = -0.1;
                    
                
                    
                case 'reg_interaction_reg_abs_folded'
                    
             
                    designMatrix(:,regressor) = data.reg_relf .* data.reg_ab;
%                     meanReg = mean(designMatrix(:,regressor));
%                     designMatrix(:,regressor) = designMatrix(:,regressor) - meanReg;                  
                    
                case 'reg_signed_prior'
                    
                    designMatrix(:,regressor) = data.reg_pri-0.5;
                    
                case 'reg_wtPos'
                    
                    designMatrix(:,regressor) = abs(data.reg_wtPos-0.5);
                    
                case 'reg_reward_probability'
                    
                    designMatrix(:,regressor) = data.reg_wtPos;
                    
                    designMatrix(data.reg_saccdir==1,regressor) = 1 - data.reg_wtPos(data.reg_saccdir==1);
                    
                case 'reg_feedback_surprise'
                    
                    designMatrix(:,regressor) = data.reg_pri-0.5;
                    
                    signedFeedback = data.reg_feedback;
                    signedFeedback(data.reg_feedback==0)= -1;
                    
                    designMatrix(:,regressor) = designMatrix(:,regressor) .* signedFeedback;
                    
                case 'reg_response_surprise'
                    
                    designMatrix(:,regressor) = data.reg_pri-0.5;
                    
                    signedResponse = data.reg_saccdir;
                    signedResponse(data.reg_saccdir==1)= -1;
                    
                    designMatrix(:,regressor) = designMatrix(:,regressor) .* signedResponse;
                    
                    
                case 'reg_orthog_correct_incorr_rew_prob'
                    
                    rew_prob = data.reg_wtPos;
                    
                    rew_prob(data.reg_saccdir==1) = 1 - data.reg_wtPos(data.reg_saccdir==1);
                    
                    
                    corr_incorr_cont = zeros(numTrials,1);
                    corr_incorr_cont(data.reg_correctAnswers==1) = 1;
                    corr_incorr_cont(data.reg_correctAnswers==0) = -1;
                    
                    
                    %
                    %             betas = glmfit(rew_prob, corr_incorr_cont, 'normal');
                    %
                    %
                    %             dm = [ones(length(rew_prob),1),rew_prob];
                    %             fit = dm * betas;
                    %
                    %             residuals = corr_incorr_cont-fit;
                    %
                    %             designMatrix(:,regressor) = residuals;
                    %
                    
                    
                    betas = glmfit(rew_prob, corr_incorr_cont, 'normal','constant','off');
                    
                    
                    dm = rew_prob;
                    fit = dm * betas;
                    
                    residuals = corr_incorr_cont - fit;
                    
                    designMatrix(:,regressor) = residuals;
                    
                case 'reg_prior_signed_for_left'
                    
                    prior = data.reg_pri; 
                    response = data.reg_saccdir; 
                    
                    
                    designMatrix(:,regressor) = prior; 
                    designMatrix(response == 1,regressor) = 1-prior(response == 1); 

                    
%                     meanReg = mean(designMatrix(:,regressor));
%                     designMatrix(:,regressor) = designMatrix(:,regressor) - meanReg;
                    
                case 'reg_interaction_abXrel_signed_for_left'
                    
                    Interaction = (data.reg_rel-0.5) .* data.reg_ab;
                    response = data.reg_saccdir;
                    Interaction(response == 1) = -1.* Interaction(response==1);
                    
                    
                    designMatrix(:,regressor) = Interaction; 
                    %meanReg = mean(designMatrix(:,regressor));
                    %designMatrix(:,regressor) = designMatrix(:,regressor) - meanReg;
                    
                    
                case 'reg_abXrelfolded_orthog' 
                    
                    abXrel = data.reg_relf .* data.reg_ab;
                    betas_abxrel = glmfit([data.reg_ab data.reg_relf],abXrel);
                  
                    dm = [ones(length(data.reg_ab),1) data.reg_ab data.reg_relf];%first column is for grand mean
                    
                    designMatrix(:,regressor) = abXrel - (dm*betas_abxrel);
                  
                    
                case 'reg_switchprior'
                     designMatrix(:,regressor) = data.reg_switchprior;
                     
                     
                case 'reg_prior_signed_for_choice'
                  
                    prior = data.reg_pri-0.5; % negative = left
                    choice = data.reg_saccdir;
                    
                    choice(choice == 1) = -1; % left 
                    choice(choice == 2) = 1;  % right 
                    
                    signPrior = sign(prior); 
                    signChoice = sign(choice); 
                    
                    prior(signPrior == signChoice) = abs(prior(signPrior == signChoice));

                    prior(signPrior ~= signChoice) =  abs(prior(signPrior ~= signChoice)).*-1; 
                    
                     designMatrix(:,regressor) = prior;
                     
                  
                case 'reg_abXrel_signed_for_choice'
                    
                    Interaction = data.reg_ab .* (data.reg_rel-0.5); % negative = left
                    
                    choice = data.reg_saccdir;
                    
                    choice(choice == 1) = -1; % left 
                    choice(choice == 2) = 1;  % right 
                    
                    signInteraction = sign(Interaction);                
                    signChoice = sign(choice); 
                    
                    Interaction(signInteraction == signChoice) = abs(Interaction(signInteraction==signChoice));
                    Interaction(signInteraction ~= signChoice) = abs(Interaction(signInteraction~=signChoice)) .*-1;

                    designMatrix(:,regressor) = Interaction;
                    
                   
                otherwise
                  
                    
                    designMatrix(:,regressor) = data.(regressorName);
%                     meanReg = mean(designMatrix(:,regressor));
%                     designMatrix(:,regressor) = designMatrix(:,regressor) - meanReg;                    
            end
            
            label{regressor} = options.TrialGLM.(glmFlag).regressors(regressor).regressorLabel;
            
        end
        
        
    end
    
    cfg = [];

    cfg.design = designMatrix;
    cfg.label       = label;
    cfg.parameter   = 'trial';
    
else
    
    numRegressors = length(options.behaviouralGLM.(glmFlag).regressors);
    
    
    lags = options.behaviouralGLM.lags;
    designMatrixAll = [];
    
    for run = 1:details.badTrialsRemoved.subject.runs
        regressorIdx = 1;
        numTrials = length(data{run}.ftData.trial);
        designMatrix = zeros(numTrials,20);
        for regressor = 1 : numRegressors
            
            
            regressorName = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorName;
            
            
            switch regressorName
                
                
                case 'reg_lagged_feedback'
                    
                    feedbackSigned = data{run}.reg_feedback;
                    feedbackSigned(feedbackSigned==0) = -1;
                    
                    for lagIdx = 1:lags
                        
                        designMatrix(:,regressorIdx) = [zeros(lagIdx,1); feedbackSigned(1:end-lagIdx)];
                        label{regressorIdx} = ['fb_t',num2str(lagIdx)];
                        regressorIdx = regressorIdx + 1;
                    end
                    
                    
                case 'reg_lagged_response'
                    
                    responseSigned = data{run}.reg_saccdir;
                    responseSigned(responseSigned==1) = -1;
                    responseSigned(responseSigned==2) = 1;
                    
                    
                    
                    for lagIdx = 1:lags
                        designMatrix(:,regressorIdx) = [zeros(lagIdx,1); responseSigned(1:end-lagIdx)];
                        label{regressorIdx} = ['RP_t',num2str(lagIdx)];
                        regressorIdx = regressorIdx + 1;
                    end
                    
                case 'reg_lagged_response_x_fb'
                    
                    responseSigned = data{run}.reg_saccdir;
                    responseSigned(responseSigned==1) = -1;
                    responseSigned(responseSigned==2) = 1;
                    
                    feedbackSigned = data{run}.reg_feedback;
                    feedbackSigned(feedbackSigned==0) = -1;
                    
                    
                    feedbackXresponse = feedbackSigned .* responseSigned;
                    
                    
                    for lagIdx = 1:lags
                        designMatrix(:,regressorIdx) = [zeros(lagIdx,1); feedbackXresponse(1:end-lagIdx)];
                        label{regressorIdx} = ['RpXFb_t',num2str(lagIdx)];
                        regressorIdx = regressorIdx + 1;
                    end
                    
                    
                case 'reg_signed_prior'
                    
                    designMatrix(:,regressorIdx) = data{run}.reg_pri-0.5;
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
                    
                    
                case 'reg_prior_x_rel'
                    
                    prior = data{run}.reg_pri;
                    rel =  data{run}.reg_rel-0.5;
                    designMatrix((prior > 0.5 & rel > 0),regressorIdx) = 1;
                    designMatrix((prior < 0.5 & rel < 0),regressorIdx) = -1;
                    
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
                    
                case 'reg_prior_x_ab'
                    
                    prior = data{run}.reg_pri-0.5;
                    ab =  data{run}.reg_ab;
                    
                    designMatrix(:,regressorIdx) = prior .* ab;
                    
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
                    
                case 'reg_prior_x_abs_x_rel'
                    
                    prior = data{run}.reg_pri-0.5;
                    ab = data{run}.reg_ab;
                    rel = data{run}.reg_rel-0.5;
                    abXrel = ab .* rel;
                    
                    designMatrix((prior > 0.5 & abXrel > 0),regressorIdx) = 1;
                    designMatrix((prior < 0.5 & abXrel < 0),regressorIdx) = -1;
                    
                    %designMatrix(:,regressorIdx) = prior .* abXrel;

                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
                    
                case 'reg_interaction_rel_abs'
                    
                    designMatrix(:,regressorIdx) = (data{run}.reg_rel-0.5) .* data{run}.reg_ab;
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
                    
                case 'reg_rel_signed'
                    
                    designMatrix(:,regressorIdx) = data{run}.reg_rel-0.5;
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
                    
                otherwise
                    designMatrix(:,regressorIdx) = data{run}.(regressorName);
                    label{regressorIdx} = options.behaviouralGLM.(glmFlag).regressors(regressor).regressorLabel;
                    regressorIdx = regressorIdx + 1;
            end
            
        end
        
        designMatrixAll = [designMatrixAll; designMatrix];
        
        
    end
    
    cfg.design = designMatrixAll(:,1:regressorIdx-1);
    cfg.label = {'constant',label{:}};
    
end



end