% Returns beta weights and t-statistics for each regressor
% that are identical to those returned by 'glmfit', but a lot
% faster, so it can be used in a big loop.
% TO-DO: contrasts!

function [b, t, err] = fast_glm(x, y)

% do the stuff
n       = size(x, 1);
k       = size(x, 2) - 1;

% b=x\y
b       = inv(x' * x) * x' * y;

% model fit
yhat    = x * b;

% error
err     = y - yhat;

% mean squared error (error has approx. zero mean)
sserr   = sum(err .^ 2);

% equivalent
% sserr = err' * err;

dferr   = n - (k + 1);
mserr   = sserr / dferr;

% standard error
C       = mserr * inv(x' * x);
se      = diag(C) .^ 0.5;

% t-statistic
t       = b ./ se;