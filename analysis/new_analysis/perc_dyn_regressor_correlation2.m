clear all

options = generate_perc_dyn_options2();
glmFlag = 'regressor_correlation';
[cl] = cbrewer('div', 'RdGy', 400);
cl = flip(cl);
for subject = 1: options.badTrialsRemoved.numSubjects
    disp(subject)
    details = generate_perdyn_subject_details2(subject,options);
   
    
    load(details.Trials.AllTrials.datafile)
    
    
    cfg = perc_dyn_create_design_matrix2(AllTrials,options,glmFlag,1,details);
  

correlationMatrix(:,:,subject) = corr(cfg.design);



end

correlationMatrixAvg = round(mean(correlationMatrix,3),2);



figure
h = heatmap(correlationMatrixAvg);
colormap(cl)
h.ColorLimits =[-1 1];
h.XDisplayLabels = {cfg.label{1:end}};
h.YDisplayLabels = {cfg.label{1:end}};

% h = colorbar;
% h.Limits = [-1 1];