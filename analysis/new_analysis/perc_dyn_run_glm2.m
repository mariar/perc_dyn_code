clear all

flags = {'correctIncorrectTR'};

trialFlag = 'all';


for flag = 1:length(flags)
    
    
    
    options = generate_perc_dyn_options2();
    glmFlag = flags{flag};
    
    
    
    clear glm timelockAverage glmResults
    
    for subject = 1: options.badTrialsRemoved.numSubjects
        disp(subject)
        details = generate_perdyn_subject_details2(subject,options);
        
        switch trialFlag
            case 'all'
                dataLoad =  load(details.Trials.AllTrials.datafile);
                data = dataLoad.AllTrials;
                
            case 'wrong'
                dataLoad =  load(details.Trials.WrongTrials.datafile);
                data = dataLoad.wrongTrials;
                
            case 'correct'
                dataLoad =  load(details.Trials.CorrectTrials.datafile);
                data = dataLoad.correctTrials;
                
        end
        
        
        % create design matrix
        try
            cfg = perc_dyn_create_design_matrix2(data,options,glmFlag,1,details);
        catch
            keyboard;
            
        end


         [timelockAverage.All{subject}] = fast_glm_wrapper(cfg, data.ftData);
        
        
    end
    
    
    switch trialFlag
        case 'all'
            saveName = options.TrialGLM.(glmFlag).dataSubjects;
            
        case 'wrong'
            saveName = options.TrialGLM.Wrong.(glmFlag).dataSubjects;
            
        case 'correct'
            saveName = options.TrialGLM.Correct.(glmFlag).dataSubjects;
            
    end
    save(saveName,'timelockAverage');
end