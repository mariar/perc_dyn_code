function [wrongTrials, correctTrials] = perc_dyn_select_data2(dataAppended)



% select wrong trials

idxWrong = dataAppended.reg_correctAnswers == 0;
idxCorrect = dataAppended.reg_correctAnswers == 1;

wrongTrials.reg_ab      = dataAppended.reg_ab(idxWrong);
wrongTrials.reg_rt      =  dataAppended.reg_rt(idxWrong);
wrongTrials.reg_saccdir = dataAppended.reg_saccdir(idxWrong);
wrongTrials.reg_fbPos   = dataAppended.reg_fbPos(idxWrong);
wrongTrials.reg_lh      = dataAppended.reg_lh(idxWrong);
wrongTrials.reg_MLE_prior = dataAppended.reg_MLE_prior(idxWrong);
wrongTrials.reg_MLE_withinTrial_posterior = dataAppended.reg_MLE_withinTrial_posterior(idxWrong);
wrongTrials.reg_pri     = dataAppended.reg_pri(idxWrong);
wrongTrials.reg_pri2    = dataAppended.reg_pri2(idxWrong);
wrongTrials.reg_pris    = dataAppended.reg_pris(idxWrong);
wrongTrials.reg_rel     = dataAppended.reg_rel(idxWrong);
wrongTrials.reg_relf    = dataAppended.reg_relf(idxWrong);
wrongTrials.reg_surp    = dataAppended.reg_surp(idxWrong);
wrongTrials.reg_trn     = dataAppended.reg_trn(idxWrong);
wrongTrials.reg_wtPos   = dataAppended.reg_wtPos(idxWrong);
wrongTrials.reg_correctAnswers = dataAppended.reg_correctAnswers(idxWrong);
wrongTrials.reg_feedback = dataAppended.reg_feedback(idxWrong);
wrongTrials.reg_switchprior = dataAppended.reg_switchprior(idxWrong);
wrongTrials.reg_signed_prior_diff_to_0 = dataAppended.reg_signed_prior_diff_to_0(idxWrong);
wrongTrials.reg_absoluted_prior_diff_to_0 = dataAppended.reg_absoluted_prior_diff_to_0(idxWrong);





cfg = [];
cfg.trials = idxWrong;
wrongTrials.ftData = ft_selectdata(cfg, dataAppended.ftData);




correctTrials.reg_ab      = dataAppended.reg_ab(idxCorrect);
correctTrials.reg_rt      =  dataAppended.reg_rt(idxCorrect);
correctTrials.reg_saccdir = dataAppended.reg_saccdir(idxCorrect);
correctTrials.reg_fbPos   = dataAppended.reg_fbPos(idxCorrect);
correctTrials.reg_lh      = dataAppended.reg_lh(idxCorrect);
correctTrials.reg_MLE_prior = dataAppended.reg_MLE_prior(idxCorrect);
correctTrials.reg_MLE_withinTrial_posterior = dataAppended.reg_MLE_withinTrial_posterior(idxCorrect);
correctTrials.reg_pri     = dataAppended.reg_pri(idxCorrect);
correctTrials.reg_pri2    = dataAppended.reg_pri2(idxCorrect);
correctTrials.reg_pris    = dataAppended.reg_pris(idxCorrect);
correctTrials.reg_rel     = dataAppended.reg_rel(idxCorrect);
correctTrials.reg_relf    = dataAppended.reg_relf(idxCorrect);
correctTrials.reg_surp    = dataAppended.reg_surp(idxCorrect);
correctTrials.reg_trn     = dataAppended.reg_trn(idxCorrect);
correctTrials.reg_wtPos   = dataAppended.reg_wtPos(idxCorrect);
correctTrials.reg_correctAnswers = dataAppended.reg_correctAnswers(idxCorrect);
correctTrials.reg_feedback = dataAppended.reg_feedback(idxCorrect);
correctTrials.reg_switchprior = dataAppended.reg_switchprior(idxCorrect);
correctTrials.reg_signed_prior_diff_to_0 = dataAppended.reg_signed_prior_diff_to_0(idxCorrect);
correctTrials.reg_absoluted_prior_diff_to_0 = dataAppended.reg_absoluted_prior_diff_to_0(idxCorrect);


cfg = [];
cfg.trials = idxCorrect;
correctTrials.ftData = ft_selectdata(cfg, dataAppended.ftData);
end