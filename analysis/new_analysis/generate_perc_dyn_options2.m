function options = generate_perc_dyn_options2()
% subject 158 had to be removed because behavioural data could not be
% matched with meg data to find out correct and incorrect trials 

addpath('/Users/maria/Documents/MATLAB/fieldtrip')
ft_defaults
 addpath(genpath('/Users/maria/Documents/MATLAB/cbrewer'));

options.paths.VolumePath = '/Volumes/perc_dyn_MEG';
options.paths.PreprocessedDataPath = fullfile(options.paths.VolumePath,'preprocessedData');
options.paths.ChannelDataPath = fullfile(options.paths.VolumePath,'channelData');
options.paths.badTrialsRemovedPath = fullfile(options.paths.VolumePath,'badTrialsRemoved');
options.paths.behaviouralData = fullfile(options.paths.VolumePath,'behaviour','B.mat');
options.paths.analysisData = fullfile(options.paths.VolumePath,'analysis');
options.paths.dataForAnalysis = fullfile(options.paths.VolumePath,'data_for_analysis');
options.paths.analysisData2 = fullfile(options.paths.VolumePath,'analysis', 'analysis2');

fileID = fopen(fullfile(options.paths.PreprocessedDataPath,'subjectFolderList.txt'),'r');
fileList = textscan(fileID,'%s');
options.raw.subjectFolderList = fileList{1};
options.raw.numSubjects = length(options.raw.subjectFolderList);


fileID = fopen(fullfile(options.paths.badTrialsRemovedPath,'subjectFolderList.txt'),'r');
fileList = textscan(fileID,'%s');
options.badTrialsRemoved.subjectFolderList = fileList{1};
options.badTrialsRemoved.numSubjects = length(options.badTrialsRemoved.subjectFolderList);



options.rtCutoff = 50;

options.regressors(1).regressorLabel    = 'absoluted_coherence';
options.regressors(1).regressorName     = 'reg_ab';
options.regressors(2).regressorLabel    = 'relative_coherence_signed';
options.regressors(2).regressorName     = 'reg_rel_signed';
options.regressors(3).regressorLabel    = 'relative_coherence_folded';
options.regressors(3).regressorName     = 'reg_relf';
options.regressors(4).regressorLabel    = 'saccade_direction';
options.regressors(4).regressorName     = 'reg_saccdir';
options.regressors(5).regressorLabel    = 'constant';
options.regressors(5).regressorName     = 'reg_const';
options.regressors(6).regressorLabel    = 'prior';
options.regressors(6).regressorName     = 'reg_pri';
options.regressors(7).regressorLabel    = 'prior_strength';
options.regressors(7).regressorName     = 'reg_pris';
options.regressors(8).regressorLabel    = 'left_correct_trials';
options.regressors(8).regressorName     = 'reg_left_corr';
options.regressors(9).regressorLabel    = 'right_correct_trials';
options.regressors(9).regressorName     = 'reg_right_corr';
options.regressors(10).regressorLabel   = 'right_wrong_trials';
options.regressors(10).regressorName    = 'reg_right_wrong';
options.regressors(11).regressorLabel   = 'left_wrong_trials';
options.regressors(11).regressorName    = 'reg_left_wrong';
options.regressors(12).regressorLabel   = 'difference_in_prior'; 
options.regressors(12).regressorName    = 'reg_surp';
options.regressors(13).regressorLabel   = 'within_trial_posterior'; 
options.regressors(13).regressorName    = 'reg_wtPos';
options.regressors(14).regressorLabel   = 'KL_divergence_prior'; 
options.regressors(14).regressorName    = 'reg_KL_divergence_prior'; 
options.regressors(15).regressorLabel   = 'feedback_signed'; 
options.regressors(15).regressorName    = 'reg_fb_signed'; 
options.regressors(16).regressorLabel   = 'interaction_rel_abs'; 
options.regressors(16).regressorName    = 'reg_interaction_rel_abs'; 
options.regressors(17).regressorLabel   = 'absoluted_scaled_prior';
options.regressors(17).regressorName    = 'reg_absoluted_scaled_prior';
options.regressors(18).regressorLabel   = 'interaction_absolute_relative_coherence_folded'; 
options.regressors(18).regressorName    = 'reg_interaction_reg_abs_folded';
options.regressors(19).regressorLabel   = 'signed_prior'; 
options.regressors(19).regressorName    = 'reg_signed_prior'; 
options.regressors(20).regressorLabel   = 'correct_incorrect_contrast';
options.regressors(20).regressorName    = 'reg_correct_incorrect_contrast';
options.regressors(21).regressorLabel   = 'reward_probability'; 
options.regressors(21).regressorName    = 'reg_reward_probability'; 
options.regressors(22).regressorLabel   = 'feedback_surprise';
options.regressors(22).regressorName    = 'reg_feedback_surprise';
options.regressors(23).regressorLabel   = 'response_surprise';
options.regressors(23).regressorName    = 'reg_response_surprise';
options.regressors(24).regressorLabel   = 'orthog_correct_incorr_rew_prob';
options.regressors(24).regressorName   = 'reg_orthog_correct_incorr_rew_prob';
options.regressors(25).regressorLabel   = 'lagged_feedback';
options.regressors(25).regressorName   = 'reg_lagged_feedback';
options.regressors(26).regressorLabel   = 'lagged_response'; 
options.regressors(26).regressorName    = 'reg_lagged_response';
options.regressors(27).regressorLabel = 'interaction_lagged_response_feedback';
options.regressors(27).regressorName = 'reg_lagged_response_x_fb';
options.regressors(28).regressorLabel = 'interaction_prior_relative_coherence'; 
options.regressors(28).regressorName = 'reg_prior_x_rel';
options.regressors(29).regressorLabel = 'interaction_prior_absolute_cohrence'; 
options.regressors(29).regressorName = 'reg_prior_x_ab';
options.regressors(30).regressorLabel = 'interaction_prior_x_ab_x_rel';
options.regressors(30).regressorName = 'reg_prior_x_abs_x_rel';
options.regressors(31).regressorLabel = 'prior_signed_for_left';
options.regressors(31).regressorName = 'reg_prior_signed_for_left';
options.regressors(32).regressorLabel = 'interaction_abXrel_signed_for_left';
options.regressors(32).regressorName = 'reg_interaction_abXrel_signed_for_left';
options.regressors(33).regressorLabel = 'abXrel_folded_orthog';
options.regressors(33).regressorName = 'reg_abXrelfolded_orthog';
options.regressors(34).regressorLabel = 'absoluted_prior_diff_to_0';
options.regressors(34).regressorName = 'reg_absoluted_prior_diff_to_0';
options.regressors(35).regressorLabel = 'signed_prior_diff_to_0';
options.regressors(35).regressorName = 'reg_signed_prior_diff_to_0';
options.regressors(36).regressorLabel = 'switch_prior';
options.regressors(36).regressorName = 'reg_switchprior';
options.regressors(37).regressorLabel = 'prior_signed_for_choice';
options.regressors(37).regressorName = 'reg_prior_signed_for_choice';
options.regressors(38).regressorLabel = 'abXrel_signed_for_choice';
options.regressors(38).regressorName = 'reg_abXrel_signed_for_choice';


options.TrialGLM.first_glm.name = 'first_glm';
options.TrialGLM.first_glm.regressors = options.regressors([5, 20]);

options.TrialGLM.correctIncorrectTR.name = 'correctIncorrectTR';
options.TrialGLM.correctIncorrectTR.regressors = options.regressors([8:11]);

options.TrialGLM.absoluted_prior.name = 'absoluted_prior'; 
options.TrialGLM.absoluted_prior.regressors = options.regressors([ 18, 17,  20, 5]);

options.TrialGLM.surprise_fb.name = 'surprise_fb'; 
options.TrialGLM.surprise_fb.regressors = options.regressors([1, 3, 20, 18, 22]);

options.TrialGLM.surprise_saccade.name = 'surprise_saccade'; 
options.TrialGLM.surprise_saccade.regressors = options.regressors([1, 3, 20, 18, 23]);

options.TrialGLM.surprise_wtPst.name = 'surprise_wtPst'; 
options.TrialGLM.surprise_wtPst.regressors = options.regressors([ 4, 13, 15, 19]);

options.TrialGLM.surprise_wtPst_with_rel_ab_reg.name = 'surprise_wtPst_with_rel_ab_reg'; 
options.TrialGLM.surprise_wtPst_with_rel_ab_reg.regressors = options.regressors([ 4, 13, 15, 19, 16, 1, 2]);

options.TrialGLM.update_prior.name = 'update_prior'; 
options.TrialGLM.update_prior.regressors = options.regressors([ 1, 3,  18, 12, 20]);

options.TrialGLM.prediction_error.name = 'prediction_error'; 
options.TrialGLM.prediction_error.regressors = options.regressors([20, 21]);

options.TrialGLM.prediction_error_orthog.name = 'prediction_error_orthog'; 
options.TrialGLM.prediction_error_orthog.regressors = options.regressors([24, 21]);



options.TrialGLM.priorInteraction.name = 'priorInteraction'; 
options.TrialGLM.priorInteraction.regressors = options.regressors([20, 31, 32]);

options.TrialGLM.priorLeftResp.name = 'priorLeftResp'; 
options.TrialGLM.priorLeftResp.regressors = options.regressors([5, 31]);

options.TrialGLM.abXrel_LeftResp.name = 'abXrel_LeftResp'; 
options.TrialGLM.abXrel_LeftResp.regressors = options.regressors([5, 32]);



%%% regressor Correlation

options.TrialGLM.regressor_correlation.name = 'regressor_correlation';
options.TrialGLM.regressor_correlation.regressors = options.regressors([1, 3, 6, 12, 16, 17, 18, 20, 21, 22, 23, 24, 31, 32, 37, 38]);

options.TrialGLM.regressor_correlation_behaviour.name = 'regressor_correlation_behaviour';
options.TrialGLM.regressor_correlation_behaviour.regressors = options.regressors([25:30, 1 2 16 19]);


 

options.behaviouralGLM.lagFeedback.name = 'lagFeedback'; 
options.behaviouralGLM.lagFeedback.regressors = options.regressors([25 1 2 16]);

options.behaviouralGLM.lagResponse.name = 'lagResponse'; 
options.behaviouralGLM.lagResponse.regressors = options.regressors([26 1 2 16]);

options.behaviouralGLM.lagResponseIntFb.name = 'lagResponseIntFb'; 
options.behaviouralGLM.lagResponseIntFb.regressors = options.regressors([25 26 27 1 2 16]);

options.behaviouralGLM.SignedPrior.name = 'SignedPrior'; 
options.behaviouralGLM.SignedPrior.regressors = options.regressors([19 1 2 16]);

options.behaviouralGLM.SignedPriorIntRel.name = 'SignedPriorIntRel'; 
options.behaviouralGLM.SignedPriorIntRel.regressors = options.regressors([19 28 1 2 16]);

options.behaviouralGLM.SignedPriorIntAbs.name = 'SignedPriorIntAbs'; 
options.behaviouralGLM.SignedPriorIntAbs.regressors = options.regressors([19 29 1 2 16]);

options.behaviouralGLM.SignedPriorIntAbsRel.name = 'SignedPriorIntAbsRel'; 
options.behaviouralGLM.SignedPriorIntAbsRel.regressors = options.regressors([19 30 1 2 16]);


%% file names GLM MEG
options.TrialGLM.correctIncorrectTR.dataSubjects    = fullfile(options.paths.analysisData2,'correctIncorrectTR_timeLockDataSubjects.mat');
options.TrialGLM.Correct.correctIncorrectTR.dataSubjects = fullfile(options.paths.analysisData2,'Correct_correctIncorrectTR_timeLockDataSubjects.mat');
options.TrialGLM.Wrong.correctIncorrectTR.dataSubjects = fullfile(options.paths.analysisData2,'Wrong_correctIncorrectTR_timeLockDataSubjects.mat');

options.TrialGLM.absoluted_prior.dataSubjects       = fullfile(options.paths.analysisData2,'absoluted_prior_timeLockDataSubjects.mat');
options.TrialGLM.Correct.absoluted_prior.dataSubjects = fullfile(options.paths.analysisData2,'Correct_absoluted_prior_timeLockDataSubjects.mat');
options.TrialGLM.Wrong.absoluted_prior.dataSubjects   = fullfile(options.paths.analysisData2,'Wrong_absoluted_prior_timeLockDataSubjects.mat');

options.TrialGLM.surprise_fb.dataSubjects           = fullfile(options.paths.analysisData2,'surprise_fb_timeLockDataSubjects.mat');

options.TrialGLM.surprise_saccade.dataSubjects      = fullfile(options.paths.analysisData2,'surprise_saccade_timeLockDataSubjects.mat');

options.TrialGLM.surprise_wtPst.dataSubjects        = fullfile(options.paths.analysisData2,'surprise_wtPst_timeLockDataSubjects.mat');

options.TrialGLM.update_prior.dataSubjects          = fullfile(options.paths.analysisData2,'update_prior_timeLockDataSubjects.mat');
options.TrialGLM.surprise_wtPst_with_rel_ab_reg.dataSubjects          = fullfile(options.paths.analysisData2,'surprise_wtPst_with_rel_ab_reg_timeLockDataSubjects.mat');

options.TrialGLM.prediction_error.dataSubjects      = fullfile(options.paths.analysisData2,'prediction_error_timeLockDataSubjects.mat');
options.TrialGLM.prediction_error_orthog.dataSubjects      = fullfile(options.paths.analysisData2,'prediction_error_orthog_timeLockDataSubjects.mat');


options.TrialGLM.priorInteraction.dataSubjects    = fullfile(options.paths.analysisData2,'priorInteraction_timeLockDataSubjects.mat');
options.TrialGLM.Correct.priorInteraction.dataSubjects    = fullfile(options.paths.analysisData2,'Correct_priorInteraction_timeLockDataSubjects.mat');
options.TrialGLM.Wrong.priorInteraction.dataSubjects    = fullfile(options.paths.analysisData2,'Wrong_priorInteraction_timeLockDataSubjects.mat');

options.TrialGLM.priorLeftResp.dataSubjects    = fullfile(options.paths.analysisData2,'priorLeftResp_timeLockDataSubjects.mat');
options.TrialGLM.Correct.priorLeftResp.dataSubjects    = fullfile(options.paths.analysisData2,'Correct_priorLeftResp_timeLockDataSubjects.mat');
options.TrialGLM.Wrong.priorLeftResp.dataSubjects    = fullfile(options.paths.analysisData2,'Wrong_priorLeftResp_timeLockDataSubjects.mat');

options.TrialGLM.abXrel_LeftResp.dataSubjects    = fullfile(options.paths.analysisData2,'abXrel_LeftResp_timeLockDataSubjects.mat');
options.TrialGLM.Correct.abXrel_LeftResp.dataSubjects    = fullfile(options.paths.analysisData2,'Correct_abXrel_LeftResp_timeLockDataSubjects.mat');
options.TrialGLM.Wrong.abXrel_LeftResp.dataSubjects    = fullfile(options.paths.analysisData2,'Wrong_abXrel_LeftResp_timeLockDataSubjects.mat');

options.TrialGLM.first_glm.dataSubjects    = fullfile(options.paths.analysisData2,'first_glm_timeLockDataSubjects.mat');



options.behaviouralGLM.lags = 3; 

options.ERPAnalysis.correctIncorrectTR.data = fullfile(options.paths.analysisData2,'correctIncorrectTR_ERPData.mat');

options.beamformed.chanLabels = {'V123_L','V123_R','hMT_L','hMT_R','pIPS_L','pIPS_R','aIPS_L','aIPS_R'};
options.channel.chanLabels = {
    'MEG0733MEG0732';
    'MEG0743MEG0742';
    'MEG1822MEG1823';
    'MEG1832MEG1833';
    'MEG1843MEG1842';
    'MEG2013MEG2012';
    'MEG2023MEG2022';
    'MEG2212MEG2213';
    'MEG2242MEG2243';
    'MEG2312MEG2313';
    'leftEye'       };

options.BehaviourMatch = [ 1 2 4 5 6 8 : 24 26 28 29 ];

end