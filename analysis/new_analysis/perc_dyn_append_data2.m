
clear all


options = generate_perc_dyn_options2();




for subject = 1: options.badTrialsRemoved.numSubjects
    disp(subject)
    details = generate_perdyn_subject_details2(subject,options);
    
    subjectRuns = details.badTrialsRemoved.subject.runs;
    
    clear data regressors
  
    
    for runid = 1:subjectRuns
        
        % load data 
        dataLoad =  load(details.badTrialsRemoved.channel.datafile(runid).run);
       
        data{runid} = dataLoad.data.ftData;
        regressors{runid} = dataLoad.data;
        
        
    end
    
    
    % append data 
    AllTrials = perc_dyn_append_raw_struct2(data, regressors, subjectRuns);
    
    % select data
    [wrongTrials, correctTrials] = perc_dyn_select_data2(AllTrials);

 
    % transform data in correct format
    AllTrials.ftData = perc_dyn_timelock_analysis2(AllTrials.ftData, 1);
    wrongTrials.ftData = perc_dyn_timelock_analysis2(wrongTrials.ftData, 1);
    correctTrials.ftData = perc_dyn_timelock_analysis2(correctTrials.ftData, 1);
    
   
    % save data 
    save(details.Trials.AllTrials.datafile,'AllTrials')
    save(details.Trials.WrongTrials.datafile, 'wrongTrials')
    save(details.Trials.CorrectTrials.datafile, 'correctTrials')
    
    
end
