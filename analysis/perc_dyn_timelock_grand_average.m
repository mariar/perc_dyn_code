function [timelockAverage] = perc_dyn_timelock_grand_average(data,options,type,glmFlag,ERPflag)

if nargin == 4
for regressor = 1:length(options.TrialGLM.(glmFlag).regressors)
    
    labelTStat = [options.TrialGLM.(glmFlag).regressors(regressor).regressorLabel,'_t'];
    labelBeta = [options.TrialGLM.(glmFlag).regressors(regressor).regressorLabel,'_b'];
    
   
switch type
    case 'within'
        cfg = [];
        cfg.method = 'across';
        cfg.parameter = labelTStat;
        timelockAverage.(labelTStat) = ft_timelockgrandaverage(cfg, data{:});
        
        
        cfg = [];
        cfg.method = 'across';
        cfg.parameter = labelBeta;
        timelockAverage.(labelBeta) = ft_timelockgrandaverage(cfg, data{:});
        
    case 'across'
        
        cfg = [];
        cfg.method = 'across';
        dataInput = {data(:).(labelTStat)};
        
        timelockAverage.(labelTStat) = ft_timelockgrandaverage(cfg, dataInput{:});
        
        
        cfg = [];
        cfg.method = 'across';
        dataInput = {data(:).(labelBeta)};
        timelockAverage.(labelBeta) = ft_timelockgrandaverage(cfg, dataInput{:});
        
end

end

else 
    
    switch type
    case 'within'
        cfg = [];
        cfg.method = 'within';
     
        timelockAverage = ft_timelockgrandaverage(cfg, data{:});
    
        case 'across' 
            cfg = [];
        cfg.method = 'across';
        
     

        timelockAverage = ft_timelockgrandaverage(cfg, data{:});
    end 
end 

end