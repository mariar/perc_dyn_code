function timeLock = perc_dyn_timelock_analysis(data, channels)

 
if nargin == 2 
    cfg = [];
    cfg.channel = channels; 
    cfg.avgoverchan = 'yes'; 
    cfg.nanmean = 'yes'; 
    
    data = ft_selectdata(cfg,data); 
    
end 
cfg = [];

cfg.keeptrials = 'yes'; 

timeLock        = ft_timelockanalysis(cfg,data);


end 